# README #

### What is this repository for? ###

* This repository contains the Holland College Curriculum database, as well as a PHP application to write forms and view reports.
* Version 1.0


### How do I get set up? ###

* There are four .sql files that need to be run in order through PHPMyadmin. They are numbered to express what order to run them in. 
* Next, launch the PHP application to the welcome page. Log in using the credentials "adminAccount" and "adminPassword". This will allow you to log in with access to every part of the application.
* Navigate to register a new user and create an admin account that you will use going forward.
* After registering a new admin user, log in with that account. Now with the users page, you can delete the set up account we used to first log in.
* Set up other users that are going to access the system. If they are allowed to have full access and be able to edit and delete, set them as an admin. For users who just need to access reports, set them as a general user.



### Who do I talk to? ###

* BJ MacLean, Tristan Whiten, Elodie Ineza, Sean Malone

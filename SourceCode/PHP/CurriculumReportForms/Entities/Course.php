<?php


class Course
{
    private $courseCode;
    private $courseTitle;
    private $courseDesc;
    private $gradeScheme;
    private $minimumGrade;
    private $instrMethod;
    private $outcomeHours;
    private $credits;
    private $hours;
    private $assessmentNote;
    private $researchComponent;
    private $openStudies;
    private $plar;
    private $newCourse;
    private $replacingExisting;
    private $originalVersionYear;
    private $currentVersionYear;
    private $revLevel;
    private $revDate;
    private $version;
    private $authPersonId;
    private $suppDocuments;
    private $addInformation;
    private $matterExperts;
    private $approvedPro;
    private $approvedProDate;
    private $approvedCon;
    private $approvedConDate;
    private $essentials;
    private $hoursByType;
    private $deliveryTypes;
    private $retiredDate;

    private $requisites;
    private $outcomes;
    private $competencies;
    private $assessmentCategories;
    private $courseReplacement;
    private $coursePrograms;


    function __construct(){
        //Default constructor
    }

    /**
     * @return mixed
     */
    public function getCourseCode()
    {
        return $this->courseCode;
    }

    /**
     * @param mixed $courseCode
     */
    public function setCourseCode($courseCode)
    {
        $this->courseCode = $courseCode;
    }

    /**
     * @return mixed
     */
    public function getCourseTitle()
    {
        return $this->courseTitle;
    }

    /**
     * @param mixed $courseTitle
     */
    public function setCourseTitle($courseTitle)
    {
        $this->courseTitle = $courseTitle;
    }

    /**
     * @return mixed
     */
    public function getCourseDesc()
    {
        return $this->courseDesc;
    }

    /**
     * @param mixed $courseDesc
     */
    public function setCourseDesc($courseDesc)
    {
        $this->courseDesc = $courseDesc;
    }

    /**
     * @return mixed
     */
    public function getGradeScheme()
    {
        return $this->gradeScheme;
    }

    /**
     * @param mixed $gradeScheme
     */
    public function setGradeScheme($gradeScheme)
    {
        $this->gradeScheme = $gradeScheme;
    }

    /**
     * @return mixed
     */
    public function getMinimumGrade()
    {
        return $this->minimumGrade;
    }

    /**
     * @param mixed $minimumGrade
     */
    public function setMinimumGrade($minimumGrade)
    {
        $this->minimumGrade = $minimumGrade;
    }

    /**
     * @return mixed
     */
    public function getInstrMethod()
    {
        return $this->instrMethod;
    }

    /**
     * @param mixed $instrMethod
     */
    public function setInstrMethod($instrMethod)
    {
        $this->instrMethod = $instrMethod;
    }

    /**
     * @return mixed
     */
    public function getOutcomeHours()
    {
        return $this->outcomeHours;
    }

    /**
     * @param mixed $outcomeHours
     */
    public function setOutcomeHours($outcomeHours)
    {
        $this->outcomeHours = $outcomeHours;
    }

    /**
     * @return mixed
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param mixed $credits
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
    }

    /**
     * @return mixed
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * @param mixed $hours
     */
    public function setHours($hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return mixed
     */
    public function getAssessmentNote()
    {
        return $this->assessmentNote;
    }

    /**
     * @param mixed $assessmentNote
     */
    public function setAssessmentNote($assessmentNote)
    {
        $this->assessmentNote = $assessmentNote;
    }

    /**
     * @return mixed
     */
    public function getResearchComponent()
    {
        return $this->researchComponent;
    }

    /**
     * @param mixed $researchComponent
     */
    public function setResearchComponent($researchComponent)
    {
        $this->researchComponent = $researchComponent;
    }

    /**
     * @return mixed
     */
    public function getOpenStudies()
    {
        return $this->openStudies;
    }

    /**
     * @param mixed $openStudies
     */
    public function setOpenStudies($openStudies)
    {
        $this->openStudies = $openStudies;
    }

    /**
     * @return mixed
     */
    public function getPlar()
    {
        return $this->plar;
    }

    /**
     * @param mixed $plar
     */
    public function setPlar($plar)
    {
        $this->plar = $plar;
    }

    /**
     * @return mixed
     */
    public function getNewCourse()
    {
        return $this->newCourse;
    }

    /**
     * @param mixed $newCourse
     */
    public function setNewCourse($newCourse)
    {
        $this->newCourse = $newCourse;
    }

    /**
     * @return mixed
     */
    public function getReplacingExisting()
    {
        return $this->replacingExisting;
    }

    /**
     * @param mixed $replacingExisting
     */
    public function setReplacingExisting($replacingExisting)
    {
        $this->replacingExisting = $replacingExisting;
    }

    /**
     * @return mixed
     */
    public function getOriginalVersionYear()
    {
        return $this->originalVersionYear;
    }

    /**
     * @param mixed $originalVersionYear
     */
    public function setOriginalVersionYear($originalVersionYear)
    {
        $this->originalVersionYear = $originalVersionYear;
    }

    /**
     * @return mixed
     */
    public function getCurrentVersionYear()
    {
        return $this->currentVersionYear;
    }

    /**
     * @param mixed $currentVersionYear
     */
    public function setCurrentVersionYear($currentVersionYear)
    {
        $this->currentVersionYear = $currentVersionYear;
    }

    /**
     * @return mixed
     */
    public function getRevLevel()
    {
        return $this->revLevel;
    }

    /**
     * @param mixed $revLevel
     */
    public function setRevLevel($revLevel)
    {
        $this->revLevel = $revLevel;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getAuthPersonId()
    {
        return $this->authPersonId;
    }

    /**
     * @param mixed $authPersonId
     */
    public function setAuthPersonId($authPersonId)
    {
        $this->authPersonId = $authPersonId;
    }

    /**
     * @return mixed
     */
    public function getSuppDocuments()
    {
        return $this->suppDocuments;
    }

    /**
     * @param mixed $suppDocuments
     */
    public function setSuppDocuments($suppDocuments)
    {
        $this->suppDocuments = $suppDocuments;
    }

    /**
     * @return mixed
     */
    public function getAddInformation()
    {
        return $this->addInformation;
    }

    /**
     * @param mixed $addInformation
     */
    public function setAddInformation($addInformation)
    {
        $this->addInformation = $addInformation;
    }

    /**
     * @return mixed
     */
    public function getMatterExperts()
    {
        return $this->matterExperts;
    }

    /**
     * @param mixed $matterExperts
     */
    public function setMatterExperts($matterExperts)
    {
        $this->matterExperts = $matterExperts;
    }

    /**
     * @return mixed
     */
    public function getApprovedPro()
    {
        return $this->approvedPro;
    }

    /**
     * @param mixed $approvedPro
     */
    public function setApprovedPro($approvedPro)
    {
        $this->approvedPro = $approvedPro;
    }

    /**
     * @return mixed
     */
    public function getApprovedProDate()
    {
        return $this->approvedProDate;
    }

    /**
     * @param mixed $approvedProDate
     */
    public function setApprovedProDate($approvedProDate)
    {
        $this->approvedProDate = $approvedProDate;
    }

    /**
     * @return mixed
     */
    public function getApprovedCon()
    {
        return $this->approvedCon;
    }

    /**
     * @param mixed $approvedCon
     */
    public function setApprovedCon($approvedCon)
    {
        $this->approvedCon = $approvedCon;
    }

    /**
     * @return mixed
     */
    public function getApprovedConDate()
    {
        return $this->approvedConDate;
    }

    /**
     * @param mixed $approvedConDate
     */
    public function setApprovedConDate($approvedConDate)
    {
        $this->approvedConDate = $approvedConDate;
    }

    /**
     * @return mixed
     */
    public function getEssentials()
    {
        return $this->essentials;
    }

    /**
     * @param mixed $essentials
     */
    public function setEssentials($essentials)
    {
        $this->essentials = $essentials;
    }

    /**
     * @return mixed
     */
    public function getHoursByType()
    {
        return $this->hoursByType;
    }

    /**
     * @param mixed $hoursByType
     */
    public function setHoursByType($hoursByType)
    {
        $this->hoursByType = $hoursByType;
    }

    /**
     * @return mixed
     */
    public function getDeliveryTypes()
    {
        return $this->deliveryTypes;
    }

    /**
     * @param mixed $deliveryTypes
     */
    public function setDeliveryTypes($deliveryTypes)
    {
        $this->deliveryTypes = $deliveryTypes;
    }

    /**
     * @return mixed
     */
    public function getRequisites()
    {
        return $this->requisites;
    }

    /**
     * @param mixed $requisites
     */
    public function setRequisites($requisites)
    {
        $this->requisites = $requisites;
    }

    /**
     * @return mixed
     */
    public function getOutcomes()
    {
        return $this->outcomes;
    }

    /**
     * @param mixed $outcomes
     */
    public function setOutcomes($outcomes)
    {
        $this->outcomes = $outcomes;
    }

    /**
     * @return mixed
     */
    public function getCompetencies()
    {
        return $this->competencies;
    }

    /**
     * @param mixed $competencies
     */
    public function setCompetencies($competencies)
    {
        $this->competencies = $competencies;
    }


    /**
     * @return mixed
     */
    public function getAssessmentCategories()
    {
        return $this->assessmentCategories;
    }

    /**
     * @param mixed $assessmentCategories
     */
    public function setAssessmentCategories($assessmentCategories)
    {
        $this->assessmentCategories = $assessmentCategories;
    }

    /**
     * @return mixed
     */
    public function getCourseReplacement()
    {
        return $this->courseReplacement;
    }

    /**
     * @param mixed $courseReplacement
     */
    public function setCourseReplacement($courseReplacement)
    {
        $this->courseReplacement = $courseReplacement;
    }

    /**
     * @return mixed
     */
    public function getCoursePrograms()
    {
        return $this->coursePrograms;
    }

    /**
     * @param mixed $coursePrograms
     */
    public function setCoursePrograms($coursePrograms)
    {
        $this->coursePrograms = $coursePrograms;
    }

    /**
     * @return mixed
     */
    public function getRevDate()
    {
        return $this->revDate;
    }

    /**
     * @param mixed $revDate
     */
    public function setRevDate($revDate)
    {
        $this->revDate = $revDate;
    }

    /**
     * @return mixed
     */
    public function getRetiredDate()
    {
        return $this->retiredDate;
    }

    /**
     * @param mixed $retiredDate
     */
    public function setRetiredDate($retiredDate)
    {
        $this->retiredDate = $retiredDate;
    }





}
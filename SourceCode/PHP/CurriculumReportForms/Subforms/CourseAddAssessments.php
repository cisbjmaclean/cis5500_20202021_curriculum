<?php
/**
 * CourseAddAssessments.php
 *
 * Allow user to add assessments to a new course.
 *
 * @author twhiten
 * @since 2021/03/16
 */

require("../Entities/Course.php");
require("../../Bootstrap/incPageHead.php");


//Declare variables

$course = new Course();
$assessmentTypes = array();
$typeOptions = array();

//Check whether there is a course in the session. If not, return to the main course creation form.
if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}
else {
    header("location:../Forms/AddCourseForm.php");
    exit();
}

//Get current assessment types
$query = "SELECT AssessmentCategoryNameId, AssessmentCategoryName FROM LookupAssessmentCategoryName";

$stmt = $db->prepare($query);

$stmt->execute();
$stmt->store_result();

$stmt->bind_result($id, $name);

if ($stmt->num_rows > 0) {
    while ($stmt->fetch()) {
        $typeOptions[$id] = $name;
    }
}

$assessmentTypes = $course->getAssessmentCategories();

//Check user action on this form
if (isset($_POST['assessmentType'])) {

    $tempAssessments = $assessmentTypes;

    $newType = $_POST['assessmentType'];
    $newPercent = $_POST['percent'];

    $tempAssessments[$newType] = $newPercent;

    $assessmentTypes = $tempAssessments;

    $course->setAssessmentCategories($assessmentTypes);
}




$_SESSION['course'] = $course;



?>
    <form action="CourseAddAssessments.php" method="post">
        <table class="table table-bordered">
            <tr class="thead-dark">
                <th colspan="3"><h1>Add Assessments</h1></th>
            </tr>
            <tr class="thead-light">
                <th><label for="assessmentType">Assessment Type</label></th>
                <th><label for="percent">Percent</label></th>
                <th></th>
            </tr>
            <?php
            if (!empty($assessmentTypes)) {
                foreach ($assessmentTypes as $id => $percent) {



                    echo '<tr>
                        <td>' . $typeOptions[$id] . '</td>
                        <td>' . $percent . '</td>
                        <td></td>
                      </tr>';
                }
            }

                echo '<tr>
                        <td>
                        <select name="assessmentType" id="assessmentType" class="form-control-sm">';

                foreach ($typeOptions AS $id => $name) {
                    echo '<option value="'.$id.'">'.$name.'</option>';
                }

                echo  '</select>
                        </td>
                        <td><input type="number" min="0" max="100" name="percent" id="percent" class="form-control"></td>
                        <td><input type="submit" value="Submit" class="form-control"></td>
                      </tr>';
                ?>
            <tr>
                <td colspan="3"><a href="CourseAddReplacements.php">Add course replacements.</a></td>
            </tr>
        </table>
    </form>

<?php



require("../../Bootstrap/incFootPage.php");
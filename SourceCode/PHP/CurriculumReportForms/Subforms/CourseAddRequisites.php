<?php
/**
 * CourseAddRequisites.php
 *
 * Allow user to add requisites to a new course.
 *
 * @author twhiten
 * @since 2021/03/15
 */




require("../Entities/Course.php");
session_start();

//Check whether all necessary information was added to this page. If not, return the user to the previous page.
if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}
else if (!isset($_POST['courseCode'])) {
    header("location:../Forms/AddCourseForm.php");
    exit();
}
else {
    //Create Course, add information
    $course = new Course();

    $course->setCourseCode($_POST['courseCode']);
    $course->setCourseTitle($_POST['courseTitle']);
    $course->setCourseDesc($_POST['courseDescription']);
    $course->setGradeScheme($_POST['gradeScheme']);
    $course->setMinimumGrade($_POST['minimumGrade']);
    $course->setInstrMethod($_POST['instructionalMethod']);
    $course->setOutcomeHours($_POST['outcomeHours']);
    $course->setCredits($_POST['credits']);
    $course->setHours($_POST['hours']);
    $course->setAssessmentNote($_POST['assessmentNote']);
    $course->setResearchComponent(isset($_POST['researchComponent']));
    $course->setOpenStudies(isset($_POST['openStudies']));
    $course->setPlar(isset($_POST['plar']));
    $course->setNewCourse(isset($_POST['newCourse']));
    $course->setReplacingExisting(isset($_POST['replacingExisting']));
    $course->setOriginalVersionYear($_POST['originalCatYear']);
    $course->setCurrentVersionYear($_POST['currentVersionCat']);
    $course->setRevLevel($_POST['revisionLevel']);
    $course->setRevDate($_POST['revisionDate']);
    $course->setVersion($_POST['version']);
    $course->setAuthPersonId($_POST['authorizedBy']);
    $course->setSuppDocuments($_POST['supportingDocuments']);
    $course->setAddInformation($_POST['additionalInformation']);
    $course->setMatterExperts($_POST['subjectMatterExperts']);
    $course->setApprovedPro($_POST['programManager']);
    $course->setApprovedProDate($_POST['approvedDate']);
    $course->setApprovedCon($_POST['consultant']);
    $course->setApprovedConDate($_POST['approvedDateCC']);
    $course->setRetiredDate($_POST['retiredDate']);
    $course->setEssentials(array('reading'=>isset($_POST['esReading']), 'document'=>isset($_POST['esDocument']), 'numeracy'=>isset($_POST['esNumeracy']), 'writing'=>isset($_POST['esWriting']), 'thinking'=>isset($_POST['esThinking']), 'oral'=>isset($_POST['esOral']), 'working'=>isset($_POST['esWorking']), 'compUse'=>isset($_POST['esCompUse']), 'contLearning'=>isset($_POST['esContLearning'])));
    $course->setHoursByType(array('class'=>$_POST['hoursClass'], 'lab'=>$_POST['hoursLab'], 'clinical'=>$_POST['hoursClinical'], 'ojt'=>$_POST['hoursOJT'], 'intern'=>$_POST['hoursIntern']));
    $course->setDeliveryTypes(array('face'=>isset($_POST['modalityFace']), 'distance'=>isset($_POST['modalityDistance']), 'hybrid'=>isset($_POST['modalityHybrid'])));

    //Check for required information
    if (empty($course->getCourseCode()) || empty($course->getCourseTitle()) || empty($course->getGradeScheme())) {
        header("location:../Forms/AddCourseForm.php?message='Please fill all required fields'");
        exit();
    }

    $_SESSION['course'] = $course;
}

require("../../Bootstrap/incPageHead.php");


//Declare variables

$course = null;
$requisites = array(array());


//Check whether all necessary information was added to this page. If not, return the user to the previous page.
if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}
else if (!isset($_POST['courseCode'])) {
    header("location:../Forms/AddCourseForm.php");
    exit();
}
else {
    //Create Course, add information
    $course = new Course();

    $course->setCourseCode($_POST['courseCode']);
    $course->setCourseTitle($_POST['courseTitle']);
    $course->setCourseDesc($_POST['courseDescription']);
    $course->setGradeScheme($_POST['gradeScheme']);
    $course->setMinimumGrade($_POST['minimumGrade']);
    $course->setInstrMethod($_POST['instructionalMethod']);
    $course->setOutcomeHours($_POST['outcomeHours']);
    $course->setCredits($_POST['credits']);
    $course->setHours($_POST['hours']);
    $course->setAssessmentNote($_POST['assessmentNote']);
    $course->setResearchComponent(isset($_POST['researchComponent']));
    $course->setOpenStudies(isset($_POST['openStudies']));
    $course->setPlar(isset($_POST['plar']));
    $course->setNewCourse(isset($_POST['newCourse']));
    $course->setReplacingExisting(isset($_POST['replacingExisting']));
    $course->setOriginalVersionYear($_POST['originalCatYear']);
    $course->setCurrentVersionYear($_POST['currentVersionCat']);
    $course->setRevLevel($_POST['revisionLevel']);
    $course->setRevDate($_POST['revisionDate']);
    $course->setVersion($_POST['version']);
    $course->setAuthPersonId($_POST['authorizedBy']);
    $course->setSuppDocuments($_POST['supportingDocuments']);
    $course->setAddInformation($_POST['additionalInformation']);
    $course->setMatterExperts($_POST['subjectMatterExperts']);
    $course->setApprovedPro($_POST['programManager']);
    $course->setApprovedProDate($_POST['approvedDate']);
    $course->setApprovedCon($_POST['consultant']);
    $course->setApprovedConDate($_POST['approvedDateCC']);
    $course->setRetiredDate($_POST['retiredDate']);
    $course->setEssentials(array('reading'=>isset($_POST['esReading']), 'document'=>isset($_POST['esDocument']), 'numeracy'=>isset($_POST['esNumeracy']), 'writing'=>isset($_POST['esWriting']), 'thinking'=>isset($_POST['esThinking']), 'oral'=>isset($_POST['esOral']), 'working'=>isset($_POST['esWorking']), 'compUse'=>isset($_POST['esCompUse']), 'contLearning'=>isset($_POST['esContLearning'])));
    $course->setHoursByType(array('class'=>$_POST['hoursClass'], 'lab'=>$_POST['hoursLab'], 'clinical'=>$_POST['hoursClinical'], 'ojt'=>$_POST['hoursOJT'], 'intern'=>$_POST['hoursIntern']));
    $course->setDeliveryTypes(array('face'=>isset($_POST['modalityFace']), 'distance'=>isset($_POST['modalityDistance']), 'hybrid'=>isset($_POST['modalityHybrid'])));

    //Check for required information
    if (empty($course->getCourseCode()) || empty($course->getCourseTitle()) || empty($course->getGradeScheme())) {
        header("location:../Forms/AddCourseForm.php?message='Please fill all required fields'");
        exit();
    }

    $_SESSION['course'] = $course;
}

if (isset($_POST['requisiteCourseCode']) && isset($_POST['requisiteType'])) {
    $newCode = $_POST['requisiteCourseCode'];
    $newType = $_POST['requisiteType'];

    if (!empty($newCode) && !empty($newType)) {
        $query = "SELECT CourseId, CourseTitle FROM CourseCatalogYear WHERE CourseId LIKE ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $newCode);

        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($code, $newTitle);

        if ($stmt->num_rows > 0) {
            $stmt->fetch();

            $currentRequisites = $course->getRequisites();

            $changedRow = null;
            
            if (!empty($currentRequisites)) {
                //Check if the current course is already a requisite

                for($row = 0; $row < sizeof($currentRequisites); $row++) {

                    if (strcasecmp($currentRequisites[$row]['Code'], $code) == 0) {
                        $changedRow = $row;
                    }
                }

                if (is_null($changedRow)) {
                    $changedRow = sizeof($currentRequisites);
                }

            }
            else {
                $changedRow = 0;
            }




            

            $currentRequisites[$changedRow]['Code'] = $newCode;
            $currentRequisites[$changedRow]['Title'] = $newTitle;
            $currentRequisites[$changedRow]['Type'] = $newType;

            $course->setRequisites($currentRequisites);

            
        }
        else {
            $message = "No course with that ID found";
        }
    }
    else if (empty($newCode) || empty($newType)) {
        $message = "Please fill out all fields";
    }
}
else if (isset($_GET['delete'])) {

    $oldRequisites = $course->getRequisites();
    $newRequisites = array(array());
    $deletedRow = $_GET['delete'];
    $rowCounter = 0;
    $numRows = 0;
    
    if (!empty($oldRequisites)) {
        $numRows = sizeof($oldRequisites);
    }

    for ($row = 0; $row < $numRows; $row++) {
        if ($row != $deletedRow) {
            $newRequisites[$rowCounter]['Code'] = $oldRequisites[$row]['Code'];
            $newRequisites[$rowCounter]['Title'] = $oldRequisites[$row]['Title'];
            $newRequisites[$rowCounter]['Type'] = $oldRequisites[$row]['Type'];
            $rowCounter++;
        }
    }

    if ($newRequisites[0]['Code'] = null) {
        $newRequisites = null;
    }

    $course->setRequisites($newRequisites);
}


    //Get requisite options
    $reqOptions = array();

    $query = "SELECT RequisiteId, RequisiteName FROM LookupRequisiteType";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $name);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $reqOptions[$id] = $name;
        }
    }

    $stmt->close();

    $_SESSION['course'] = $course;
    
    $requisites = $course->getRequisites();


?>
<form action="CourseAddRequisites.php" method="post">
    <table class="table table-bordered">
        <tr>
            <th colspan="4">Add Requisites</th>
        </tr>
        <tr class="thead-light">
            <th><label for="requisiteCourseCode">Requisite Course Code</label></th>
            <th><label for="requisiteCourseTitle">Requisite Course Title</label></th>
            <th><label for="requisiteType">Type</label></th>
            <th></th>
        </tr>
        <?php


        if (!empty($requisites)) {
            for($row = 0; $row < sizeof($requisites); $row++) {
                echo '<tr>
                        <td>'.$requisites[$row]['Code'].'</td>
                        <td>'.$requisites[$row]['Title'].'</td>
                        <td>'.$reqOptions[$requisites[$row]['Type']].'</td>
                        <td><a href="CourseAddRequisites.php?delete='.$row.'">Remove Requisite</a></td>
                      </tr>';
            }
        }
        ?>
        <tr>
            <td colspan="2"><input type="text" name="requisiteCourseCode" id="requisiteCourseCode"></td>
            <td colspan="2">
                <select id="requisiteType" name="requisiteType">
                    <?php
                        foreach($reqOptions AS $key => $value) {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Add Requisite"></td>
            <td colspan="2"><a href="CourseAddOutcomes.php">Continue to Outcomes and Competencies</a></td>
        </tr>
            <?php
            if (isset($message)) {
                    echo '<tr>
                            <td colspan="4">'.$message.'</td>
                          </tr>';
            }
            ?>
    </table>
</form>

<?php



require("../../Bootstrap/incFootPage.php");
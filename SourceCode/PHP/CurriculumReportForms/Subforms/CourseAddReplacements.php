<?php
/**
 * CourseAddReplacements.php
 *
 * Allow user to add information on courses this course is replacing.
 *
 * @author twhiten
 * @since 2021/03/16
 */


require("../Entities/Course.php");
session_start();
require("../../Bootstrap/incPageHead.php");


//Declare variables

$course = null;
$courseReplacement = array();


//Check whether there is a course in the session. If not, return to the main course creation form.
if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}
else {
    header("location:../Forms/AddCourseForm.php");
    exit();
}

if (isset($_POST['oldCourseCode'])) {
    $courseCode = $_POST['oldCourseCode'];

    if (!empty($courseCode)) {

        $query = "SELECT CourseId, CourseTitle FROM CourseCatalogYear WHERE CourseId LIKE ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $courseCode);

        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($oldCode, $oldTitle);

        if ($stmt->num_rows > 0) {
            $stmt->fetch();

            $currentReplacement = $course->getCourseReplacement();

            $currentReplacement[$oldCode] = $oldTitle;

            $course->setCourseReplacement($currentReplacement);

        }
        else {
            $message = "No course with that ID found";
        }
    }
    else {
        $message = "Please properly fill field";
    }
}
else if (isset($_GET['delete'])) {

    $tempReplacement = $course->getCourseReplacement();

    unset($tempReplacement[$_GET['delete']]);

    $course->setCourseReplacement($tempReplacement);
}


$_SESSION['course'] = $course;

$courseReplacement = $course->getCourseReplacement();
?>

<form action="CourseAddReplacements.php" method="post">
    <table class="table table-bordered">
        <tr class="thead-dark">
            <th colspan="3"><h1>Course Replacements</h1></th>
        </tr>
        <tr class="thead-light">
            <th><label for="oldCourseCode">Old Course ID</label></th>
            <th colspan="2"><label for="oldCourseTitle">Old Course Title</label></th>
        </tr>
        <?php
        if (!empty($courseReplacement)) {
            foreach($courseReplacement AS $code => $title) {
                echo '<tr>
                        <td>'.$code.'</td>
                        <td>'.$title.'</td>
                        <td><a href="CourseAddReplacements.php?delete='.$code.'">Remove replacement</a></td>
                      </tr>';
            }
        }
        ?>
        <tr>
            <td>Add ID of old course</td>
            <td colspan="2"><input type="text" name="oldCourseCode" id="oldCourseCode" class="form-control"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Add Replacement"></td>
            <td colspan="2"><a href="CourseAddPrograms.php">Add Programs</a></td>
        </tr>
            <?php
            if (isset($message)) {
                    echo '<tr>
                            <td colspan="3">'.$message.'</td>
                          </tr>';
            }
            ?>
    </table>
</form>

<?php



require("../../Bootstrap/incFootPage.php");
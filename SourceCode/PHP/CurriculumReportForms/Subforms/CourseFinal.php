<?php
/**
 * CourseFinal.php
 *
 * Insert information collected from user into the curriculum database
 *
 * @author twhiten
 * @since 2021/03/17
 */

require("../Entities/Course.php");
session_start();
$db = new mysqli('localhost', 'root', '', 'curriculum');





$course = new Course;

if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}

$courseCode = $course->getCourseCode();
$courseTitle = $course->getCourseTitle();
$courseDescription = $course->getCourseDesc();
$gradeScheme = $course->getGradeScheme();
$minimumGrade = $course->getMinimumGrade();
$instrMethod = $course->getInstrMethod();
$outcomeHours = $course->getOutcomeHours();
$research = $course->getResearchComponent();
$credits = $course->getCredits();
$hours = $course->getHours();
$assessmentNote = $course->getAssessmentNote();
$openStudies = $course->getOpenStudies();
$plar = $course->getPlar();
$newCourse = $course->getNewCourse();
$replacingExisting = $course->getReplacingExisting();
$originalVersionYear = $course->getOriginalVersionYear();
$currentVersionYear = $course->getCurrentVersionYear();
$revLevel = $course->getRevLevel();
$revDate = $course->getRevDate();
$version = $course->getVersion();
$authPersonId = $course->getAuthPersonId();
$suppDocuments = $course->getSuppDocuments();
$addInformation = $course->getAddInformation();
$matterExperts = $course->getMatterExperts();
$approvedPro = $course->getApprovedPro();
$approvedProDate = $course->getApprovedProDate();
$approvedCon = $course->getApprovedCon();
$approvedConDate = $course->getApprovedConDate();
$essentials = $course->getEssentials();
$hoursByType = $course->getHoursByType();
$deliveryTypes = $course->getDeliveryTypes();
$retiredDate = $course->getRetiredDate();

$requisites = $course->getRequisites();
$outcomes = $course->getOutcomes();
$competencies = $course->getCompetencies();
$assessmentCategories = $course->getAssessmentCategories();
$courseReplacement = $course->getCourseReplacement();

$coursePrograms = $course->getCoursePrograms();

//Create main course

$query = "INSERT INTO Course (CourseId, Active) VALUES (?, 1)";

$stmt = $db->prepare($query);

$stmt->bind_param("s", $courseCode);

$stmt->execute();
$stmt->store_result();

if ($stmt->affected_rows == 1) {

    $stmt->close();

    $query = "INSERT INTO CourseCatalogYear (CourseId, CatalogYearId, CourseVersion, Active, CourseTitle, MinimumGrade, Hours, Credits, CourseDescription, GradeSchemeId, OutcomeHours, Research, NewCourse, ReplacingExisting, OriginalCatYear, CurrentVersionCat, RevisionLevel, RevisionDate, SupportingDoc, AdditionalInfo, SubjectMatterExpert, ApprovedDate, ApprovedByConsultantPersonId, ApprovedDateCC, Plar, RetiredDate, ESReading, ESDocumentUse, ESNumeracy, ESWriting, ESThinking, ESOral, ESWorking, ESCompUse, ESContinuousLearning, ClassHours, LabHours, ClinicalHours, OJTPracticumHours, InternshipHours, OpenStudies, ModalityFace, ModalityDistance, ModalityHybrid, InstructionalMethodId, ApprovedByPMPersonId, AssessmentNote, AuthorizedByPersonId) VALUES (?, (SELECT MAX(catalogYearId) FROM LookupCatalogYear), ?, 1, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    $stmt = $db->prepare($query);

    $stmt->bind_param("sssssssssiiissssssssssisiiiiiiiiisssssiiiissss", $courseCode, $version, $courseTitle, $minimumGrade, $hours, $credits, $courseDescription, $gradeScheme, $outcomeHours, intval($research), intval($newCourse), intval($replacingExisting), $originalVersionYear, $currentVersionYear, $revLevel, $revDate, $suppDocuments, $addInformation, $matterExperts, $approvedProDate, $approvedCon, $approvedConDate, intval($plar), $retiredDate, intval($essentials['reading']), intval($essentials['document']), intval($essentials['numeracy']), intval($essentials['writing']), intval($essentials['thinking']), intval($essentials['oral']), intval($essentials['working']), intval($essentials['compUse']), intval($essentials['contLearning']), $hoursByType['class'], $hoursByType['lab'], $hoursByType['clinical'], $hoursByType['ojt'], $hoursByType['intern'], intval($openStudies), intval($deliveryTypes['face']), intval($deliveryTypes['distance']), intval($deliveryTypes['hybrid']), $instrMethod, $approvedPro, $assessmentNote, $authPersonId);

    $stmt->execute();
    $stmt->store_result();

    if ($stmt->affected_rows == 1) {
        $courseCatalogYearId = $stmt->insert_id;
        $stmt->close();

        if (!empty($requisites)) {
            for ($row = 0; $row < sizeof($requisites); $row++) {

                $code = $requisites[$row]['Code'];
                $title = $requisites[$row]['Title'];
                $type = $requisites[$row]['Type'];

                $query = "INSERT INTO CourseRequisite (CatalogYearId, ParentCourseId, ChildCourseId, ChildTitle, RequisiteTypeId) VALUES ((SELECT MAX(CatalogYearId) FROM LookupCatalogYear), ?, ?, ?, ?)";

                $stmt = $db->prepare($query);

                $stmt->bind_param("sssi", $courseCode, $code, $title, $type);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows != 1) {
                    header("location:../Forms/AddCourseForm.php?message='Could not create Course Requisite'");
                    exit();
                }

                $stmt->close();

            }
        }

        $outcomeCount = 1;
        $compCount = 1;

        if (!empty($outcomes)) {
            foreach ($outcomes as $currentOutcome) {
                $compCount = 1;

                $query = "INSERT INTO LearningOutcome (CourseCatalogYearId, LearningOutcomeName) VALUES (?, ?)";

                $stmt = $db->prepare($query);

                $currentOutcome = $outcomeCount . ". " . $currentOutcome;

                $stmt->bind_param("is", $courseCatalogYearId, $currentOutcome);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows != 1) {
                    header("location:../Forms/AddCourseForm.php?message='Could not create Learning Outcome'");
                    exit();
                }

                $outcomeId = $stmt->insert_id;

                $stmt->close();


                if (!empty($competencies[$outcomeCount - 1])) {
                    foreach ($competencies[$outcomeCount - 1] as $currentComp) {

                        $query = "INSERT INTO LearningCompetency (LearningOutcomeId, LearningCompetencyName) VALUES (?, ?)";

                        $stmt = $db->prepare($query);

                        $currentComp = $outcomeCount . "." . $compCount . " " . $currentComp;

                        $stmt->bind_param("is", $outcomeId, $currentComp);

                        $stmt->execute();
                        $stmt->store_result();

                        if ($stmt->affected_rows != 1) {
                            header("location:../Forms/AddCourseForm.php?message='Could not create Learning Competency'");
                            exit();
                        }

                        $stmt->close();

                        $compCount++;
                    }
            }
                $outcomeCount++;
            }
        }

        if (!empty($assessmentCategories)) {
            foreach ($assessmentCategories as $currentAssessment => $currentPercent) {

                $query = "INSERT INTO assessmentCategory (CourseCatalogYearId, AssessmentCategoryNameId, AssessmentCategoryPercent) VALUES (?, ?, ?)";

                $stmt = $db->prepare($query);

                $stmt->bind_param("iii", $courseCatalogYearId, $currentAssessment, $currentPercent);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows != 1) {
                    header("location:../Forms/AddCourseForm.php?message='Could not create Assessment Category'");
                    exit();
                }

                $stmt->close();
            }
        }

        if (!empty($courseReplacement)) {
            foreach ($courseReplacement as $code => $title) {
                $query = "INSERT INTO courseReplacement (OldCourseId, OldCourseTitle, NewCourseId) VALUES (?, ?, ?)";

                $stmt = $db->prepare($query);

                $stmt->bind_param("sss", $code, $title, $courseCode);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows != 1) {
                    header("location:../Forms/AddCourseForm.php?message='Could not create Course Replacement'");
                    exit();
                }

                $stmt->close();
            }
        }

        if (!empty($coursePrograms)) {
            for ($row = 0; $row < sizeof($coursePrograms); $row++) {

                $programId = $coursePrograms[$row]['id'];
                $programYear = $coursePrograms[$row]['year'];
                $programElective = intval($coursePrograms[$row]['elective']);
                $programOptional = intval($coursePrograms[$row]['optional']);

                $query = "SELECT ProgramCatalogYearId FROM ProgramCatalogYear WHERE ProgramId = ? AND CatalogYearId = ?";

                $stmt = $db->prepare($query);

                $stmt->bind_param("ss", $programId, $programYear);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->num_rows != 1) {
                    header("location:../Forms/AddCourseForm.php?message='Could not find program'");
                    exit();
                }

                $stmt->bind_result($tempProgramId);

                $stmt->fetch();

                $programCatalogYearId = $tempProgramId;

                $stmt->close();

                $query = "INSERT INTO programCourse(ProgramCatalogYearId, CatalogYearId, CourseCatalogYearId, Elective, Optional) VALUES (?, (SELECT CatalogYearId FROM LookupCatalogYear WHERE CatalogYearName = ?), ?, ?, ?)";

                $stmt = $db->prepare($query);

                $stmt->bind_param("sssii", $programCatalogYearId, $programYear, $courseCatalogYearId, $programElective, $programOptional);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows != 1) {
                    $stmt->close();
                    header("location:../Forms/AddCourseForm.php?message='Could not create Program Course'");
                    exit();
                }


            }
        }
    }
    else {
        header("location:../Forms/AddCourseForm.php?message='Could not create CourseCatalogYear object'");
        exit();
    }
}
else {
    header("location:../Forms/AddCourseForm.php?message='Could not create Course'");
    exit();
}


header("location:../Forms/AddCourseForm.php?message='Course successfully created'");
exit();


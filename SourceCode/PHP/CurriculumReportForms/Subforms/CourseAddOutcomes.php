<?php
/**
 * CourseAddOutcomes.php
 *
 * Allow user to add outcomes and competencies to a new course.
 *
 * @author twhiten
 * @since 2021/03/16
 */


    require("../Entities/Course.php");
    session_start();
    require("../../Bootstrap/incPageHead.php");




//Declare variables

$course = null;
$outcomeList = array();
$competencyList = array(array());


//Check whether there is a course in the session. If not, return to the main course creation form.
if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}
else {
    header("location:../Forms/AddCourseForm.php");
    exit();
}

$outcomeList = $course->getOutcomes();
$competencyList = $course->getCompetencies();


//Check user action on this form
if (isset($_GET['outcome']) && !isset($_GET['competency'])) {
    //Delete an outcome from the form

    //Empty arrays that will be used to create copies of the current array
    $updateOutcome = array();
    $updateComp = array(array());

    //GET variable checks which row will be removed
    $deletedRow = $_GET['outcome'];

    //Count variables
    $outcomeCount = 0;
    $compCount = 0;

    //Loop through existing array lists and copy them into the new arrays
    foreach ($outcomeList AS $cOutcome) {
        $compCount = 0;
        if ($deletedRow != $outcomeCount) {
            $updateOutcome[$outcomeCount] = $cOutcome;

            if (!empty($competencyList[$outcomeCount])) {
                foreach ($competencyList[$outcomeCount] AS $cComp) {
                    $updateComp[$outcomeCount][$compCount] = $cComp;
                    $compCount++;
                }
            }
            $outcomeCount++;
        }
        else {
            $deletedRow = -1;
        }

    }

    //Transfer new arrays into the Course object.
    $course->setOutcomes($updateOutcome);
    $course->setCompetencies($updateComp);

    $outcomeList = $course->getOutcomes();
    $competencyList = $course->getCompetencies();

}
else if(isset($_GET['outcome']) && isset($_GET['competency'])) {
    //Delete a competency from the form

    //Empty array used to create a copy of the new array
    $updateOutcome = array();
    $updateComp = array(array());

    //GET competency variable determines which part of the array will be removed
    $deletedRow = $_GET['competency'];

    //GET outcome variable determines which row the competency will be removed from
    $selectedOutcome = $_GET['outcome'];
    $outcomeCount = 0;
    $compCount = 0;


    foreach ($outcomeList AS $cOutcome) {
        $compCount = 0;
        $updateOutcome = $cOutcome;
        if ($outcomeCount != $selectedOutcome) {
            foreach ($competencyList[$outcomeCount] as $cComp) {
                $updateComp[$outcomeCount][$compCount] = $cComp;
                $compCount++;
            }
        }
        else {
            foreach ($competencyList[$outcomeCount] as $cComp) {
                if ($compCount != $deletedRow) {
                $updateComp[$outcomeCount][$compCount] = $cComp;
                $compCount++;
                }
                else {
                    $deletedRow = -1;
                }
            }
        }
        $outcomeCount++;
    }

    $course->setOutcomes($updateOutcome);
    $course->setCompetencies($updateComp);

}
else if(isset($_POST['newComp'])) {
    //Add a competency to an outcome.

    $newComp = $_POST['newComp'];
    $selectedOutcome = $_POST['outcome'];

    $updateComp = $course->getCompetencies();

    $updateCount = null;

    if (!empty($updateComp[$selectedOutcome])) {
        $updateCount = count($updateComp[$selectedOutcome]);
    }
    else {
        $updateCount = 0;
    }


    if (!empty($newComp)) {
        $updateComp[$selectedOutcome][$updateCount] = $newComp;

        $course->setCompetencies($updateComp);

        $competencyList = $course->getCompetencies();
    }
}
else if(isset($_POST['newOutcome'])) {

    $newOutcome = $_POST['newOutcome'];

    $updateOutcome = $outcomeList;
    $updateCount = null;

    if (!empty($updateOutcome)) {
        $updateCount = sizeof($updateOutcome);
    }
    else {
        $updateCount = 0;
    }


    if (!empty($newOutcome)) {
        $updateOutcome[$updateCount] = $newOutcome;

        $outcomeList = $course->setOutcomes($updateOutcome);

    }
}

//Save any changes made to the course
$outcomeList = $course->getOutcomes();
$competencyList = $course->getCompetencies();

$_SESSION['course'] = $course;



?>
    <table class="table table-bordered">
        <tr class="thead-dark">
            <th colspan="3"><h1>Add Learning Outcomes</h1></th>
        </tr>
        <?php

        $outcomeCount = null;

        if (!empty($outcomeList)) {
            $outcomeCount = count($outcomeList);
        }
        else {
            $outcomeCount = 0;
        }



        for ($currentOutcome = 0; $currentOutcome < $outcomeCount; $currentOutcome++) {
            echo '<tr class="thead-light">
                    <th colspan="2">'.$outcomeList[$currentOutcome].'</th>
                    <th><a href="CourseAddOutcomes.php?outcome='.$currentOutcome.'">Remove Outcome</a></th>
                    <th></th>
                  </tr>';

            $compCount = null;

            if (!empty($competencyList[$currentOutcome])) {
                $compCount = sizeof($competencyList[$currentOutcome]);
            }
            else {
                $compCount = 0;
            }

            for($currentComp = 0; $currentComp < $compCount; $currentComp++) {
                echo '<tr>
                        <td></td>
                        <td colspan="2">'.$competencyList[$currentOutcome][$currentComp].'</td>
                        <td><a href="CourseAddOutcomes.php?outcome='.$currentOutcome.'&competency='.$currentComp.'">Remove Competency</a></td>
                      </tr>';
            }
            echo '<form action="CourseAddOutcomes.php" method="post">
                    <tr>
                        <td><input type="hidden" name="outcome" id="outcome" value="'.$currentOutcome.'"></td>
                        <td colspan="2"><input type="text" name="newComp" id="newComp"></td>
                        <td><input type="submit" value="Add Competency"></td>
                    </tr>
                  </form>';
        }
        ?>
        <form action="CourseAddOutcomes.php" method="post">
            <tr>
                <td><label for="newOutcome">Add new outcome</label></td>
                <td colspan="2"><input type="text" name="newOutcome" id="newOutcome"></td>
                <td><input type="submit" value="Add Outcome"></td>
            </tr>
        </form>
        <tr>
            <td colspan="4"><a href="CourseAddAssessments.php">Add new assessment categories</a></td>
        </tr>
            <?php
            if (isset($message)) {
                    echo '<tr>
                            <td colspan="4">'.$message.'</td>
                          </tr>';
            }
            ?>
    </table>

<?php



require("../../Bootstrap/incFootPage.php");
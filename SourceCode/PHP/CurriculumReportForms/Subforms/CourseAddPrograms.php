<?php
/**
 * CourseAddPrograms.php
 *
 * Allow a user to add programs to a course.
 *
 * @author twhiten
 * @since 2021/03/17
 */



require("../Entities/Course.php");
session_start();
require('../../Bootstrap/incPageHead.php');



$course = new Course();
$coursePrograms = array(array());

//Check whether there is a course in the session. If not, return to the main course creation form.
if (isset($_SESSION['course'])) {
    $course = $_SESSION['course'];
}
else {
    header("location:../Forms/AddCourseForm.php");
    exit();
}


//Check user action
if (isset($_GET['delete'])) {
    $oldPrograms = $course->getCoursePrograms();
    $newPrograms = array(array());
    $deletedRow = $_GET['delete'];
    $rowCount = 0;

    for ($row = 0; $row < sizeof($oldPrograms); $row++) {
        if ($row != $deletedRow) {
            $newPrograms[$rowCount]['id'] = $oldPrograms[$row]['id'];
            $newPrograms[$rowCount]['year'] = $oldPrograms[$row]['year'];
            $newPrograms[$rowCount]['elective'] = $oldPrograms[$row]['elective'];
            $newPrograms[$rowCount]['optional'] = $oldPrograms[$row]['optional'];
            $rowCount++;
        }
    }

    $course->setCoursePrograms($newPrograms);
}
else if (isset($_POST['programId']) && isset($_POST['catalogYear'])) {

    $programId = $_POST['programId'];
    $catalogYear = $_POST['catalogYear'];

    if (!empty($programId) && !empty($catalogYear)) {
        $query = "SELECT programId, programCatalogYear.catalogYearId FROM programCatalogYear INNER JOIN lookupCatalogYear ON programCatalogYear.catalogYearId = lookupCatalogYear.catalogYearId WHERE programId = ? AND catalogYearName = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("ss", $programId, $catalogYear);

        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($newProgram, $catalogYearId);

        if ($stmt->num_rows == 1) {

            $stmt->fetch();

            $coursePrograms = $course->getCoursePrograms();
            $newRow = null;

            if (!empty($coursePrograms)) {
                for ($row = 0; $row < sizeof($coursePrograms); $row++) {
                    if ($coursePrograms[$row]['id'] = $programId) {
                        $newRow = $row;
                    }
                }
            }

            if (is_null($newRow) && !empty($coursePrograms)) {
                $newRow = sizeof($coursePrograms);
            }
            else {
                $newRow = 0;
            }





            $coursePrograms[$newRow]['id'] = $newProgram;
            $coursePrograms[$newRow]['year'] = $catalogYearId;
            $coursePrograms[$newRow]['elective'] = isset($_POST['elective']);
            $coursePrograms[$newRow]['optional'] = isset($_POST['optional']);

            $course->setCoursePrograms($coursePrograms);
        }
        else {
            $message = "No program with id ".$programId." found";
        }

        $stmt->close();
    }
}




$_SESSION['course'] = $course;

$coursePrograms = $course->getCoursePrograms();

$query = "SELECT MAX(CatalogYearName) FROM LookupCatalogYear";

$stmt = $db->prepare($query);

$stmt->execute();
$stmt->store_result();

$stmt->bind_result($latestYear);

$stmt->fetch();

$maxYear = $latestYear;


?>

    <form action="CourseAddPrograms.php" method="post">
        <table class="table table-bordered">
            <tr class="thead-dark">
                <th colspan="5"><h1>Add Programs</h1></th>
            </tr>
            <tr class="thead-light">
                <th><label for="programId">Program Id</label></th>
                <th><label for="catalogYear">Year</label></th>
                <th><label for="elective">Elective</label></th>
                <th><label for="optional">Optional</label></th>
                <th></th>
            </tr>
            <?php
            if (!empty($coursePrograms)) {
                for ($row = 0; $row < sizeof($coursePrograms); $row++) {
                    echo '<tr>
                            <td>'.$coursePrograms[$row]['id'].'</td>
                            <td>'.$coursePrograms[$row]['year'].'</td>
                            
                            <td>';

                            if ($coursePrograms[$row]['elective']) {
                                echo '<input type="checkbox" checked=checked onclick="return false">';
                            }
                            else {
                                echo '<input type="checkbox" onclick="return false">';
                            }

                    echo   '</td>

                            <td>';

                            if ($coursePrograms[$row]['optional']) {
                                echo '<input type="checkbox" checked=checked onclick="return false">';
                            }
                            else {
                                echo '<input type="checkbox" onclick="return false">';
                            }

                    echo    '</td>
                             <td><a href="CourseAddPrograms.php?delete='.$row.'"</td>
                            
                            
                          </tr>';
                }
            }
            ?>
            <tr class="thead-light">
                <th>Enter ID of program</th>
                <th>Select year</th>
                <th>Elective</th>
                <th>Optional</th>
                <th></th>
            </tr>
            <tr>
                <td><input type="text" id="programId" name="programId" class="form-control"></td>
                <td><input type="number" id=catalogYear" name="catalogYear" class="form-control" min="2014" max="<?php $maxYear ?>"</td>
                <td><input type="checkbox" id="elective" name="elective" class="form-check-inline"></td>
                <td><input type="checkbox" id="optional" name="optional" class="form-check-inline"></td>
                <td><input type="submit" value="Submit" class="form-control"></td>
            </tr>
            <tr>
                <td></td>
                <td colspan="3"><a href="CourseFinal.php">Continue to final confirmation</a></td>
                <td></td>
            </tr>
            <?php
            if (isset($message)) {
                echo '<tr>
                        <td colspan="5">'.$message.'</td>
                      </tr>';
            }
            ?>
        </table>
    </form>

<?php
require('../../Bootstrap/incFootPage.php');
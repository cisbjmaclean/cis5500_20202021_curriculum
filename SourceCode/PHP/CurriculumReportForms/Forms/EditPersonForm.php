<?php
/**
 * EditPersonForm.php
 *
 * Allows a user to edit a person in the database
 *
 * @author twhiten
 * @since 2021/04/16
 */



require("../../Bootstrap/incPageHead.php");

//Check if user is a logged in admin.
if(!isset($_SESSION['userType']) || $_SESSION['userType'] != 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}

$id = null;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
else if (isset($_POST['id'])) {
    $id = $_POST['id'];
}
else {
    header("location: ../Reports/PersonList.php");
}

    $message = null;

    if (isset($_POST['personFName'])) {
        $firstName = $_POST['personFName'];
        $lastName = $_POST['personLName'];


        if (empty($firstName) || empty($lastName)) {
            $message = "Please enter a name";
        }
        else {
            $programManager = isset($_POST['programManager']);
            $consultant = isset($_POST['consultant']);
            $active = isset($_POST['active']);

            $query = "UPDATE Person SET PersonFirstName = ?, PersonLastName = ?, IsProgramManager = ?, IsConsultant = ?, Active = ? WHERE PersonId = ?";

            $stmt = $db->prepare($query);

            if ($stmt != false) {
                $stmt->bind_param("ssiiii", $firstName, $lastName, $programManager, $consultant, $active, $id);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows == 1) {
                    $message = $firstName . " " . $lastName . " successfully added";
                } else {
                    $message = "Could not add person to database";
                }
            }
            else {
                $message = "Query creation failed";
            }

        }
    }

    $query = "Select PersonFirstName, PersonLastName, IsProgramManager, IsConsultant, Active FROM Person WHERE PersonId = ?";

    $stmt = $db->prepare($query);

    $stmt->bind_param("s", $id);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($firstName, $lastName, $isPM, $isC, $active);

    if ($stmt->num_rows == 1) {

        $stmt->fetch();

        echo '<form action="EditPersonForm.php" method="post" class="form-group">
    <table class="table table-bordered">
        <tr>
            <td colspan="2" class="thead-light"><h2>Edit Person</h2></td>
            <input type="hidden" name="id" value="'.$id.'">
        </tr>
        <tr>
            <td colspan="2"><label for="personFName">Name</label><label for="personLName">(First/Last)</label></td>
        </tr>
        <tr>
            <td><input type="text" id="personFName" name="personFName" maxlength="40" class="form-control" value="'.$firstName.'"></td>
            <td><input type="text" id="personLName" name="personLName" maxlength="40" class="form-control" value="'.$lastName.'"></td>
        </tr>
        <tr>
            <td><label for="programManager">Program Manager</label></td>';
            if ($isPM) {
                        echo '<td><input type="checkbox" name="programManager" checked="checked" class="form-check-inline"></td>';
                    }
                    else {
                        echo '<td><input type="checkbox" name="programManager" class="form-check-inline"></td>';
                    }
  echo '</tr>
        <tr>
            <td><label for="consultant">Consultant</label></td>';
            if ($isC) {
                echo '<td><input type="checkbox" name="consultant" checked="checked" class="form-check-inline"></td>';
            }
            else {
                echo '<td><input type="checkbox" name="consultant" class="form-check-inline"></td>';
            }
  echo '</tr>
        <tr>
            <td><label for="active">Active</label></td>';
        if ($active) {
            echo '<td><input type="checkbox" name="active" checked="checked" class="form-check-inline"></td>';
        }
        else {
            echo '<td><input type="checkbox" name="active" class="form-check-inline"></td>';
        }
        echo '
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Submit" class="form-control-sm"></td>
        </tr>
    </table>';
    }




        if (isset($message)) {
            echo '<p class="alert-warning">'.$message.'</p>';
        }
echo '</form>';


include("../../Bootstrap/incFootPage.php");

<?php
/**
 * CourseAddAssessments.php
 *
 * Allow user to add assessments to a new course.
 *
 * @author twhiten
 * @since 2021/03/16
 */


require("../../Bootstrap/incPageHead.php");


//Declare variables

$assessmentTypes = null;
$typeOptions = array();
$currentAssessments = array();
$courseId = null;
$courseYear = null;


//Get current assessment types
$query = "SELECT AssessmentCategoryNameId, AssessmentCategoryName FROM LookupAssessmentCategoryName";

$stmt = $db->prepare($query);

$stmt->execute();
$stmt->store_result();

$stmt->bind_result($id, $name);

if ($stmt->num_rows > 0) {
    while ($stmt->fetch()) {
        $typeOptions[$id] = $name;
    }
}

$stmt->close();


//Check user action on this form
if (isset($_POST['assessmentType'])) {

    $assessmentType = $_POST['assessmentType'];
    $assessmentPercent = $_POST['percent'];
    $courseCatalogYearId = $_POST['course'];

    //Check if the current assessment exists for that class
    $query = "SELECT assessmentCategoryId FROM assessmentCategory WHERE assessmentCategoryNameId = ? AND courseCatalogYearId = ?";

    $stmt = $db->prepare($query);

    $stmt->bind_param("ss", $assessmentType, $courseCatalogYearId);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($tempId);

    if ($stmt->num_rows > 0) {
        $stmt->fetch();

        $updateId = $tempId;

        $stmt->close();

        $updateQuery = "UPDATE assessmentCategory SET assessmentCategoryPercent = ? WHERE assessmentCategoryId = ?";

        $stmt = $db->prepare($updateQuery);

        $stmt->bind_param("ss", $assessmentPercent, $updateId);

        $stmt->execute();

        $stmt->close();

    }
    else {
        $stmt->close();

        $addQuery = "INSERT INTO assessmentCategory (assessmentCategoryNameId, assessmentCategoryPercent, courseCatalogNameId) VALUES (?, ?, ?)";

        $stmt = $db->prepare($addQuery);

        $stmt->bind_param("sss", $assessmentType, $assessmentPercent, $courseCatalogYearId);

        $stmt->execute();

        $stmt->close();
    }


}

if (isset($_GET['assessment'])) {



    //Delete an assessment from the database
    $query = "DELETE FROM AssessmentCategory WHERE AssessmentCategoryId = ?";

    $stmt = $db->prepare($query);

    $stmt->bind_param("s", $_GET['assessment']);

    $stmt->execute();

    $stmt->close();
}

$courseCatalogYearId = null;

//Get assessments from current course
if (isset($_GET['course'])) {
    $courseCatalogYearId = $_GET['course'];
}
else {
    $courseCatalogYearId = $_POST['course'];
}

$assessmentTypes = array();

$query = "SELECT AssessmentCategoryId, AssessmentCategoryNameId, AssessmentCategoryPercent WHERE CourseCatalogYearId = ?";

$stmt = $db->prepare($query);

$stmt->bind_param("s", $courseCatalogYearId);

$stmt->execute();
$stmt->store_result();

$stmt->bind_result($assessmentId, $nameId, $percent);

if ($stmt->num_rows > 0) {
    while ($stmt->fetch()) {
        $assessmentTypes[$assessmentId] = array('name' => $nameId, 'percent' => $percent);
    }
}



?>
    <form action="CourseAddAssessments.php" method="post">
        <table class="table table-bordered">
            <tr class="thead-dark">
                <th colspan="3"><h1>Add Assessments</h1></th>
            </tr>
            <tr class="thead-light">
                <th><label for="assessmentType">Assessment Type</label></th>
                <th><label for="percent">Percent</label></th>
                <th></th>
            </tr>
            <?php
            if (!empty($assessmentTypes)) {
                foreach ($assessmentTypes as $id => $current) {



                    echo '<tr>
                        <td>' . $typeOptions[$current['name']] . '</td>
                        <td>' . $current['percent'] . '</td>
                        <td><a href="CourseEditAssessments.php?course='.$courseCatalogYearId.'&assessment='.$id.'"</td>
                      </tr>';
                }
            }

                echo '<input type="hidden" name="course" value="'.$courseCatalogYearId.'">
                      <tr>
                        <td>
                        <select name="assessmentType" id="assessmentType" class="form-control-sm">';

                foreach ($typeOptions AS $id => $name) {
                    echo '<option value="'.$id.'">'.$name.'</option>';
                }

                echo  '</select>
                        </td>
                        <td><input type="number" min="0" max="100" name="percent" id="percent" class="form-control"></td>
                        <td><input type="submit" value="Add/Update Assessment" class="form-control"></td>
                      </tr>';
                ?>
            <tr>
                <td colspan="3"><a href="../Reports/QF132CourseOutline.php">Finish editing.</a></td>
            </tr>
        </table>
    </form>

<?php



require("../../Bootstrap/incFootPage.php");
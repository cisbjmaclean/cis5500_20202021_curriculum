<?php
/**
 * AddProgramForm.php
 *
 * Allows a user to add a program to the database
 *
 * @author twhiten
 * @since 2021/03/12
 */



require("../../Bootstrap/incPageHead.php");
//Check if user is a logged in admin.
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION['userType'] != 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}

    $message = null;
    $consultants = array();
    $programManagers = array();

    //Obtain available consultants and program managers
    $query = 'SELECT personId, concat(personFirstName," ",personLastName) AS personName, isProgramManager, isConsultant FROM Person WHERE Active AND (isProgramManager OR isConsultant)';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $name, $isManager, $isConsultant);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            if ($isManager) {
                $programManagers[$id] = $name;
            }

            if ($isConsultant) {
                $consultants[$id] = $name;
            }
        }
    }

    if (isset($_POST['programId'])) {
        $id = null;
        $title = null;
        $minimum = null;
        $manager = null;
        $consultant = null;


        if (empty($_POST['programId']) || empty($_POST['programTitle']) || empty($_POST['programMinimum']) || empty($_POST['programManager']) || empty($_POST['consultant'])) {
            $message = "Please fill all fields";
        }
        else {
            $id = $_POST['programId'];
            $title = $_POST['programTitle'];
            $minimum = $_POST['programMinimum'];
            $manager = $_POST['programManager'];
            $consultant = $_POST['consultant'];

            $query = "INSERT INTO Program (ProgramId) VALUES (?)";

            $stmt = $db->prepare($query);

            $stmt->bind_param("s", $id);

            $stmt->execute();
            $stmt->store_result();

            if ($stmt->affected_rows == 1) {
                $stmt->close();

                $query = "INSERT INTO LookupProgramTitle (ProgramTitle) VALUES (?)";

                $stmt = $db->prepare($query);

                $stmt->bind_param("s", $title);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows == 1) {
                    $titleId = $stmt->insert_id;

                    $stmt->close();

                    $query = "INSERT INTO ProgramCatalogYear (ProgramId, CatalogYearId, ProgramTitleId, ProgramMinimum, ProgramManagerPersonId) VALUES (?, (SELECT CatalogYearId FROM LookupCatalogYear WHERE CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)), ?, ?, ?)";

                    $stmt = $db->prepare($query);

                    $stmt->bind_param("siis", $id, $titleId, $minimum, $manager);

                    $stmt->execute();
                    $stmt->store_result();

                    if ($stmt->affected_rows == 1) {


                        $programCatalogYearId = $stmt->insert_id;

                        $stmt->close();

                        $query = "INSERT INTO ProgramCatalogYearConsultant (ConsultantPersonId, ProgramCatalogYearId) VALUES (?, ?)";

                        $stmt = $db->prepare($query);

                        $stmt->bind_param("ii", $consultant, $programCatalogYearId);

                        $stmt->execute();
                        $stmt->store_result();

                        if ($stmt->affected_rows == 1) {
                            $message = "Program added to database.";
                        } else {
                            $message = "Could not add Consultant to database.";
                        }
                    } else {
                        $message = "Could not add Program Catalog Year";
                    }
                } else {
                    $message = "Could not add Program Title";
                }
            }
            else {
                $message = "Could not add Program ID";
            }
        }
    }
?>


<form action="AddProgramForm.php" method="post" class="form-group">
    <table class="table table-bordered">
        <tr>
            <td colspan="2"><h2>Add Program</h2></td>
        </tr>
        <tr>
            <td><label for="programId">Program ID</label></td>
            <td><input type="text" id="programId" name="programId" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="programTitle">Program Title</label></td>
            <td><input type="text" id="programTitle" name="programTitle" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="programMinimum">Program Minimum</label></td>
            <td><input type="number" id="programMinimum" name="programMinimum" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="programManager" class="form-text">Program Manager</label></td>
            <td><label for="consultant" class="form-text">Consultant</label></td>
        </tr>
        <tr>
            <td>
                <select id="programManager" name="programManager" class="form-control-sm">
                    <?php
                        foreach($programManagers AS $key => $value) {
                            echo '<option value="'.$id.'">'.$value.'</option>';
                        }
                    ?>
                </select>
            </td>
            <td>
                <select id="consultant" name="consultant" class="form-control-sm">
                    <?php
                        foreach($consultants AS $key => $value) {
                            echo '<option value="'.$id.'">'.$value.'</option>';
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Submit" class="form-control"></td>
        </tr>
    </table>





    <?php
        if (isset($message)) {
            echo '<p class="alert-warning">'.$message.'</p>';
        }
    ?>
</form>

<?php

include("../../Bootstrap/incFootPage.php");


<?php
/**
 * EditProgramForm.php
 *
 * Allows a user to edit a program in the database
 *
 * @author twhiten
 * @since 2021/04/16
 */



session_start();

if(!isset($_SESSION['userType']) || $_SESSION['userType'] != 2){
    header("location: ../Reports/ProgramList.php");
    exit;
}

require("../../Bootstrap/incPageHead.php");
//Check if user is a logged in admin.




$id = null;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
else if (isset($_POST['id'])) {
    $id = $_POST['id'];
}
else {
    header("location: ../Reports/ProgramList.php");
    exit;
}

    $message = null;
    $consultants = array();
    $programManagers = array();

    //Obtain available consultants and program managers
    $query = 'SELECT personId, concat(personFirstName," ",personLastName) AS personName, isProgramManager, isConsultant FROM Person WHERE Active AND (isProgramManager OR isConsultant)';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $name, $isManager, $isConsultant);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            if ($isManager) {
                $programManagers[$id] = $name;
            }

            if ($isConsultant) {
                $consultants[$id] = $name;
            }
        }
    }

    if (isset($_POST['programId'])) {
        $id = null;
        $title = null;
        $minimum = null;
        $manager = null;
        $consultant = null;


        if (empty($_POST['programId']) || empty($_POST['programTitle']) || empty($_POST['programMinimum']) || empty($_POST['programManager']) || empty($_POST['consultant'])) {
            $message = "Please fill all fields";
        }
        else {
            $programId = $_POST['programId'];
            $title = $_POST['programTitle'];
            $minimum = $_POST['programMinimum'];
            $manager = $_POST['programManager'];
            $consultant = $_POST['consultant'];

            $query = "UPDATE Program SET ProgramId = ? WHERE ProgramId = (SELECT ProgramId FROM ProgramCatalogYear WHERE ProgramCatalogYearId = ?)";

            $stmt = $db->prepare($query);

            $stmt->bind_param("ss", $programId, $id);

            $stmt->execute();
            $stmt->store_result();

            if ($stmt->affected_rows == 1) {
                $stmt->close();

                $query = "UPDATE LookupProgramTitle SET ProgramTitle = ? WHERE ProgramTitleId = (SELECT ProgramTitleId FROM ProgramCatalogYear WHERE ProgramCatalogYearId = ?)";

                $stmt = $db->prepare($query);

                $stmt->bind_param("ss", $title, $id);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows == 1) {
                    $titleId = $stmt->insert_id;

                    $stmt->close();

                    $query = "UPDATE ProgramCatalogYear SET ProgramId = ?, ProgramMinimum = ?, ProgramManagerPersonId = ? WHERE ProgramCatalogYearId = ?";

                    $stmt = $db->prepare($query);

                    $stmt->bind_param("siis", $programId, $minimum, $manager, $id);

                    $stmt->execute();
                    $stmt->store_result();

                    if ($stmt->affected_rows == 1) {


                        $programCatalogYearId = $stmt->insert_id;

                        $stmt->close();

                        $query = "UPDATE ProgramCatalogYearConsultant SET ConsultantPersonId = ? WHERE ProgramCatalogYearId = ?";

                        $stmt = $db->prepare($query);

                        $stmt->bind_param("ii", $consultant, $programCatalogYearId);

                        $stmt->execute();
                        $stmt->store_result();

                        if ($stmt->affected_rows == 1) {
                            $message = "Program added to database.";
                        } else {
                            $message = "Could not add Program Consultant ID to database.";
                        }
                    } else {
                        $message = "Could not add Program Catalog Year";
                    }
                } else {
                    $message = "Could not add Program Title";
                }
            }
            else {
                $message = "Could not add Program ID";
            }
        }
    }

    //Get program information
    $query = "SELECT ProgramId, ProgramTitle, ProgramMinimum, ProgramManagerPersonId, ConsultantPersonId FROM ProgramCatalogYear INNER JOIN ProgramCatalogYearConsultant ON ProgramCatalogYearConsultant.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON LookupProgramTitle.ProgramTitleId = ProgramCatalogYear.ProgramTitleId WHERE ProgramCatalogYear.ProgramCatalogYearID = ?";

    $stmt = $db->prepare($query);

    $stmt->bind_param("s", $id);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($programId, $programTitle, $programMinimum, $pmId, $cId);

    if ($stmt->num_rows == 1) {

        $stmt->fetch();

        echo '<form action="EditProgramForm.php" method="post" class="form-group">
    <table class="table table-bordered">
        <tr>
            <td colspan="2"><h2>Edit Program</h2></td>
            <input type="hidden" name="id" value="'.$id.'">;
        </tr>
        <tr>
            <td><label for="programId">Program ID</label></td>
            <td><input type="text" id="programId" name="programId" maxlength="255" class="form-control" value="'.$programId.'"></td>
        </tr>
        <tr>
            <td><label for="programTitle">Program Title</label></td>
            <td><input type="text" id="programTitle" name="programTitle" maxlength="255" class="form-control" value="'.$programTitle.'"></td>
        </tr>
        <tr>
            <td><label for="programMinimum">Program Minimum</label></td>
            <td><input type="number" id="programMinimum" name="programMinimum" class="form-control" value="'.$programMinimum.'"></td>
        </tr>
        <tr>
            <td><label for="programManager" class="form-text">Program Manager</label></td>
            <td><label for="consultant" class="form-text">Consultant</label></td>
        </tr>
        <tr>
            <td>
                <select id="programManager" name="programManager" class="form-control-sm">';
        foreach($programManagers AS $key => $value) {
            if ($key == $pmId) {
                echo '<option selected="selected" value="'.$key.'">'.$value.'</option>';
            }
            else {
                echo '<option value="'.$key.'">'.$value.'</option>';
            }

        }
        echo   '</select>
            </td>
            <td>
                <select id="consultant" name="consultant" class="form-control-sm">';
        foreach($consultants AS $key => $value) {
            if ($key == $cId) {
                echo '<option selected="selected" value="'.$key.'">'.$value.'</option>';
            }
            else {
                echo '<option value="'.$key.'">'.$value.'</option>';
            }

        }

        echo   '</select>
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Submit" class="form-control"></td>
        </tr>
    </table>';
    }

        if (isset($message)) {
            echo '<p class="alert-warning">'.$message.'</p>';
        }
echo '</form>';

include("../../Bootstrap/incFootPage.php");


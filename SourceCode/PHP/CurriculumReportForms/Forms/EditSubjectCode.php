<?php

/**
 * EditSubjectCode.php
 *
 * Allow an admin to edit a selected subject code.
 *
 * @author twhiten
 * @since 2021/04/15
 */

require ("../../Bootstrap/incPageHead.php");

if (isset($_SESSION['loggedIn']) && $_SESSION['userType'] != 2) {
    header("location: ../Reports/SubjectCodes.php");
    exit();
}

$id = null;
if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
else if (isset($_POST['id'])) {
    $id = $_POST['id'];
}
else {
    header("location: ../Reports/SubjectCodes.php");
    exit();
}

if (isset($_POST['code']) && !empty($_POST['code']) && strlen($_POST['code']) == 4 && isset($_POST['desc']) && !empty($_POST['desc'])) {
    $code = $_POST['code'];
    $desc = $_POST['desc'];

    $query = "UPDATE subjectCode SET FourLetterCode = ?, SubjectDescription = ? WHERE SubjectCodeId = ?";

    $stmt = $db->prepare($query);

    $stmt->bind_param("sss", $code, $desc, $id);

    $stmt->execute();
    $stmt->store_result();

    if ($stmt->affected_rows == 1) {
        $stmt->close();

        header("location: ../Reports/SubjectCodes.php");
        exit();
    }
}

//Get information about selected subject code

$code = null;
$desc = null;

$query = "SELECT FourLetterCode, SubjectDescription FROM SubjectCode WHERE SubjectCodeId = ?";

$stmt = $db->prepare($query);

$stmt->bind_param("s", $id);

$stmt->execute();
$stmt->store_result();

if ($stmt->num_rows == 1) {
    $stmt->bind_result($c, $d);

    $code = $c;
    $desc = $d;

    $stmt->fetch();

    $stmt->close();


}


echo '<form action="EditSubjectCode.php" method="post" class="form-group">
    <table class="table table-bordered">
        <tr>
            <td colspan="2" class="thead-light"><h2>Edit Subject Code</h2></td>
            <input type="hidden" name="id" value="'.$id.'">
        </tr>
        <tr>
            <td><label for="code">Four Letter Code</label></td>
            <td><label for="desc">Description</label></td>
        </tr>
        <tr>
            <td><input type="text" id="code" name="code" maxlength="4" class="form-control" value="'.$code.'"></td>
            <td><input type="text" id="desc" name="desc" maxlength="255" class="form-control" value="'.$desc.'"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Submit" class="btn btn-primary"></td>
        </tr>
    </table>';


echo '</form>';

require ("../../Bootstrap/incFootPage.php");

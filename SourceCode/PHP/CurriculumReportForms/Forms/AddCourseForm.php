<?php
/**
 * AddCourseForm.php
 *
 * Step one of adding a course to the database. When properly filled out, user will be sent through
 * each of the forms in the CourseDetailForms folder to add additional information.
 *
 * @author twhiten
 * @since 2021/03/12
 */

    require("../Entities/Course.php");
    require("../../Bootstrap/incPageHead.php");

    //Check if the user is a logged in Admin
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION['userType'] != 2){
        header("location: ../../UserAccessPHP/UserAccess/welcome.php");
        exit;
    }

    //Remove course object from session if it exists
    if (isset($_SESSION['course'])) {
        unset($_SESSION['course']);
    }

    $message = null;
    $consultants = array();
    $people = array();
    $programManagers = array();
    $instrMethods = array();
    $schemes = array();
    $maxYear = null;

    //Obtain necessary information to populate fields
    if (isset($_GET['message'])) {
        $message = $_GET['message'];
    }

    $query = 'SELECT personId, concat(personFirstName," ",personLastName) AS personName, isProgramManager, isConsultant FROM Person WHERE Active';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $name, $isManager, $isConsultant);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {

            $people[$id] = $name;

            if ($isManager) {
                $programManagers[$id] = $name;
            }

            if ($isConsultant) {
                $consultants[$id] = $name;
            }
        }
    }

    $stmt->close();

    $query = "SELECT instructionalMethodId, instructionalMethodName FROM lookupInstrMethod WHERE Active";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($methodId, $methodName);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $instrMethods[$methodId] = $methodName;
        }
    }

    $stmt->close();

    $query = "SELECT GradeSchemeId, GradeSchemeName FROM LookupGradeScheme";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($schemeId, $schemeName);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $schemes[$schemeId] = $schemeName;
        }
    }

    $query = "SELECT MAX(CatalogYearName) FROM LookupCatalogYear";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($latestYear);

    $stmt->fetch();

    $maxYear = $latestYear;



?>

<form action="../Subforms/CourseAddRequisites.php" method="post" class="form-group">
    <table class="table table-bordered">
        <tr class="thead-dark">
            <th colspan="4" class="thead-light"><h2 class="form-text">Add Course</h2><p class="form-text">Required fields are in <span class="alert-danger">red</span></p></th>
        </tr>
        <tr>
            <td colspan="2"><label for="courseCode"><span class="alert-danger form-text">Course Code</span></label> <input type="text" id="courseCode" name="courseCode" maxlength="255" class="form-control"></td>
            <td colspan="2"><label for="courseTitle"><span class="alert-danger form-text">Course Title</span></label> <input type="text" id="courseTitle" name="courseTitle" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="4"><label for="courseDescription" class="form-text">Course Description</label> <input type="text" id="courseDescription" name="courseDescription" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="gradeScheme" class="required"><span class="alert-danger">Grade Scheme</span></label>
                <select id="gradeScheme" name="gradeScheme" class="form-control-sm">
                    <?php
                    foreach($schemes AS $key => $value) {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                    }
                    ?>
                </select>
            </td>
            <td colspan="2"><label for="minimumGrade">Minimum Grade</label> <input type="number" id="minimumGrade" name="minimumGrade" min="0" max="100" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="instructionalMethod">Instructional Methods</label></td>
            <td colspan="2">
                <select id="instructionalMethod" name="instructionalMethod" class="form-control-sm">
                    <?php
                    foreach($instrMethods AS $key => $value) {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr class="thead-light">
            <td>Course Value</td>
            <td><label for="outcomeHours">Outcome Hours</label></td>
            <td><label for="credits">Credits</label></td>
            <td><label for="hours">Hours</label></td>
        </tr>
        <tr>
            <td/>
            <td><input type="number" id="outcomeHours" name="outcomeHours" class="form-control"></td>
            <td><input type="text" id="credits" name="credits" class="form-control"></td>
            <td><input type="number" id="hours" name="hours" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="assessmentNote">Assessment Note</label></td>
            <td colspan="2"><input type="text" id="assessmentNote" name="assessmentNote" class="form-control"></td>
        </tr>
        <tr>
            <td>
                <label for="researchComponent">Is there a research component?</label>
                <input type="checkbox" id="researchComponent" name="researchComponent" class="form-check-inline">
            </td>
            <td>
                <label for="newCourse">Is this a new course?</label>
                <input type="checkbox" id="newCourse" name="newCourse" checked="checked" class="form-check-inline">
            </td>
            <td>
                <label for="openStudies">Open Studies</label>
                <input type="checkbox" id="openStudies" name="openStudies" class="form-check-inline">
            </td>
            <td>
                <label for="plar">Plar</label>
                <input type="checkbox" id="plar" name="plar" class="form-check-inline">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label for="replacingCourse">Is this replacing an existing course?</label>
                <input type="checkbox" id="replacingCourse" name="replacingCourse" class="form-check-inline">
            </td>
            <td colspan="2"><label for="retiredDate">Retired Date:</label> <input type="date" id="retiredDate" name="retiredDate" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="originalCatYear">Catalog Year of Original Course Implementation</label></td>
            <td colspan="2"><input type="number" min="2014" max="<?php$maxYear?>" id="originalCatYear" name="originalCatYear" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="currentVersionCat">Catalog Year of Current Version Implementation</label></td>
            <td colspan="2"><input type="number" min="2014" max="<?php$maxYear?>" id="currentVersionCat" name="currentVersionCat" class="form-control"></td>
        </tr>
        <tr>
            <td>
                <label for="revisionLevel">Revision Level</label>
                <input type="text" id="revisionLevel" name="revisionLevel" class="form-control">
            </td>
            <td>
                <label for="version">Version</label>
                <input type="text" id="version" name="version" class="form-control">
            </td>
            <td>
                <label for="revisionDate">Date</label>
                <input type="date" id="revisionDate" name="revisionDate" class="form-control">
            </td>
            <td>
                <label for="authorizedBy">Authorized By</label><br>
                <select id="authorizedBy" name="authorizedBy" class="form-control-sm">
                    <?php
                    foreach($people AS $key => $value) {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="4"><label for="supportingDocuments">Supporting Documents</label> <input type="text" name="supportingDocuments" id="supportingDocuments" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="4"><label for="additionalInformation">Additional Information</label> <input type="text" name="additionalInformation" id="additionalInformation" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="subjectMatterExperts">Subject Matter Experts</label></td>
            <td colspan="2"><input type="text" id="subjectMatterExperts" name="subjectMatterExperts" maxlength="255" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="programManager">Approved By (Program Manager)</label></td>
            <td>
                <select id="programManager" name="programManager" class="form-control-sm">
                    <?php
                    foreach($programManagers AS $key => $value) {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                    }
                    ?>
                </select>
            </td>
            <td><label for="approvedDate">Approved Date</label></td>
            <td><input type="date" id="approvedDate" name="approvedDate" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="consultant">Approved By (Consultant)</label></td>
            <td>
                <select id="consultant" name="consultant" class="form-control-sm">
                    <?php
                    foreach($consultants AS $key => $value) {
                        echo '<option value="'.$key.'">'.$value.'</option>';
                    }
                    ?>
                </select>
            </td>
            <td><label for="approvedDateCC">Approved Date</label></td>
            <td><input type="date" id="approvedDateCC" name="approvedDateCC" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2">Essential Skills</td>
            <td colspan="2">Course Hours By Type</td>
        </tr>
        <tr>
            <td><label for="esReading">Reading</label></td>
            <td><input type="checkbox" id="esReading" name="esReading" class="form-check-inline"></td>
            <td><label for="hoursClass">Class Hours</label></td>
            <td><input type="number" id="hoursClass" name="hoursClass" min="0" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="esDocument">Document Use</label></td>
            <td><input type="checkbox" id="esDocument" name="esDocument" class="form-check-inline"></td>
            <td><label for="hoursLab">Lab Hours</label></td>
            <td><input type="number" id="hoursLab" name="hoursLab" min="0" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="esNumeracy">Numeracy</label></td>
            <td><input type="checkbox" id="esNumeracy" name="esNumeracy" class="form-check-inline"></td>
            <td><label for="hoursClinical">Clinical Hours</label></td>
            <td><input type="number" id="hoursClinical" name="hoursClinical" min="0" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="esWriting">Writing</label></td>
            <td><input type="checkbox" id="esWriting" name="esWriting" class="form-check-inline"></td>
            <td><label for="hoursOJT">OJT Practicum Hours</label></td>
            <td><input type="number" id="hoursOJT" name="hoursOJT" min="0" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="esThinking">Thinking</label></td>
            <td><input type="checkbox" id="esThinking" name="esThinking" class="form-check-inline"></td>
            <td><label for="hoursIntern">Internship Hours</label></td>
            <td><input type="number" id="hoursIntern" name="hoursIntern" min="0" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="esOral">Oral</label></td>
            <td><input type="checkbox" id="esOral" name="esOral" class="form-check-inline"></td>
            <td colspan="2">Delivery Modalities</td>
        </tr>
        <tr>
            <td><label for="esWorking">Working</label></td>
            <td><input type="checkbox" id="esWorking" name="esWorking" class="form-check-inline"></td>
            <td><label for="modalityFace">Face</label></td>
            <td><input type="checkbox" id="modalityFace" name="modalityFace" class="form-check-inline"></td>
        </tr>
        <tr>
            <td><label for="esCompUse">Comp Use</label></td>
            <td><input type="checkbox" id="esCompUse" name="esCompUse" class="form-check-inline"></td>
            <td><label for="modalityDistance">Distance</label></td>
            <td><input type="checkbox" id="modalityDistance" name="modalityDistance" class="form-check-inline"></td>
        </tr>
        <tr>
            <td><label for="esContLearning">Continuous Learning</label></td>
            <td><input type="checkbox" id="esContLearning" name="esContLearning" class="form-check-inline"></td>
            <td><label for="modalityHybrid">Hybrid</label></td>
            <td><input type="checkbox" id="modalityHybrid" name="modalityHybrid" class="form-check-inline"></td>
        </tr>
        <tr>
            <td colspan="4"><input type="submit" value="Submit" class="form-control"></td>
        </tr>
        <?php
        if (isset($message)) {
            echo '<tr>
                    <td colspan="4" class="alert-warning">'.$message.'</td>
                  </tr>';
        }
        ?>
    </table>






</form>

<?php

require("../../Bootstrap/incFootPage.php");


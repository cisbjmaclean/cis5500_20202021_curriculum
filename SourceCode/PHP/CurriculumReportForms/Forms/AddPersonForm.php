<?php
/**
 * AddPersonForm.php
 *
 * Allows a user to add a person to the database
 *
 * @author twhiten
 * @since 2021/03/12
 */



require("../../Bootstrap/incPageHead.php");

//Check if user is a logged in admin.
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true || $_SESSION['userType'] != 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}

    $message = null;

    if (isset($_POST['personFName'])) {
        $firstName = $_POST['personFName'];
        $lastName = $_POST['personLName'];


        if (empty($firstName) || empty($lastName)) {
            $message = "Please enter a name";
        }
        else {
            $programManager = isset($_POST['programManager']);
            $consultant = isset($_POST['consultant']);

            $query = "INSERT INTO Person (PersonFirstName, PersonLastName, IsProgramManager, IsConsultant, Active) VALUES (?, ?, ?, ?, ?)";

            $stmt = $db->prepare($query);

            if ($stmt != false) {
                $stmt->bind_param("ssiii", $firstName, $lastName, $programManager, $consultant, true);

                $stmt->execute();
                $stmt->store_result();

                if ($stmt->affected_rows == 1) {
                    $message = $firstName . " " . $lastName . " successfully added";
                } else {
                    $message = "Could not add person to database";
                }
            }
            else {
                $message = "Query creation failed";
            }

        }
    }
?>

<form action="AddPersonForm.php" method="post" class="form-group">
    <table class="table table-bordered">
        <tr>
            <td colspan="2" class="thead-light"><h2>Add Person</h2></td>
        </tr>
        <tr>
            <td colspan="2"><label for="personFName">Name</label><label for="personLName">(First/Last)</label></td>
        </tr>
        <tr>
            <td><input type="text" id="personFName" name="personFName" maxlength="40" class="form-control"></td>
            <td><input type="text" id="personLName" name="personLName" maxlength="40" class="form-control"></td>
        </tr>
        <tr>
            <td><label for="programManager">Program Manager</label></td>
            <td><input type="checkbox" name="programManager" id="programManager" maxlength="40" class="form-check-inline"></td>
        </tr>
        <tr>
            <td><label for="consultant">Consultant</label></td>
            <td><input type="checkbox" name="consultant" id="consultant" class="form-check-inline"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Submit" class="form-control-sm"></td>
        </tr>
    </table>





    <?php
        if (isset($message)) {
            echo '<p class="alert-warning">'.$message.'</p>';
        }
    ?>
</form>

<?php

include("../../Bootstrap/incFootPage.php");
?>

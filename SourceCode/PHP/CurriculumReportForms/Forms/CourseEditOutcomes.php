<?php
/**
 * CourseEditOutcomes.php
 *
 * Allow user to update a course's outcome and competency lists.
 *
 * @author twhiten
 * @since 2021/03/16
 */


    require("../../Bootstrap/incPageHead.php");




//Declare variables

$outcomeList = array();
$competencyList = array(array());

//Get id for selected course
$courseCatalogYearId = null;
if (isset($_GET['course'])) {
    $courseCatalogYearId = $_GET['course'];
}
else {
    $courseCatalogYearId = $_POST['course'];
}

//Check user action on this form
if (isset($_GET['outcome'])) {

    //Delete a selected outcome

    $deletedOutcome = $_GET['outcome'];

    $deleteQuery = "DELETE FROM LearningCompetency WHERE LearningOutcomeId = ?";

    $stmt = $db->prepare($deleteQuery);

    $stmt->bind_param("s", $deletedOutcome);

    $stmt->execute();

    $stmt->close();

    $deleteQuery = "DELETE FROM LearningOutcome WHERE LearningOutcomeId = ?";

    $stmt = $db->prepare($deleteQuery);

    $stmt->bind_param("s", $deletedOutcome);

    $stmt->execute();

    $stmt->close();

}
else if(isset($_GET['competency'])) {
    //Delete a competency from the form
    $deletedComp = $_GET['competency'];

    $deleteQuery = "DELETE FROM LearningCompetency WHERE LearningCompetencyId = ?";

    $stmt = $db->prepare($deleteQuery);

    $stmt->bind_param("s", $deletedComp);

    $stmt->execute();

    $stmt->close();

}
else if(isset($_POST['newComp'])) {
    //Add a competency to an outcome.

    $outcome = $_POST['outcome'];
    $newComp = $_POST['newComp'];

    $query = "INSERT INTO LearningCompetency (LearningCompetencyName, LearningOutcomeId) VALUES (?, ?)";

    $stmt = $db->prepare($query);

    $stmt->bind_param("ss", $newComp, $outcome);

    $stmt->execute();

    $stmt->close();
}
else if(isset($_POST['newOutcome'])) {

    $newOutcome = $_POST['newOutcome'];

    $query = "INSERT INTO LearningOutcome (LearningOutcomeName, CourseCatalogYearId) VALUES (?, ?)";

    $stmt = $db->prepare($query);

    $stmt->bind_param("ss", $newOutcome, $courseCatalogYearId);

    $stmt->execute();

    $stmt->close();
}

$outcomeList = array();
$competencyList = array(array());


//Get outcomes from database

$query = 'SELECT LearningOutcomeId, LearningOutcomeName, LearningCompetencyId, LearningCompetencyName FROM learningoutcome INNER JOIN learningcompetency ON learningoutcome.learningOutcomeId = learningcompetency.learningoutcomeid WHERE courseCatalogYearId = ?';

$stmt = $db->prepare($query);

$stmt->bind_param("s", $courseCatalogYearId);

$stmt->execute();

$stmt->bind_result($outcomeId, $outcome, $compId, $competency);

if ($stmt->num_rows > 0) {
    $currentOutcome = null;


    while ($stmt->fetch()) {
        if ($currentOutcome != $outcome) {
            $outcomeList[$outcomeId] = $outcome;

            $currentOutcome = $outcome;
        }


        $competencyList[$outcomeId][$compId] = $competency;
    }
}

?>
    <table class="table table-bordered">
        <tr class="thead-dark">
            <th colspan="3"><h1>Add Learning Outcomes</h1></th>
        </tr>
        <?php



        if (!empty($outcomeList)) {
            foreach ($outcomeList as $outcomeKey => $outcomeValue) {
                echo '<tr class="thead-light">
                    <th colspan="2">' . $outcomeValue . '</th>
                    <th><a href="CourseEditOutcomes.php?outcome=' . $outcomeKey . '">Remove Outcome</a></th>
                    <th></th>
                  </tr>';


                if (!empty($competencyList[$outcomeKey])) {
                    foreach ($competencyList[$outcomeKey] AS $compKey => $compValue) {
                        echo '<tr>
                        <td></td>
                        <td colspan="2">' . $compValue . '</td>
                        <td><a href="CourseEditOutcomes.php?competency=' . $compKey . '&course='.$courseCatalogYearId.'">Remove Competency</a></td>
                      </tr>';
                    }
                }


                echo '<form action="CourseEditOutcomes.php" method="post">
                    <tr>
                        <input type="hidden" name="course" value="'.$courseCatalogYearId.'">
                        <td><input type="hidden" name="outcome" id="outcome" value="' . $outcomeKey . '"></td>
                        <td colspan="2"><input type="text" name="newComp" id="newComp"></td>
                        <td><input type="submit" value="Add Competency"></td>
                    </tr>
                  </form>';
            }
        }
        echo '<form action="CourseEditOutcomes.php" method="post">
            <tr>
                <input type="hidden" name="course" value="'.$courseCatalogYearId.'"
                <td><label for="newOutcome">Add new outcome</label></td>
                <td colspan="2"><input type="text" name="newOutcome" id="newOutcome"></td>
                <td><input type="submit" value="Add Outcome"></td>
            </tr>
        </form>
        <tr>
            <td colspan="4"><a href="../Reports/QF132CourseOutline.php">Finish</a></td>
        </tr>';
            if (isset($message)) {
                    echo '<tr>
                            <td colspan="4">'.$message.'</td>
                          </tr>';
            }
    echo '</table>';




require("../../Bootstrap/incFootPage.php");
<?php
/**
 * CourseEditReplacements.php
 *
 * Allow user to update information on replacement courses
 *
 * @author twhiten
 * @since 2021/04/14
 */


require("../../Bootstrap/incPageHead.php");



//Get id for selected course
$courseCatalogYearId = null;
if (isset($_GET['course'])) {
    $courseCatalogYearId = $_GET['course'];
}
else {
    $courseCatalogYearId = $_POST['course'];
}


if (isset($_POST['oldCourseCode'])) {
    $courseCode = $_POST['oldCourseCode'];

    if (!empty($courseCode)) {

        $query = "SELECT CourseId, CourseTitle FROM CourseCatalogYear WHERE CourseId LIKE ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $courseCode);

        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($oldCode, $oldTitle);

        if ($stmt->num_rows > 0) {
            $stmt->fetch();

            $id = $oldCode;
            $title = $oldTitle;

            $stmt->close();

            //Check if the selected course is already a replacement for that course

            $query = "SELECT * FROM CourseReplacement WHERE OldCourseId = ? AND NewCourseId = (SELECT CourseId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?)";

            $stmt = $db->prepare($query);

            $stmt->bind_param("ss", $oldCode, $courseCatalogYearId);

            $stmt->execute();
            $stmt->store_result();

            if ($stmt->num_rows = 0) {
                $stmt->close();

                $query = "INSERT INTO CourseReplacement (OldCourseId, OldCourseTitle, NewCourseId) VALUES (?, ?, (SELECT CourseId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?))";

                $stmt = $db->prepare($query);

                $stmt->bind_param("sss", $id, $title, $courseCatalogYearId);

                $stmt->execute();
            }
            else {
                $message = "That course is already in the list of replacements";
            }
        }
        else {
            $message = "No course with that ID found";
        }

        $stmt->close();
    }
    else {
        $message = "Please properly fill field";
    }
}
else if (isset($_GET['delete'])) {

    $deletedReplacement = $_GET['delete'];

    $deleteQuery = "DELETE FROM CourseReplacement WHERE CourseReplacementId = ?";

    $stmt = $db->prepare($deleteQuery);

    $stmt->bind_param("s", $deletedReplacement);

    $stmt->execute();

    $stmt->close();
}


//Get courses from database
$query = "SELECT CourseReplacementId, OldCourseId, OldCourseTitle FROM CourseReplacement WHERE NewCourseId = (SELECT CourseId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?)";

$stmt = $db->prepare($query);

$stmt->bind_param("s", $courseCatalogYearId);

$stmt->execute();
$stmt->store_result();

$stmt->bind_result($courseReplacementId, $courseId, $courseTitle);
$courseReplacement = null;

if ($stmt->num_rows > 0) {
    while ($stmt->fetch()) {
        $courseReplacement[$courseReplacementId] = array('courseId' => $courseId, 'courseTitle' => $courseTitle);
    }
}

$stmt->close();

?>

<form action="CourseEditReplacements.php" method="post">
    <table class="table table-bordered">
        <tr class="thead-dark">
            <th colspan="3"><h1>Course Replacements</h1></th>
        </tr>
        <tr class="thead-light">
            <th><label for="oldCourseCode">Old Course ID</label></th>
            <th colspan="2"><label for="oldCourseTitle">Old Course Title</label></th>
        </tr>
        <?php
        if (!empty($courseReplacement)) {
            foreach($courseReplacement AS $key => $array) {
                echo '<tr>
                        <td>'.$array['courseId'].'</td>
                        <td>'.$array['courseTitle'].'</td>
                        <td><a href="CourseEditReplacements.php?delete='.$key.'&course='.$courseCatalogYearId.'">Remove replacement</a></td>
                      </tr>';
            }
        }
        echo '<tr>
            <td>Add ID of old course</td>
            <td colspan="2"><input type="text" name="oldCourseCode" id="oldCourseCode" class="form-control"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Add Replacement"></td>
            <td colspan="2"><a href="../Reports/QF132CourseOutline.php">Finish</a></td>
        </tr>';
            if (isset($message)) {
                    echo '<tr>
                            <td colspan="3">'.$message.'</td>
                          </tr>';
            }
    echo '</table>
</form>';




require("../../Bootstrap/incFootPage.php");
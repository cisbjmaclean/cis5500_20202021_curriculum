<?php
/**
 * CourseEditRequisites.php
 *
 * Allow user to edit a course's requisites.
 *
 * @author twhiten
 * @since 2021/04/14
 */




require("../../Bootstrap/incPageHead.php");


//Declare variables

$course = null;
$requisites = array(array());

//Get id for selected course
$courseCatalogYearId = null;
if (isset($_GET['course'])) {
    $courseCatalogYearId = $_GET['course'];
}
else if (isset($_POST['course'])) {
    $courseCatalogYearId = $_POST['course'];
}
else {
    header("location: ../Reports/QF132CourseOutline.php");
    exit;
}


if (isset($_POST['requisiteCourseCode']) && isset($_POST['requisiteType'])) {
    //Add a new requisite to the database
    $code = $_POST['requisiteCourseCode'];
    $type = $_POST['requisiteType'];

    if (!empty($newCode) && !empty($newType)) {
        $query = "SELECT CourseTitle FROM CourseCatalogYear WHERE CourseId LIKE ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $newCode);

        $stmt->execute();
        $stmt->store_result();

        $stmt->bind_result($newTitle);

        if ($stmt->num_rows > 0) {
            $stmt->fetch();
            $title = $newTitle;
            $stmt->close();

            //Check if the current course already has that course as a requisite.

            $query = "SELECT CourseRequisiteId FROM CourseRequisite WHERE ParentCourseId = (SELECT CourseId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?) AND CatalogYearId = (SELECT CatalogYearId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?)";
            $stmt = $db->prepare($query);

            $stmt->bind_param("ss", $courseCatalogYearId, $courseCatalogYearId);

            $stmt->execute();
            $stmt->store_result();

            $stmt->bind_result($requisiteId);

            if ($stmt->num_rows == 1) {
                $stmt->fetch();

                $req = $requisiteId;

                $stmt->close();

                $updateQuery = "UPDATE CourseRequisite SET RequisiteTypeId = ? WHERE CourseRequisiteId = ?";

                $stmt = $db->prepare($updateQuery);

                $stmt->bind_param("ss", $type, $req);

                $stmt->execute();

                $stmt->close();
            }
            else {
                $insertQuery = "INSERT INTO CourseRequisite (ChildCourseId, ChildCourseTitle, RequisiteTypeId, ParentCourseId, CatalogYearId) VALUES (?, ?, ?, (SELECT CourseId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?), (SELECT CatalogYearId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?))";

                $stmt = $db->prepare($insertQuery);

                $stmt->bind_param("sssss", $code, $title, $type, $courseCatalogYearId, $courseCatalogYearId);

                $stmt->execute();

                $stmt->close();
            }

            
        }
        else {
            $message = "No course with that ID found";
        }
    }
    else if (empty($newCode) || empty($newType)) {
        $message = "Please fill out all fields";
    }
}
else if (isset($_GET['delete'])) {

    $delete = $_GET['delete'];

    $deleteQuery = "DELETE FROM CourseRequisite WHERE CourseRequisiteId = ?";

    $stmt = $db->prepare($deleteQuery);

    $stmt->bind_param("s", $delete);

    $stmt->execute();

    $stmt->close();



}


    //Get requisite options
    $reqOptions = array();

    $query = "SELECT RequisiteId, RequisiteName FROM LookupRequisiteType";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $name);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $reqOptions[$id] = $name;
        }
    }

    $stmt->close();


    $requisites = null;

    //Get requisites from database
    $query = "SELECT CourseRequisiteId, ChildCourseTitle, ChildCourseId, RequisiteTypeId FROM CourseRequisite WHERE ParentCourseId = (SELECT CourseId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?) AND CatalogYearId = (SELECT CatalogYearId FROM CourseCatalogYear WHERE CourseCatalogYearId = ?)";

    $stmt = $db->prepare($query);

    $stmt->bind_param("ss", $courseCatalogYearId, $courseCatalogYearId);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($requisiteId, $childTitle, $childId, $typeId);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $requisites[$requisiteId] = array('childTitle' => $childTitle, 'childId' => $childId, 'type' => $typeId);
        }
    }


echo '<form action="CourseEditRequisites.php" method="post">
    <table class="table table-bordered">
        <tr>
            <th colspan="4">Add Requisites</th>
        </tr>
        <tr class="thead-light">
            <th><label for="requisiteCourseCode">Requisite Course Code</label></th>
            <th><label for="requisiteCourseTitle">Requisite Course Title</label></th>
            <th><label for="requisiteType">Type</label></th>
            <th></th>
        </tr>';


        if (!empty($requisites)) {
            foreach ($requisites AS $key => $array) {
                echo '<tr>
                        <td>'.$array['childId'].'</td>
                        <td>'.$array['childTitle'].'</td>
                        <td>'.$reqOptions[$array['type']].'</td>
                        <td><a href="CourseEditRequisites.php?delete='.$key.'&course='.$courseCatalogYearId.'">Remove Requisite</a></td>
                      </tr>';
            }
        }
        echo '<tr>
            <td colspan="2"><input type="text" name="requisiteCourseCode" id="requisiteCourseCode"></td>
            <td colspan="2">
                <select id="requisiteType" name="requisiteType">';
                        foreach($reqOptions AS $key => $value) {
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
          echo '</select>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="course" value="'.$courseCatalogYearId.'">
            <td colspan="2"><input type="submit" value="Add Requisite"></td>
            <td colspan="2"><a href="../Reports/QF132CourseOutline.php">Finish</a></td>
        </tr>';
            if (isset($message)) {
                    echo '<tr>
                            <td colspan="4">'.$message.'</td>
                          </tr>';
            }
    echo '</table>
</form>';




require("../../Bootstrap/incFootPage.php");
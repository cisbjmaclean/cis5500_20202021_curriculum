<?php
/**
 * EditCourseForm.php
 *
 * Obtains a selected course from the database and allows a user to update it.
 *
 * @author twhiten
 * @since 2021/04/12
 */


require("../../Bootstrap/incPageHead.php");


    //Check if the user is a logged in Admin
    if(!isset($_SESSION["userType"]) ||  $_SESSION['userType'] != 2){
        header("location: ../Reports/QF132CourseOutline.php");
        exit;
    }

if (isset($_POST['courseCatalogYearId']) && isset($_POST['courseTitle']) && !empty($_POST['courseTitle'])) {




    $courseCatalogYearId = $_POST['courseCatalogYearId'];
    $courseTitle = $_POST['courseTitle'];
    $courseDescription = $_POST['courseDescription'];
    $gradeScheme = $_POST['gradeScheme'];
    $minimumGrade = $_POST['minimumGrade'];
    $instructionalMethod = $_POST['instructionalMethod'];
    $outcomeHours = $_POST['outcomeHours'];
    $credits = $_POST['credits'];
    $hours = $_POST['hours'];
    $assessmentNote = $_POST['assessmentNote'];
    $research = isset($_POST['researchComponent']);
    $openStudies = isset($_POST['openStudies']);
    $plar = isset($_POST['plar']);
    $newCourse = isset($_POST['newCourse']);
    $replacingExisting = isset($_POST['replacingExisting']);
    $originalCatYear = $_POST['originalCatYear'];
    $currentVersionCat = $_POST['currentVersionCat'];
    $revisionLevel = $_POST['revisionLevel'];
    $revisionDate = $_POST['revisionDate'];
    $courseVersion = $_POST['version'];
    $authorizedBy = $_POST['authorizedBy'];
    $supportingDoc = $_POST['supportingDocuments'];
    $additionalInfo = $_POST['additionalInformation'];
    $subjectMatterExpert = $_POST['subjectMatterExperts'];
    $programManagers = $_POST['programManager'];
    $approvedDate = $_POST['approvedDate'];
    $approvedConsultant = $_POST['consultant'];
    $approvedCC = $_POST['approvedDateCC'];
    $retiredDate = $_POST['retiredDate'];
    $essentials = array('reading' => isset($_POST['esReading']), 'document' => isset($_POST['esDocument']), 'numeracy' => isset($_POST['esNumeracy']), 'writing' => isset($_POST['esWriting']), 'thinking' => isset($_POST['esThinking']), 'oral' => isset($_POST['esOral']), 'working' => isset($_POST['esWorking']), 'compUse' => isset($_POST['esCompUse']), 'contLearning' => isset($_POST['esContLearning']));
    $hourArray = array('class' => $_POST['hoursClass'], 'lab' => $_POST['hoursLab'], 'clinical' => $_POST['hoursClinical'], 'ojt' => $_POST['hoursOJT'], 'intern' => $_POST['hoursIntern']);
    $delivery = array('face' => isset($_POST['modalityFace']), 'distance' => isset($_POST['modalityDistance']), 'hybrid' => isset($_POST['modalityHybrid']));


    $query = "UPDATE CourseCatalogYear SET CourseTitle = ?, CourseDescription = ?, GradeSchemeId = ?";
    $query .= ", MinimumGrade = ?, OutcomeHours = ?, Credits = ?, Hours = ?, Research = ?, NewCourse = ?";
    $query .= ", ReplacingExisting = ?, OriginalCatYear = ?, CurrentVersionCat = ?, RevisionLevel = ?, CourseVersion = ?,";
    $query .= " RevisionDate = ?, AuthorizedByPersonId = ?, SupportingDoc = ?, AdditionalInfo = ?, SubjectMatterExpert = ?, ApprovedByPMPersonId = ?,";
    $query .= " ApprovedDate = ?, ApprovedByConsultantPersonId = ?, ApprovedDateCC = ?, InstructionalMethodId = ?,";
    $query .= " AssessmentNote = ?, OpenStudies = ?, Plar = ?, retiredDate = ?, ESReading = ?, ESDocumentUse = ?,";
    $query .= " ESNumeracy = ?, ESWriting = ?, ESThinking = ?, ESOral = ?, ESWorking = ?, ESCompUse = ?, ESContinuousLearning = ?,";
    $query .= " ClassHours = ?, LabHours = ?, ClinicalHours = ?, OJTPracticumHours = ?, InternshipHours = ?, ModalityFace = ?, ModalityDistance = ?, ModalityHybrid = ?";
    $query .= " WHERE CourseCatalogYearId = ?";

    $stmt = $db->prepare($query);

    $stmt->bind_param("ssiiiiiiiiiissiisssisisisiisiiiiiiiiiiiiiiiiii", $courseTitle, $courseDescription, $gradeScheme,
            $minimumGrade, $outcomeHours, $credits, $hours, $research, $newCourse, $replacingExisting, $originalCatYear, $currentVersionCat,
            $revisionLevel, $courseVersion, $revisionDate, $authorizedBy, $supportingDoc, $additionalInfo, $subjectMatterExpert,
            $approvedPM, $approvedDate, $approvedConsultant, $approvedCC, $instructionalMethod, $assessmentNote, $openStudies, $plar,
            $retiredDate, $essentials['reading'], $essentials['document'], $essentials['numeracy'], $essentials['writing'], $essentials['thinking'],
            $essentials['oral'], $essentials['working'], $essentials['compUse'], $essentials['contLearning'], $hourArray['class'], $hourArray['lab'],
            $hourArray['clinical'], $hourArray['ojt'], $hourArray['intern'], $delivery['face'], $delivery['distance'], $delivery['hybrid'],
            $courseCatalogYearId);

    $stmt->execute();

    $stmt->store_result();

    if ($stmt->affected_rows == 1) {
        header("location: ../Reports/QF132CourseOutline.php");
    }


}




    $message = null;
    $consultants = array();
    $people = array();
    $programManagers = array();
    $instrMethods = array();
    $schemes = array();
    $maxYear = null;

    //Obtain necessary information to populate fields
    if (isset($_GET['message'])) {
        $message = $_GET['message'];
    }

    $query = 'SELECT personId, concat(personFirstName," ",personLastName) AS personName, isProgramManager, isConsultant FROM Person WHERE Active';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($id, $name, $isManager, $isConsultant);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {

            $people[$id] = $name;

            if ($isManager) {
                $programManagers[$id] = $name;
            }

            if ($isConsultant) {
                $consultants[$id] = $name;
            }
        }
    }

    $stmt->close();

    $query = "SELECT instructionalMethodId, instructionalMethodName FROM lookupInstrMethod WHERE Active";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($methodId, $methodName);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $instrMethods[$methodId] = $methodName;
        }
    }

    $stmt->close();

    $query = "SELECT GradeSchemeId, GradeSchemeName FROM LookupGradeScheme";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($schemeId, $schemeName);

    if ($stmt->num_rows > 0) {
        while ($stmt->fetch()) {
            $schemes[$schemeId] = $schemeName;
        }
    }

    $query = "SELECT MAX(CatalogYearName) FROM LookupCatalogYear";

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();

    $stmt->bind_result($latestYear);

    $stmt->fetch();

    $maxYear = $latestYear;

$query = 'SELECT CourseCatalogYearId, CatalogYearName, CourseTitle, CourseId, CourseDescription, GradeSchemeName, MinimumGrade, OutcomeHours, Credits, Hours, Research, NewCourse, ReplacingExisting, OriginalCatYear, CurrentVersionCat, RevisionLevel, CourseVersion, RevisionDate, AuthorizedByPersonId, SupportingDoc, AdditionalInfo, SubjectMatterExpert, pmTable.personName, ApprovedDate, cTable.personName, ApprovedDateCC, InstructionalMethodId, AssessmentNote, OpenStudies, Plar, retiredDate, ESReading, ESDocumentUse, ESNumeracy, ESWriting, ESThinking, ESOral, ESWorking, ESCompUse, ESContinuousLearning, ClassHours, LabHours, ClinicalHours, OJTPracticumHours, InternshipHours, ModalityFace, ModalityDistance, ModalityHybrid FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId INNER JOIN LookupGradeScheme ON CourseCatalogYear.GradeSchemeId = LookupGradeScheme.GradeSchemeId INNER JOIN (SELECT personId, concat(PersonFirstName, " ", personLastName) AS personName FROM Person) AS pmTable ON pmTable.PersonId = CourseCatalogYear.ApprovedByPMPersonId INNER JOIN (SELECT personId, concat(PersonFirstName, " ", personLastName) AS personName FROM Person) AS cTable ON cTable.PersonId = CourseCatalogYear.ApprovedByConsultantPersonId WHERE CourseCatalogYearId = ?';

$courseCatalogYearId = null;

if (isset($_GET['course'])) {
    $courseCatalogYearId = $_GET['course'];
}
else if (isset($_POST['course'])) {
    $courseCatalogYearId = $_POST['course'];
}

$stmt = $db->prepare($query);


$stmt->bind_param("s", $courseCatalogYearId);



$stmt->execute();
$stmt->store_result();

$stmt->bind_result($courseCatalogYearId, $catalogYear, $courseTitle, $courseId, $courseDescription, $gradeScheme, $minimumGrade, $outcomeHours, $credits, $hours, $research, $newCourse, $replacingExisting, $originalCatYear, $currentVersionCat, $revisionLevel, $courseVersion, $revisionDate, $authorizedBy, $supportingDoc, $additionalInfo, $subjectMatterExpert, $approvedPM, $approvedDate, $approvedConsultant, $approvedCC, $instructionalMethod, $assessmentNote, $openStudies, $plar, $retiredDate, $reading, $documentUse, $numeracy, $writing, $thinking, $oral, $working, $compUse, $contLearning, $classHours, $labHours, $clinicalHours, $ojtHours, $internshipHours, $face, $distance, $hybrid);




if ($stmt->num_rows == 1) {
    $stmt->fetch();

    echo '<form action="EditCourseForm.php" method="post" class="form-group">
<input type="hidden" name="courseCatalogYearId" value="'.$courseCatalogYearId.'">
<input type="hidden" name="courseId" value="'.$courseId.'">';
    if (isset($yearSearch)) {
        echo '<input type="hidden" name="courseYear" value="'.$yearSearch.'">';
    }
echo '<table class="table table-bordered">
        <tr class="thead-dark">
            <th colspan="4" class="thead-light"><h2 class="form-text">Edit Course</h2></th>
        </tr>
        <tr>
            <td colspan="2"><label for="courseCode"><span class="alert alert-danger form-text">Course Code</span></label> <p>'.$courseId.'</p></td>
            <td colspan="2"><label for="courseTitle"><span class="alert alert-danger form-text">Course Title</span></label> <input type="text" id="courseTitle" name="courseTitle" maxlength="255" class="form-control" value="'.$courseTitle.'"></td>
        </tr>
        <tr>
            <td colspan="4"><label for="courseDescription" class="form-text">Course Description</label> <input type="text" id="courseDescription" name="courseDescription" maxlength="255" class="form-control" value="'.$courseDescription.'"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="gradeScheme" class="required"><span class="alert alert-danger">Grade Scheme</span></label>
                <select id="gradeScheme" name="gradeScheme" class="form-control-sm">';
    foreach($schemes AS $key => $value) {
        if ($gradeScheme == $key) {
            echo '<option value="'.$key.'" selected="selected">'.$value.'</option>';
        }
        else {
            echo '<option value="'.$key.'">'.$value.'</option>';
        }

    }


    echo    '</select>
            </td>
            <td colspan="2"><label for="minimumGrade">Minimum Grade</label> <input type="number" id="minimumGrade" name="minimumGrade" min="0" max="100" class="form-control" value="'.$minimumGrade.'"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="instructionalMethod">Instructional Methods</label></td>
            <td colspan="2">
                <select id="instructionalMethod" name="instructionalMethod" class="form-control-sm">';
    foreach($instrMethods AS $key => $value) {
        if ($instructionalMethod == $key) {
            echo '<option value="'.$key.'" selected="selected">'.$value.'</option>';
        }
        else {
            echo '<option value="'.$key.'">'.$value.'</option>';
        }
    }
    echo   '</select>
            </td>
        </tr>
        <tr class="thead-light">
            <td>Course Value</td>
            <td><label for="outcomeHours">Outcome Hours</label></td>
            <td><label for="credits">Credits</label></td>
            <td><label for="hours">Hours</label></td>
        </tr>
        <tr>
            <td/>
            <td><input type="number" id="outcomeHours" name="outcomeHours" class="form-control" value="'.$outcomeHours.'"></td>
            <td><input type="text" id="credits" name="credits" class="form-control" value="'.$credits.'"></td>
            <td><input type="number" id="hours" name="hours" class="form-control" min="0" value="'.$hours.'"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="assessmentNote">Assessment Note</label></td>
            <td colspan="2"><input type="text" id="assessmentNote" name="assessmentNote" class="form-control" value="'.$assessmentNote.'"></td>
        </tr>
        <tr>
            <td>
                <label for="researchComponent">Is there a research component?</label>';
    if ($research) {
        echo '<input type="checkbox" checked="checked" id="researchComponent" name="researchComponent" class="form-check-inline">';
    }
    else {
        echo '<input type="checkbox" id="researchComponent" name="researchComponent" class="form-check-inline">';
    }

    echo   '</td>
            <td>
                <label for="newCourse">Is this a new course?</label>';

    if ($newCourse) {
        echo '<input type="checkbox" checked="checked" id="newCourse" name="newCourse" class="form-check-inline">';
    }
    else {
        echo '<input type="checkbox" id="newCourse" name="newCourse" class="form-check-inline">';
    }

    echo   '</td>
            <td>
                <label for="openStudies">Open Studies</label>';

    if ($openStudies) {
        echo '<input type="checkbox" checked="checked" id="openStudies" name="openStudies" class="form-check-inline">';
    }
    else {
        echo '<input type="checkbox" id="openStudies" name="openStudies" class="form-check-inline">';
    }

    echo   '</td>
            <td>
                <label for="plar">Plar</label>';

    if ($plar) {
        echo '<input type="checkbox" checked="checked" id="plar" name="plar" class="form-check-inline">';
    }
    else {
        echo '<input type="checkbox" id="plar" name="plar" class="form-check-inline">';
    }

    echo   '</td>
        </tr>
        <tr>
            <td colspan="2">
                <label for="replacingCourse">Is this replacing an existing course?</label>';
    if ($replacingExisting) {
        echo '<input type="checkbox" checked="checked" id="replacingCourse" name="replacingCourse" class="form-check-inline">';
    }
    else {
        echo '<input type="checkbox" id="replacingCourse" name="replacingCourse" class="form-check-inline">';
    }

    echo   '</td>
            <td colspan="2"><label for="retiredDate">Retired Date:</label> <input type="date" id="retiredDate" name="retiredDate" class="form-control" value="'.$retiredDate.'"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="originalCatYear">Catalog Year of Original Course Implementation</label></td>
            <td colspan="2"><input type="number" min="2014" max="'.$maxYear.'" id="originalCatYear" name="originalCatYear" class="form-control" value="'.$originalCatYear.'"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="currentVersionCat">Catalog Year of Current Version Implementation</label></td>
            <td colspan="2"><input type="number" min="2014" max="'.$maxYear.'" id="currentVersionCat" name="currentVersionCat" class="form-control" value="'.$currentVersionCat.'"></td>
        </tr>
        <tr>
            <td>
                <label for="revisionLevel">Revision Level</label>
                <input type="text" id="revisionLevel" name="revisionLevel" class="form-control" value="'.$revisionLevel.'">
            </td>
            <td>
                <label for="version">Version</label>
                <input type="text" id="version" name="version" class="form-control" value="'.$courseVersion.'">
            </td>
            <td>
                <label for="revisionDate">Date</label>
                <input type="date" id="revisionDate" name="revisionDate" class="form-control" value="'.$revisionDate.'">
            </td>
            <td>
                <label for="authorizedBy">Authorized By</label><br>
                <select id="authorizedBy" name="authorizedBy" class="form-control-sm">';
    foreach($people AS $key => $value) {
        if ($authorizedBy == $key) {
            echo '<option value="' . $key . '" selected="selected">' . $value . '</option>';
        } else {
            echo '<option value="' . $key . '">' . $value . '</option>';
        }
    }
    echo   '</select>
            </td>
        </tr>
        <tr>
            <td colspan="4"><label for="supportingDocuments">Supporting Documents</label> <input type="text" name="supportingDocuments" id="supportingDocuments" maxlength="255" class="form-control" value="'.$supportingDoc.'"></td>
        </tr>
        <tr>
            <td colspan="4"><label for="additionalInformation">Additional Information</label> <input type="text" name="additionalInformation" id="additionalInformation" maxlength="255" class="form-control" value="'.$additionalInfo.'"></td>
        </tr>
        <tr>
            <td colspan="2"><label for="subjectMatterExperts">Subject Matter Experts</label></td>
            <td colspan="2"><input type="text" id="subjectMatterExperts" name="subjectMatterExperts" maxlength="255" class="form-control" value="'.$subjectMatterExpert.'"></td>
        </tr>
        <tr>
            <td><label for="programManager">Approved By (Program Manager)</label></td>
            <td>
                <select id="programManager" name="programManager" class="form-control-sm">';
    foreach($programManagers AS $key => $value) {
        if ($approvedPM == $key) {
            echo '<option value="'.$key.'" selected="selected">'.$value.'</option>';
        }
        else {
            echo '<option value="'.$key.'">'.$value.'</option>';
        }
    }
    echo   '</select>
            </td>
            <td><label for="approvedDate">Approved Date</label></td>
            <td><input type="date" id="approvedDate" name="approvedDate" class="form-control" value="'.$approvedDate.'"></td>
        </tr>
        <tr>
            <td><label for="consultant">Approved By (Consultant)</label></td>
            <td>
                <select id="consultant" name="consultant" class="form-control-sm">';
    foreach($programManagers AS $key => $value) {
        if ($approvedConsultant == $key) {
            echo '<option value="'.$key.'" selected="selected">'.$value.'</option>';
        }
        else {
            echo '<option value="'.$key.'">'.$value.'</option>';
        }
    }
    echo   '</select>
            </td>
            <td><label for="approvedDateCC">Approved Date</label></td>
            <td><input type="date" id="approvedDateCC" name="approvedDateCC" class="form-control" value="'.$approvedCC.'"></td>
        </tr>
        <tr>
            <td colspan="2">Essential Skills</td>
            <td colspan="2">Course Hours By Type</td>
        </tr>
        <tr>
            <td><label for="esReading">Reading</label></td>';
            if ($reading) {
                echo '<td><input type="checkbox" checked="checked" id="esReading" name="esReading" class="form-check-inline"></td>';
            }
            else {
                echo '<td><input type="checkbox" id="esReading" name="esReading" class="form-check-inline"></td>';
            }

    echo   '<td><label for="hoursClass">Class Hours</label></td>
            <td><input type="number" id="hoursClass" name="hoursClass" min="0" class="form-control" value="'.$classHours.'"></td>
        </tr>
        <tr>
            <td><label for="esDocument">Document Use</label></td>';
    if ($documentUse) {
        echo '<td><input type="checkbox" checked="checked" id="esDocument" name="esDocument" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esDocument" name="esDocument" class="form-check-inline"></td>';
    }

    echo   '<td><label for="hoursLab">Lab Hours</label></td>
            <td><input type="number" id="hoursLab" name="hoursLab" min="0" class="form-control" value="'.$labHours.'"></td>
        </tr>
        <tr>
            <td><label for="esNumeracy">Numeracy</label></td>';
    if ($numeracy) {
        echo '<td><input type="checkbox" checked="checked" id="esNumeracy" name="esNumeracy" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esNumeracy" name="esNumeracy" class="form-check-inline"></td>';
    }

    echo   '<td><label for="hoursClinical">Clinical Hours</label></td>
            <td><input type="number" id="hoursClinical" name="hoursClinical" min="0" class="form-control" value="'.$clinicalHours.'"></td>
        </tr>
        <tr>
            <td><label for="esWriting">Writing</label></td>';
    if ($writing) {
        echo '<td><input type="checkbox" checked="checked" id="esWriting" name="esWriting" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esWriting" name="esWriting" class="form-check-inline"></td>';
    }

    echo   '<td><label for="hoursOJT">OJT Practicum Hours</label></td>
            <td><input type="number" id="hoursOJT" name="hoursOJT" min="0" class="form-control" value="'.$ojtHours.'"></td>
        </tr>
        <tr>
            <td><label for="esThinking">Thinking</label></td>';
    if ($thinking) {
        echo '<td><input type="checkbox" checked="checked" id="esThinking" name="esThinking" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esThinking" name="esThinking" class="form-check-inline"></td>';
    }

    echo   '<td><label for="hoursIntern">Internship Hours</label></td>
            <td><input type="number" id="hoursIntern" name="hoursIntern" min="0" class="form-control" value="'.$internshipHours.'"></td>
        </tr>
        <tr>
            <td><label for="esOral">Oral</label></td>';
    if ($oral) {
        echo '<td><input type="checkbox" checked="checked" id="esOral" name="esOral" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esOral" name="esOral" class="form-check-inline"></td>';
    }

    echo   '<td colspan="2">Delivery Modalities</td>
        </tr>
        <tr>
            <td><label for="esWorking">Working</label></td>';
    if ($working) {
        echo '<td><input type="checkbox" checked="checked" id="esWorking" name="esWorking" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esWorking" name="esWorking" class="form-check-inline"></td>';
    }

    echo   '<td><label for="modalityFace">Face</label></td>';
    if ($face) {
        echo '<td><input type="checkbox" checked="checked" id="modalityFace" name="modalityFace" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="modalityFace" name="modalityFace" class="form-check-inline"></td>';
    }
echo   '</tr>
        <tr>
            <td><label for="esCompUse">Comp Use</label></td>';
    if ($compUse) {
        echo '<td><input type="checkbox" checked="checked" id="esCompUse" name="esCompUse" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esCompUse" name="esCompUse" class="form-check-inline"></td>';
    }

    echo   '<td><label for="modalityDistance">Distance</label></td>';
    if ($distance) {
        echo '<td><input type="checkbox" checked="checked" id="modalityDistance" name="modalityDistance" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="modalityDistance" name="modalityDistance" class="form-check-inline"></td>';
    }

echo   '</tr>
        <tr>
            <td><label for="esContLearning">Continuous Learning</label></td>';
    if ($contLearning) {
        echo '<td><input type="checkbox" checked="checked" id="esContLearning" name="esContLearning" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="esContLearning" name="esContLearning" class="form-check-inline"></td>';
    }

    echo   '<td><label for="modalityHybrid">Hybrid</label></td>';
    if ($hybrid) {
        echo '<td><input type="checkbox" checked="checked" id="modalityHybrid" name="modalityHybrid" class="form-check-inline"></td>';
    }
    else {
        echo '<td><input type="checkbox" id="modalityHybrid" name="modalityHybrid" class="form-check-inline"></td>';
    }

echo   '</tr>
        <tr>
            <td colspan="4"><input type="submit" value="Submit" class="form-control"></td>
        </tr>';
    if (isset($message)) {
        echo '<tr>
                    <td colspan="4" class="alert-warning">'.$message.'</td>
                  </tr>';
    }

    echo '</table>

</form> ';
}




require("../../Bootstrap/incFootPage.php");


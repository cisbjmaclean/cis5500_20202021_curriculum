<?php

/**
 * CourseCreditValue.php
 *
 * Returns a list of courses and programs sorted by Credit value, then CourseId, while allowing a user
 * to select a specific credit value.
 *
 * @author twhiten
 * @since 20201/03/04
 */

    include('../../Bootstrap/incPageHead.php');


    $query = "SELECT CatalogYearName, CourseId, CourseTitle, Credits FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN (SELECT ParentCourseId, ChildCourseId FROM LookupRequisiteType INNER JOIN CourseRequisite ON CourseRequisite.RequisiteTypeId = LookupRequisiteType.RequisiteId WHERE RequisiteName LIKE 'Co-requisite') AS corequisite ON CourseCatalogYear.CourseId = corequisite.ParentCourseId INNER JOIN (SELECT ParentCourseId, ChildCourseId FROM LookupRequisiteType INNER JOIN CourseRequisite ON CourseRequisite.RequisiteTypeId = LookupRequisiteType.RequisiteId WHERE RequisiteName LIKE 'Prerequisite') AS prerequisite ON CourseCatalogYear.CourseId = prerequisite.ParentCourseId";

    if (isset($_POST['search'])) {
        $query .= " WHERE Credits = ? AND prerequisite.ChildCourseId LIKE 'NONE' AND corequisite.ChildCourseId LIKE 'NONE'";
    };

    $query .= " ORDER BY Credits, CourseId";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = $_POST['search'];
        $stmt->bind_param("s", $searchTerm);
    }


    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($catalogYearName, $courseId, $courseTitle, $credits);


    ?>

    <div class="toast-header justify-content-center">
        <form action="CourseCreditValueNoReqs.php" method="post">
            <label for="search">Enter Credit Value</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table class="table table-bordered">';

    if ($stmt->num_rows > 0) {
        $currentCredit = -1;
        $titleRow = false;
        $currentCourse = null;



        while ($stmt->fetch()) {
            if (!$titleRow) {
                echo '<tr class="thead-dark">
                        <th/>
                        <th>Course Code</th>
                        <th colspan="2">Course Title</th>
                      </tr>';
                $titleRow = true;
            }
            if ($currentCredit != $credits) {
                echo '<tr class="thead-light">
                        <th>Credit Value: ';
                        if (!isset($credits)) {
                           echo '0</th>';
                       }
                       else {
                           echo $credits.'</th>';
                       }
                 echo '<th colspan="3"/>
                      </tr>';

                $currentCredit = $credits;
            }
            if ($currentCourse != $courseId) {
                echo '<tr class="thead-light"><th colspan="4"/></tr>';
                echo '<tr>
                    <td/>
                    <td>'.$courseId.'</td>
                    <td colspan="2">'.$courseTitle.'</td>
                  </tr>';
                $currentCourse = $courseId;
            }



        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
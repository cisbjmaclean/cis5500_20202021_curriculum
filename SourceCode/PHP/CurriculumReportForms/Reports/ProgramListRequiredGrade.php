<?php

/**
 * ProgramList.php
 *
 * Return a list of programs.
 *
 * @author twhiten
 * @since 20201/03/05
 */

    include('../../Bootstrap/incPageHead.php');


    $query = 'SELECT DISTINCT Program.ProgramId, ProgramTitle, ProgramMinimum FROM Program INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramId = Program.ProgramId  INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($programId, $programTitle, $programMinimum);

    echo '<table class="table table-bordered">
              <tr>
                <th colspan="4"><h1>Program List</h1><p>Programs and passing grades</p></th>
              </tr>
              <tr class="thead-light">
                <th>Program Id</th>
                <th colspan="2">Program Title</th>
                <th>Program Minimum</th>
              </tr>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {
            echo '<tr>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                    <td>'.$programMinimum.'</td>
                  </tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
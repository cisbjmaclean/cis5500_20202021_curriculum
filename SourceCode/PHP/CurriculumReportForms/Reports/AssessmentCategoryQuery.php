<?php

/**
 * AssessmentCategoryQuery.php
 *
 * Returns a list of courses and programs with details on assessments involved with each program.
 * Allows users to search for specific assessments.
 *
 * @author twhiten
 * @since 20201/03/03
 */

    require('../../Bootstrap/incPageHead.php');



    $query = 'SELECT AssessmentCategoryId, CatalogYearName, ProgramCatalogYear.ProgramId, ProgramTitle, CourseId, CourseTitle, AssessmentCategoryName FROM LookupCatalogYear INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearID AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN Program ON Program.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId INNER JOIN ProgramCourse ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN CourseCatalogYear ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN AssessmentCategory ON AssessmentCategory.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupAssessmentCategoryName ON LookupAssessmentCategoryName.AssessmentCategoryNameId = AssessmentCategory.AssessmentCategoryNameId';

    if (isset($_POST['search'])) {
        $query .= " WHERE AssessmentCategoryName LIKE ? ";
    };

    $query .= " ORDER BY AssessmentCategoryName";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    };

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($assessmentCategoryId, $catalogYearName, $programId, $programTitle, $courseId,
        $courseTitle, $assessmentCategoryName);


    ?>

    <div class="toast-header justify-content-center">
        <form action="AssessmentCategoryQuery.php" method="post">
            <label for="search">Enter Assessment Name</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table class="table table-bordered">
              <tr class="thead-dark">
                <th>Year</th>
                <th>Assessment Category</th>
                <th>Program ID</th>
                <th>Program Title</th>
                <th>Course Catalog Year</th>
                <th>Course Title</th>
              </tr>';

    if ($stmt->num_rows > 0) {
        $currentYear= 0;
        $currentCategory = null;


        while ($stmt->fetch()) {
            if ($currentYear != $catalogYearName) {
                echo '<tr class="thead-light"><th>'.$catalogYearName.'</th><th colspan="5"></th></tr>';
                $currentYear = $catalogYearName;
            }
            if ($currentCategory != $assessmentCategoryName) {
                echo '<tr class="thead-light"><th></th><th>'.$assessmentCategoryName.'</th><th colspan="4"></th></tr>';
                $currentCategory = $assessmentCategoryName;
            }
            echo '<tr><td colspan="2"></td><td>'.$programId.'</td><td>'.$programTitle.'</td><td>'.$catalogYearName.'</td>
                      <td>'.$courseTitle.'</td></tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="6">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
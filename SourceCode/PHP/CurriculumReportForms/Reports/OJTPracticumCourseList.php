<?php

/**
 * OJTPracticumCourseList.php
 *
 * Returns a detailed list of OJT courses.
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('../../Bootstrap/incPageHead.php');


    $query = 'SELECT CourseId, CourseTitle, Hours, Credits, GradeSchemeName FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LookupGradeScheme ON CourseCatalogYear.GradeSchemeId = LookupGradeScheme.GradeSchemeId WHERE CourseTitle LIKE "%OJT%" OR CourseTitle LIKE "%Practicum%" OR CourseTitle LIKE "%Job%" OR CourseTitle LIKE "%Clinical%" OR CourseTitle LIKE "%Capstone%"';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $hours, $credits, $gradeScheme);



    echo '<table class="table table-bordered">';

    if ($stmt->num_rows > 0) {

        echo '<tr class="thead-dark">
                <th colspan="6"><h1>OJT Practicum Courses</h1></th>
              </tr>';

        echo '<tr class="thead-light">
                <th>Course Code</th>
                <th colspan="2">Course Title</th>
                <th>Grade Scheme</th>
                <th>Hours</th>
                <th>Credits</th>
              </tr>';

        while ($stmt->fetch()) {

                echo '<tr>
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td>'.$gradeScheme.'</td>
                        <td>'.$hours.'</td>
                        <td>'.$credits.'</td>
                      </tr>';


        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
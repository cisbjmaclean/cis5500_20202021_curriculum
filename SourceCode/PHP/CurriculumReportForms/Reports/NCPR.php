<?php

/**
 * NCPR.php
 *
 * Returns a list of NCPR courses and the programs under those courses.
 *
 * @author twhiten
 * @since 20201/03/08
 */

    include('../../Bootstrap/incPageHead.php');


    $query = 'SELECT CourseId, CourseTitle, ProgramId, ProgramTitle, MinimumGrade, Hours, Credits  FROM CourseCatalogYear INNER JOIN LearningOutcome ON LearningOutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId WHERE CourseId LIKE "NCPR%" AND CourseCatalogYear.CatalogYearName = (SELECT Max(CatalogYearName) FROM LookupCatalogYear) ORDER BY CourseId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($CourseId, $CourseTitle, $ProgramId, $ProgramTitle, $Grade, $Hours, $Credits);



    echo '<table class="table table-bordered">
            <tr class="thead-dark">
                <th colspan="8">NCPR Courses</th>
            </tr>';

    if ($stmt->num_rows > 0) {

        $currentCourse = null;

        while ($stmt->fetch()) {

            if ($currentCourse != $CourseId) {
                echo '<tr class="thead-dark">
                        <th>Course Code</th>
                        <th colspan="4">Course Title</th>
                        <th>Grade</th>
                        <th>Hours</th>
                        <th>Credits</th>
                      </tr>
                      <tr class="thead-light">
                        <th>'.$CourseId.'</th>
                        <th colspan="4">'.$CourseTitle.'</th>';

                if (empty($Grade)) {
                    echo '<th>P</th>';
                }
                else {
                    echo '<th>'.$Grade.'</th>';
                }

                  echo '<th>'.$Hours.'</th>
                        <th>'.$Credits.'</th>
                      </tr>
                      <tr class="thead-light">
                        <th colspan="7">Programs adopting this NCPR</th>
                        <th/>
                      </tr>';

                $currentCourse = $CourseId;
            }

            echo '<tr>
                    <td colspan="2"/>
                    <td>'.$ProgramId.'</td>
                    <td colspan="2">'.$ProgramTitle.'</td>
                    <td colspan="3"/>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
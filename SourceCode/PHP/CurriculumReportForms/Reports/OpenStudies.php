<?php

/**
 * OpenStudies.php
 *
 * Return a list of "open study" courses
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('../../Bootstrap/incPageHead.php');


    $query = 'SELECT CourseId, CourseTitle FROM CourseCatalogYear INNER JOIN LookupCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) WHERE OpenStudies';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle);


    echo '<table class="table table-bordered">
              <tr class="thead-dark">
                <th colspan="2"><h1>Open Courses</h1></th>
              </tr>
              <tr class="thead-light">
                <th>Course Id</th>
                <th>Course Title</th>
              </tr>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {
            echo '<tr><td>'.$courseId.'</td><td>'.$courseTitle.'</td></tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
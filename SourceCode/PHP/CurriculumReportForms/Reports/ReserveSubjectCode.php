<?php

/**
 * ReserveSubjectCode.php
 *
 * Returns reserved subject codes
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('../../Bootstrap/incPageHead.php');

    $admin = false;
    $colspan = 4;

    if (isset($_SESSION['userType']) && $_SESSION['userType'] == 2) {
        $admin = true;
        $colspan = 5;
    }

if ($admin && isset($_POST['code'])) {
    if (strlen($_POST['code']) == 4 && strlen($_POST['number']) == 4 && is_numeric($_POST['number'])) {
        $code = $_POST['code'];
        $num = $_POST['number'];
        $desc = $_POST['desc'];
        $title = $_POST['title'];
        $res = $_POST['res'];
        $common = $_POST['common'];
        $accepted = $_POST['accepted'];

        $query = "INSERT INTO ReserveSubjectCode (SubjectCode, Number, Title, Description, ReservedBy, Common, Accepted) VALUES (?, ?, ?, ?, ?, ?, ?)";

        $stmt = $db->prepare($query);

        $stmt->bind_param("sssssss", $code, $num, $title, $desc, $res, $common, $accepted);

        $stmt->execute();

        $stmt->close();
    }
}


    $query = 'SELECT SubjectCode, Number, Title, ReservedBy, Common FROM reservesubjectcode';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($code, $number, $title, $reserved, $common);



    echo '<table class="table table-bordered">';

    if ($stmt->num_rows >= 0) {

        echo '<tr class="thead-dark">
                <th colspan="'.$colspan.'"><h1>Reserved Subject Codes</h1></th>
              </tr>
              <tr class="thead-light">
                <th>Subject Code</th>
                <th>Title</th>
                <th>Reserved</th>
                <th>Common</th>';

        if ($admin) {
            echo '<th>Actions</th>';
        }

        echo '</tr>';

        if ($admin) {
            echo '<form action="ReserveSubjectCode.php" method="post">
                      <tr>
                        <td><input type="text" class="form-control" name="code" placeholder="Subject Code"></td>
                        <td><input type="text" class="form-control" name="number" placeholder="Subject Number"></td>
                        <td colspan="2"><input type="text" class="form-control" name="desc" placeholder="Description"></td>
                        <td><input type="text" class="form-control" name="title" placeholder="Title"></td>
                      </tr>
                      <tr>
                        <td><input type="text" name="res" class=form-control placeholder="Reserved By"></td>
                        <td><input type="text" name="common" class=form-control placeholder="Common"></td>
                        <td><input type="text" name="accepted" class=form-control placeholder="Accepted"></td>
                        <td/>
                        <td><input type="submit" class="btn btn-primary" value="Add New"></td>
                      </tr>
                  </form>';
        }


        while ($stmt->fetch()) {

            echo '<tr>
                    <td>'.$code.'-'.$number.'</td>
                    <td>'.$title.'</td>
                    <td>'.$reserved.'</td>
                    <td>'.$common.'</td>
                  </tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
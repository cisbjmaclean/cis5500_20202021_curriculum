<?php

/**
 * CourseHourBreakdown.php
 *
 * Returns a list of courses and programs, with subtotals for credit courses, non-credit courses, and total values.
 *
 * @author twhiten
 * @since 20201/03/05
 */

include('../../Bootstrap/incPageHead.php');

$programSearch = null;
$yearSearch = null;

$query = 'SELECT CatalogYearName, ProgramCatalogYear.ProgramId, LookupProgramTitle.ProgramTitle, CourseCatalogYear.CourseId, CourseCatalogYear.CourseTitle, CourseCatalogYear.Hours, CourseCatalogYear.Credits, CourseCatalogYear.ClassHours, CourseCatalogYear.LabHours, CourseCatalogYear.ClinicalHours, CourseCatalogYear.OJTPracticumHours, CourseCatalogYear.InternshipHours, CourseCatalogYear.InstructionalMethodId, If(CourseCatalogYear.Credits>0,"Credit Course","Non-Credit Course") AS CreditCourse, creditCount.rowCount, nonCreditCount.rowCount FROM LookupProgramTitle INNER JOIN ProgramCatalogYear ON LookupProgramTitle.ProgramTitleId = ProgramCatalogYear.ProgramTitleId INNER JOIN ProgramCourse ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId INNER JOIN CourseCatalogYear ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupCatalogYear ON ProgramCatalogYear.CatalogYearID = LookupCatalogYear.CatalogYearID INNER JOIN';


//Get number of courses from the year
if (isset($_POST['year']) && strlen($_POST['year']) > 0) {
    $query .= '(SELECT ProgramId, count(CourseId) AS rowCount FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId WHERE Credits > 0 AND CatalogYearName = ? GROUP BY ProgramId) AS creditCount ON creditCount.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN (SELECT ProgramId, count(CourseId) AS rowCount FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId WHERE Credits <= 0 AND CatalogYearName = ? GROUP BY ProgramId) AS nonCreditCount';
}
else {
    $query .= '(SELECT ProgramId, count(CourseId) AS rowCount FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId WHERE Credits > 0 AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) GROUP BY ProgramId) AS creditCount ON creditCount.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN (SELECT ProgramId, count(CourseId) AS rowCount FROM CourseCatalogYear INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId WHERE Credits <= 0 AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) GROUP BY ProgramId) AS nonCreditCount';
}

$query .= 'ON nonCreditCount.ProgramId = ProgramCatalogYear.ProgramId';

if (isset($_POST['program']) && strlen($_POST['program']) > 0) {
    $programSearch = "%".$_POST['program']."%";
    $query .= " WHERE ProgramCatalogYear.ProgramId LIKE ?";
}

if (isset($_POST['year'])) {
    $yearSearch = $_POST['year'];

    if (isset($_POST['program']) && strlen($_POST['program']) > 0) {
        $query .= " AND ";
    }
    else {
        $query .= " WHERE ";
    }

    $query .= "CatalogYearName = ?";
}
else {

    if (isset($_POST['program']) && strlen($_POST['program']) > 0) {
        $query .= " AND ";
    }
    else {
        $query .= " WHERE ";
    }

    $query.= "CatalogYearName = (Select MAX(CatalogYearName) FROM LookupCatalogYear)";
}

$query .= " ORDER BY ProgramId, CreditCourse, CourseId, CatalogYearName DESC";

$stmt = $db->prepare($query);


if (isset($programSearch) && !isset($yearSearch)) {
    $searchTerm = "%".$programSearch."%";
    $stmt->bind_param("s", $searchTerm);
}
if (!isset($programSearch) && isset($yearSearch)) {
    $searchTerm = $yearSearch;
    $stmt->bind_param("sss", $searchTerm, $searchTerm, $searchTerm);
}
if (isset($programSearch) && isset($yearSearch)) {
    $search1 = "%".$programSearch."%";
    $search2 = $yearSearch;
    $stmt->bind_param("ssss", $search2, $search2, $search1, $search2);
}

$stmt->execute();
$stmt->store_result();


$stmt->bind_result($CatalogYearName, $ProgramId, $ProgramTitle, $CourseId, $CourseTitle, $Hours, $Credits, $ClassHours, $LabHours, $ClinicalHours, $OJTHours, $InternHours, $InstructionalId, $isCredit, $creditRows, $nonCreditRows);


?>



    <div class="toast-header justify-content-center">
        <form action="CourseHourBreakdown.php" method="post">
            <label for="program">Enter Program Name: </label>
            <input type="text" id="program" name="program"><br>
            <label for="year">Enter Year</label>
            <select type="text" id="year" name="year">
                <?php

                foreach(range(2014, (int)date("Y")) as $yearSearch) {
                    echo "\t<option value='".$yearSearch."'>".$yearSearch."</option>\n\r";
                }

                ?>
            </select>
            <br><input type="submit" value="Search">
        </form>
    </div>

<?php

/*
 * New Program: ProgramID, ProgramName, CatalogYear
 * When a list of credit or non-credit course starts: Credit/Non-Credit, number of courses, headers
 * List of courses
 * List of credit/non-credit courses ends OR new program starts: Credit/Non-Credit, subtotals
 * List of program ends: Total
 */

if ($stmt->num_rows > 0) {
    $currentProgram = null;
    $currentCredit = null;

    $hourSubtotal = 0;
    $hourTotal = 0;
    $subtotalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
    $totalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
    $creditSubtotal = 0;
    $creditTotal = 0;

    echo '<table class="table table-bordered">';

    while ($stmt->fetch()) {
        if ($currentProgram != $ProgramId) {
            if (isset($currentProgram)) {

                echo '<tr class="thead-light">
                        <th/>
                        <th>'.$currentCredit.'</th>
                        <th>Subtotal:</th>
                        <th>'.$creditSubtotal.'</th>
                        <th>'.$hourSubtotal.'</th>
                        <th>'.$subtotalArray['Class'].'</th>
                        <th>'.$subtotalArray['Lab'].'</th>
                        <th>'.$subtotalArray['Clinical'].'</th>
                        <th>'.$subtotalArray['OJT'].'</th>
                        <th>'.$subtotalArray['Internship'].'</th>
                      </tr>';

                $hourTotal += $hourSubtotal;
                $creditTotal += $creditSubtotal;
                $totalArray['Class'] += $subtotalArray['Class'];
                $totalArray['Lab'] += $subtotalArray['Lab'];
                $totalArray['Clinical'] += $subtotalArray['Clinical'];
                $totalArray['OJT'] += $subtotalArray['OJT'];
                $totalArray['Internship'] += $subtotalArray['Internship'];

                $hourSubtotal = 0;
                $creditSubtotal = 0;

                $subtotalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);

                echo '<tr class="thead-light">
                          <th/>
                          <th>'.$currentProgram.'</th>
                          <th>Total Hours:</th>
                          <th>'.$creditTotal.'</th>
                          <th>'.$hourTotal.'</th>
                          <th>'.$totalArray['Class'].'</th>
                          <th>'.$totalArray['Lab'].'</th>
                          <th>'.$totalArray['Clinical'].'</th>
                          <th>'.$totalArray['OJT'].'</th>
                          <th>'.$totalArray['Internship'].'</th>
                      </tr>';

                $hourTotal = 0;
                $creditTotal = 0;

                $totalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
            }
            echo '<tr class="thead-dark">
                        <th>'.$ProgramId.'</th>
                        <th colspan="7">'.$ProgramTitle.'</th>
                        <th>Catalog Year</th>
                        <th>'.$CatalogYearName.'</th>
                      </tr>
                      <tr class="thead-light">
                        <th>'.$isCredit.'</th>
                        <th>Count:</th>';

            if ($isCredit == "Credit Course") {
                echo '<th>'.$creditRows.'</th>';
            }
            else {
                echo '<th>'.$nonCreditRows.'</th>';
            }

            echo  '<th>Credits</th>
                        <th>Total Hours</th>
                        <th>Class</th>
                        <th>Lab</th>
                        <th>Clinical</th>
                        <th>Practicum</th>
                        <th>Internship</th>
                      </tr>';

            $currentProgram = $ProgramId;
            $currentCredit = $isCredit;
        }
        if ($currentCredit != $isCredit) {
            echo '<tr class="thead-light">
                        <th/>
                        <th>'.$currentCredit.'</th>
                        <th>Subtotal:</th>
                        <th>'.$creditSubtotal.'</th>
                        <th>'.$hourSubtotal.'</th>
                        <th>'.$subtotalArray['Class'].'</th>
                        <th>'.$subtotalArray['Lab'].'</th>
                        <th>'.$subtotalArray['Clinical'].'</th>
                        <th>'.$subtotalArray['OJT'].'</th>
                        <th>'.$subtotalArray['Internship'].'</th>
                      </tr>
                      <tr class="thead-light">
                        <th>'.$isCredit.'</th>
                        <th>Count:</th>';
            if ($isCredit == "Credit Course") {
                echo '<th>'.$creditRows.'</th>';
            }
            else {
                echo '<th>'.$nonCreditRows.'</th>';
            }
            echo '<th>Credits</th>
                        <th>Total Hours</th>
                        <th>Class</th>
                        <th>Lab</th>
                        <th>Clinical</th>
                        <th>Practicum</th>
                        <th>Internship</th>
                      </tr>';

            $hourTotal += $hourSubtotal;
            $creditTotal += $creditSubtotal;
            $totalArray['Class'] += $subtotalArray['Class'];
            $totalArray['Lab'] += $subtotalArray['Lab'];
            $totalArray['Clinical'] += $subtotalArray['Clinical'];
            $totalArray['OJT'] += $subtotalArray['OJT'];
            $totalArray['Internship'] += $subtotalArray['Internship'];
            $currentCredit = $isCredit;

            $hourSubtotal = 0;
            $creditSubtotal = 0;

            $subtotalArray = array('Class'=>0, 'Lab'=>0, 'Clinical'=>0, 'OJT'=>0, 'Internship'=>0);
        }
        echo '<tr>
                    <td>'.$CourseId.'</td>
                    <td colspan="2">'.$CourseTitle.'</td>
                    <td>'.$Credits.'</td>
                    <td>'.$Hours.'</td>
                    <td>'.$ClassHours.'</td>
                    <td>'.$LabHours.'</td>
                    <td>'.$ClinicalHours.'</td>
                    <td>'.$OJTHours.'</td>
                    <td>'.$InternHours.'</td>
                  </tr>';


        $subtotalArray['Class'] += $ClassHours;
        $subtotalArray['Lab'] += $LabHours;
        $subtotalArray['Clinical'] += $ClinicalHours;
        $subtotalArray['OJT'] += $OJTHours;
        $subtotalArray['Internship'] += $InternHours;
        $creditSubtotal += $Credits;
        $hourSubtotal += $Hours;
    }

    echo '<tr class="thead-light">
                <th/>
                <th>'.$currentCredit.'</th>
                <th>Subtotal:</th>
                <th>'.$creditSubtotal.'</th>
                <th>'.$hourSubtotal.'</th>
                <th>'.$subtotalArray['Class'].'</th>
                <th>'.$subtotalArray['Lab'].'</th>
                <th>'.$subtotalArray['Clinical'].'</th>
                <th>'.$subtotalArray['OJT'].'</th>
                <th>'.$subtotalArray['Internship'].'</th>
              </tr>';

    $hourTotal += $hourSubtotal;
    $creditTotal += $creditSubtotal;
    $totalArray['Class'] += $subtotalArray['Class'];
    $totalArray['Lab'] += $subtotalArray['Lab'];
    $totalArray['Clinical'] += $subtotalArray['Clinical'];
    $totalArray['OJT'] += $subtotalArray['OJT'];
    $totalArray['Internship'] += $subtotalArray['Internship'];

    echo '<tr class="thead-light">
                  <th/>
                  <th>'.$currentProgram.'</th>
                  <th>Total Hours:</th>
                  <th>'.$creditTotal.'</th>
                  <th>'.$hourTotal.'</th>
                  <th>'.$totalArray['Class'].'</th>
                  <th>'.$totalArray['Lab'].'</th>
                  <th>'.$totalArray['Clinical'].'</th>
                  <th>'.$totalArray['OJT'].'</th>
                  <th>'.$totalArray['Internship'].'</th>
              </tr>';
}
else {
    $error = $db->errno . " " . $db->error;
    echo '<tr><td colspan="6">'.$error.'</td></tr>';
}

echo '</table>';

include('../../Bootstrap/incFootPage.php');
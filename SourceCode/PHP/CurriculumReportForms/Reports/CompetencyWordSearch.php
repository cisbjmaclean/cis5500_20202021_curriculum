<?php

    /**
     * CourseCreditValue.php
     *
     * Returns a list of courses with learning outcomes and competencies involved with that course.
     *
     * @author twhiten
     * @since 20201/03/04
     */

    require('../../Bootstrap/incPageHead.php');


    $query = 'SELECT CourseId, CourseTitle, LearningOutcomeName, LearningCompetencyName FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LearningOutcome ON LearningOutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LearningCompetency ON LearningOutcome.LearningOutcomeId = LearningCompetency.LearningOutcomeId';


    if (isset($_POST['search'])) {
        $query .= "WHERE LearningCompetencyName LIKE ?";
    }

    $query .= " ORDER BY CourseId, LearningOutcomeName";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    };

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $oName, $cName);


    ?>

    <div class="toast-header justify-content-center">
        <form action="CompetencyWordSearch.php" method="post">
            <label for="search">Search Outcomes/Competencies</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table class="table table-bordered">';

    if ($stmt->num_rows > 0) {
        $currentCourse = null;
        $currentOutcome = null;
        $outId = 0;
        $compId = 0;

        echo '<tr class="thead-dark">
                <th colspan="5"><h2>Competency Search</h2></th>
              </tr>';


        while ($stmt->fetch()) {
            if ($currentCourse != $courseId) {
                echo '<tr class="thead-light"><th>'.$courseId.'</th><th>'.$courseTitle.'</th><th colspan="3"></th></tr>';
                $currentCourse = $courseId;
                $outId = 0;
                $compId = 0;
            }
            if ($currentOutcome != $oName) {
                $outId += 1;
                echo '<tr class="thead-light"><th/><th colspan="4">'.$oName.'</th><th/></tr>';
                $currentOutcome = $oName;
            }
            $compId += 1;
            echo '<tr><td colspan="2"/><td colspan="3">'.$cName.'</td></tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="5">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
<?php

/**
 *
 * SubjectCode.php
 *
 * Returns subject codes. Admins can add, delete, and update codes
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('../../Bootstrap/incPageHead.php');

    $colspan = 2;
    $admin = false;

    if (isset($_SESSION['userType']) && $_SESSION['userType'] == 2) {
        $colspan = 3;
        $admin = true;
    }

    if ($admin && isset($_GET['id'])) {
        $deleteQuery = "DELETE FROM SubjectCodes WHERE SubjectCodeId = ?";

        $stmt = $db->prepare($deleteQuery);

        $stmt->bind_param("s", $_GET['id']);

        $stmt->execute();

        $stmt->close();
    }

    if ($admin && isset($_POST['code']) && isset($_POST['desc'])) {
        if (strlen($_POST['code']) == 4 &&  !empty($_POST['desc'])) {
            $code = $_POST['code'];
            $desc = $_POST['desc'];

            $query = "INSERT INTO SubjectCodes (FourLetterCode, SubjectDescription) VALUES (?, ?)";

            $stmt = $db->prepare($query);

            $stmt->bind_param("ss", $code, $desc);

            $stmt->execute();

            $stmt->close();
        }
    }


    $query = 'SELECT * FROM subjectCodes';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($id, $code, $description);


    echo '<table class="table table-bordered">';

    if ($stmt->num_rows >= 0) {

        echo '<tr class="thead-dark">
                <th colspan="'.$colspan.'"><h1>Subject Codes</h1></th>
              </tr>
              <tr class="thead-light">
                <th>Subject Code</th>
                <th>Subject Description</th>';

        if ($admin) {
            echo '<th>Actions</th>';
        }

        echo '</tr>';

        if ($admin) {
            echo '<form action="SubjectCodes.php" method="post">
                      <tr>
                        <td><input type="text" class="form-control" name="code"></td>
                        <td><input type="text" class="form-control" name="desc"></td>
                        <td><input type="submit" value="Add New" class="btn btn-primary"></td>
                      </tr>
                  </form>';
        }

        while ($stmt->fetch()) {

            echo '<tr>
                    <td>'.$code.'</td>
                    <td>'.$description.'</td>';

            if ($admin) {
                echo '<td><a href="../Forms/EditSubjectCode.php?id='.$id.'">Update</a> <a href="SubjectCodes.php?id='.$id.'">Delete</a></td>';
            }

            echo '</tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
<?php

/**
 * Entrepreneurship Extended.php
 *
 * Returns a list of courses and programs with a focus on entrepreneurial or business planning.
 *
 * @author twhiten
 * @since 20201/03/08
 */

    include('../../Bootstrap/incPageHead.php');



    $query = 'SELECT DISTINCT CourseId, CourseTitle, ProgramId, ProgramTitle, Hours 
FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN LearningOutcome ON LearningOutcome.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCourse ON ProgramCourse.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId WHERE CourseTitle LIKE "%entrepreneur%" OR LearningOutcomeName LIKE "%entrepreneur%" OR CourseTitle LIKE "%Business Plan%" OR LearningOutcomeName LIKE "%Business Plan%" ORDER BY CourseId';


    $stmt = $db->prepare($query);


    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($courseId, $courseTitle, $programId, $programTitle, $hours);


    echo '<table class="table table-bordered">
              ';

    if ($stmt->num_rows > 0) {
        $currentCourse = null;

        echo '<tr class="thead-dark">
                <th colspan="5">Entrepreneurship Courses</th>
              </tr>';


        while ($stmt->fetch()) {
            if ($currentCourse != $courseId) {
                echo '<tr class="thead-light">
                        <th>Course:</th>
                        <th>'.$courseId.'</th>
                        <th colspan="2">'.$courseTitle.'</th>
                        <th/>
                      </tr>
                      <tr class="thead-light">
                        <th/>
                        <th colspan="3">Course Adopted In The Following Programs</th>
                        <th>Hours:</th>
                      </tr>';
                $currentCourse = $courseId;
            }
            echo '<tr>
                    <td></td>
                    <td>'.$programId.'</td>
                    <td colspan="2">'.$programTitle.'</td>
                    <td>'.$hours.'</td>
                  </tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="5">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
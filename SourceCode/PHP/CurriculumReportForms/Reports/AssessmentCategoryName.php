<?php

/**
 * AssessmentCategoryName.php
 *
 * Returns a list of assessment category names
 *
 * @author twhiten
 * @since 20201/03/03
 */

    $pageTitle = "Assessment Category Name";

    require('../../Bootstrap/incPageHead.php');

    $admin = false;
    $colspan = 1;
    if (isset($_SESSION['loggedIn']) && $_SESSION['userType'] == 2) {
        $admin = true;
        $colspan = 2;
    }


    $query = 'SELECT AssessmentCategoryNameId, AssessmentCategoryName FROM LookupAssessmentCategoryName';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($id, $name);




    echo '<div id="container">';





    echo '<table class="table table-bordered">';

    if ($stmt->num_rows > 0) {

        $currentCourse = null;

        echo '<tr class="thead-light"><th colspan="'.$colspan.'">Assessment Categories</th></tr>';

        while ($stmt->fetch()) {

            echo '<tr>
                    <td>'.$name.'</td>';
                    
                    if ($admin) {
                        echo '<td><a href="../Forms/EditAssessmentForm.php?id='.$id.'">Edit Assessment Name</a></td>';
                    }

            echo  '</tr>';

        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    echo "</div>";
    include('../../Bootstrap/incFootPage.php');



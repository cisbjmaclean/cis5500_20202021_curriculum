<?php

/**
 * AssessmentCategorySearch.php
 *
 * Returns a list of courses and programs with details on assessments involved with each program.
 * Allows users to search for specific assessments.
 *
 * @author twhiten
 * @since 20201/03/03
 */

 require('../../Bootstrap/incPageHead.php');


$query = 'SELECT ProgramCatalogYear.ProgramId, ProgramTitle, CourseId, CourseTitle, AssessmentCategoryName FROM LookupCatalogYear INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearID AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN Program ON Program.ProgramId = ProgramCatalogYear.ProgramId INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId INNER JOIN ProgramCourse ON ProgramCourse.ProgramCatalogYearId = ProgramCatalogYear.ProgramCatalogYearId INNER JOIN CourseCatalogYear ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN AssessmentCategory ON AssessmentCategory.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupAssessmentCategoryName ON LookupAssessmentCategoryName.AssessmentCategoryNameId = AssessmentCategory.AssessmentCategoryNameId';

    if (isset($_POST['search'])) {
        $query .= " WHERE AssessmentCategoryName LIKE ? ";
    };

    $query .= " ORDER BY AssessmentCategoryName, ProgramCatalogYear.ProgramId";

    $stmt = $db->prepare($query);


    if (isset($_POST['search'])) {
        $searchTerm = "%".$_POST['search']."%";
        $stmt->bind_param("s", $searchTerm);
    };

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($programId, $programTitle, $courseId,
        $courseTitle, $assessmentCategoryName);

    $resultArray = null;

    ?>

    <div class="toast-header justify-content-center">
        <form action="AssessmentCategorySearch.php" method="post">
            <label for="search">Enter Assessment Name</label></br>
            <input type="text" id="search" name="search"><input type="submit" value="Search">
        </form>
    </div>

    <?php

    echo '<table class="table table-bordered">
              <tr class="thead-dark">
                <th>Assessment Category</th><th colspan="3"/>
              </tr>
              <tr class="thead-dark">
                <th/><th>Program Code</th><th>Program Title</th><th/>
              </tr>
              <tr class="thead-dark">
                <th colspan="2"/><th>Course Code</th><th>Course Title</th>
              </tr>';

    if ($stmt->num_rows > 0) {
        $currentCategory = null;
        $currentProgram = null;


        while ($stmt->fetch()) {
            if ($currentCategory != $assessmentCategoryName) {
                echo '<tr class="thead-light"><th>'.$assessmentCategoryName.'</th><th colspan="3"></th></tr>';
                $currentCategory = $assessmentCategoryName;
            }
            if ($currentProgram != $programId) {
                echo '<tr class="thead-light"><th/><th>'.$programId.'</th><th>'.$programTitle.'</th><th/></tr>';
                $currentProgram = $programId;
            }
            echo '<tr><td colspan="2"></td><td>'.$courseId.'</td><td>'.$courseTitle.'</td></tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="4">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
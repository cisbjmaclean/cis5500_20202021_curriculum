<?php

/**
 * TitleSearch.php
 *
 * Returns a detailed list of courses.
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('../../Bootstrap/incPageHead.php');


    $query = 'SELECT CatalogYearName, CourseId, CourseTitle, ProgramId, ProgramTitle, GradeSchemeName, MinimumGrade FROM LookupCatalogYear INNER JOIN CourseCatalogYear ON LookupCatalogYear.CatalogYearID = CourseCatalogYear.CatalogYearId AND LookupCatalogYear.CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear) INNER JOIN ProgramCourse ON CourseCatalogYear.CourseCatalogYearId = ProgramCourse.CourseCatalogYearId INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramCatalogYearId = ProgramCourse.ProgramCatalogYearId INNER JOIN LookupProgramTitle ON LookupProgramTitle.ProgramTitleId = ProgramCatalogYear.ProgramTitleId INNER JOIN LookupGradeScheme ON CourseCatalogYear.GradeSchemeId = LookupGradeScheme.GradeSchemeId ORDER BY ProgramId, CourseId';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($catalogYear, $courseId, $courseTitle, $programId, $programTitle, $gradeScheme, $minimumGrade);


    echo '<table class="table table-bordered">';

    if ($stmt->num_rows > 0) {

        $currentProgram = null;


        echo '<tr class="thead-dark">
                <th>Program Code</th>
                <th>Program Title</th>
                <th colspan="5"></th>
              </tr>
              <tr class="thead-light">
                <th/>
                <th>Course Code</th>
                <th colspan="2">Course Title</th>
                <th colspan="3"/>
              </tr>';

        while ($stmt->fetch()) {


            if ($currentProgram != $programId) {
                echo '<tr class="thead-light">
                        <th>'.$programId.'</th>
                        <th>'.$programTitle.'</th>
                        <th colspan="5"/>
                      </tr>';

                $currentProgram = $programId;
            }
                echo '<tr>
                        <td/>
                        <td>'.$courseId.'</td>
                        <td colspan="2">'.$courseTitle.'</td>
                        <td>Passing Grade</td>
                        <td>'.$gradeScheme.'</td>
                        <td>'.$minimumGrade.'</td>
                      </tr>';


        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
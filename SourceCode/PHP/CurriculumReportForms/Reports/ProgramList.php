<?php

/**
 * ProgramList.php
 *
 * Return a list of programs.
 *
 * @author twhiten
 * @since 20201/03/05
 */

    include('../../Bootstrap/incPageHead.php');


    //Check if the user is authorized to edit the page
    $admin = false;
    $colspan = 2;
    if (isset($_SESSION['userType']) && $_SESSION['userType'] == 2) {
        $admin = true;
        $colspan = 3;
    }


    //Delete a selected program
    if (isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "DELETE FROM ProgramCourse WHERE ProgramCatalogYearId = ANY (SELECT ProgramCatalogYearId FROM ProgramCatalogYear WHERE ProgramId = ?)";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $id);

        $stmt->execute();

        $stmt->close();

        $query = "DELETE FROM ProgramCatalogYearConsultant WHERE ProgramCatalogYearId = ANY (SELECT ProgramCatalogYearId FROM ProgramCatalogYear WHERE ProgramId = ?)";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $id);

        $stmt->execute();

        $stmt->close();

        $query = "SELECT ProgramTitleId FROM ProgramCatalogYear WHERE ProgramId = ?";

        $stmt->bind_param("s", $id);

        $stmt->execute();

        $stmt->store_result();

        $stmt->bind_result($title);

        $stmt->fetch();

        $deletedTitle = $title;

        $query = "DELETE FROM ProgramCatalogYear WHERE ProgramId = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $id);

        $stmt->execute();

        $stmt->close();

        $query = "UPDATE LookupProgramTitle SET Active = false WHERE ProgramTitleId = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $deletedTitle);

        $stmt->execute();

        $stmt->close();

        $query = "DELETE FROM Program WHERE ProgramId = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $id);

        $stmt->execute();

        $stmt->close();


    }

    $query = 'SELECT Program.ProgramId, ProgramTitle, ProgramCatalogYear.ProgramCatalogYearId FROM Program INNER JOIN ProgramCatalogYear ON ProgramCatalogYear.ProgramId = Program.ProgramId  INNER JOIN LookupProgramTitle ON ProgramCatalogYear.ProgramTitleId = LookupProgramTitle.ProgramTitleId WHERE ProgramCatalogYear.CatalogYearId = (SELECT CatalogYearId FROM LookupCatalogYear WHERE CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear))';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($programId, $programTitle, $programCatalogYear);

    echo '<table class="table table-bordered">
              <tr class="thead-dark">
                <th colspan="'.$colspan.'"><h1>Program List</h1></th>
              </tr>
              <tr class="thead-light">
                <th>Program Id</th>
                <th>Program Title</th>';

            if ($admin) {
                echo '<th>Actions</th>';
            }


    echo      '</tr>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {
            echo '<tr><td>'.$programId.'</td><td>'.$programTitle.'</td>';

            if ($admin) {
                echo '<td><a href="../Forms/EditProgramForm.php?id='.$programCatalogYear.'">Edit</a> <a href="ProgramList.php?id='.$programId.'">Delete</a> </td>';
            }

            echo '</tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
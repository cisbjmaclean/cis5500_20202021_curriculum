<?php

/**
 * ProgramConsultant.php
 *
 * Return a list of consultants and their programs
 *
 * @author twhiten
 * @since 20201/03/09
 */

    include('../../Bootstrap/incPageHead.php');

    $admin = false;
    $colspan = 4;

    if (isset($_SESSION['userType']) && $_SESSION['userType'] == 2) {
        $admin = true;
        $colspan = 5;
    }

    if ($admin && isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "UPDATE Person SET Active = False WHERE PersonId = ?";

        $stmt = $db->prepare($query);

        $stmt->bind_param("s", $id);

        $stmt->execute();

        $stmt->close();
    }


    $query = 'SELECT PersonId, concat(PersonFirstName, " ", PersonLastName) AS PersonName, isProgramManager, isConsultant FROM Person WHERE Active';

    $stmt = $db->prepare($query);

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($personId, $personName, $isPM, $isC);


    if ($stmt->num_rows > 0) {


        echo '<table class="table table-bordered">
              <tr class="thead-dark">
                <th colspan="'.$colspan.'"><h1>List of People</h1></th>
              </tr>
              <tr class="thead-dark">
                <th colspan="2">Person Name</th>
                <th>Is Program Manager</th>
                <th>Is Consultant</th>';

            if ($admin) {
                echo '<th>Actions</th>';
            }

        echo '</tr>';


        while ($stmt->fetch()) {
            echo '<tr>
                    <td colspan="2">'.$personName.'</td>';


                    if ($isPM) {
                        echo '<td><input type="checkbox" checked="checked" class="form-check-inline" onclick="return false"></td>';
                    }
                    else {
                        echo '<td><input type="checkbox" class="form-check-inline" onclick="return false"></td>';
                    }

            if ($isC) {
                echo '<td><input type="checkbox" checked="checked" class="form-check-inline" onclick="return false"></td>';
            }
            else {
                echo '<td><input type="checkbox" class="form-check-inline" onclick="return false"></td>';
            }

            if ($admin) {
                echo '<td><a href="PersonList.php?id='.$personId.'">Deactivate</a> <a href="../Forms/EditPersonForm.php?id='.$personId.'">Edit Person</a></td>';
            }


            echo '</tr>';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td colspan="2">'.$error.'</td></tr>';
    }

    echo '</table>';

    include('../../Bootstrap/incFootPage.php');
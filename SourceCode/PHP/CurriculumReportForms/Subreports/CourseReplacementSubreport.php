<?php

/**
 * Course Replacement Subreport.php
 *
 * Return a list of replaced courses.
 *
 * @author twhiten
 * @since 20201/03/08
 */

    $subdb = new mysqli('localhost', 'root', '', 'twhiten_curriculum');

    $subquery = 'SELECT OldCourseId, OldCourseTitle FROM CourseReplacement INNER JOIN CourseCatalogYear ON CourseReplacement.NewCourseId = CourseCatalogYear.CourseId INNER JOIN LookupCatalogYear ON CourseCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearId';

    if (isset($courseSearch)) {
        $subquery .= ' AND NewCourseId = ?';
    }
    if (isset($yearSearch)) {
        $subquery .= ' AND CatalogYearName = ?';
    }
    else {
        $subquery .= ' AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)';
    }

        $subquery .= ' AND OldCourseTitle IS NOT NULL';


    $substmt = $subdb->prepare($subquery);

    if (!isset($yearSearch)) {
        $substmt->bind_param('s', $courseSearch);
    }
    else {
        $substmt->bind_param('ss', $courseSearch, $yearSearch);
    }

    $substmt->execute();
    $substmt->store_result();


    $substmt->bind_result($subId, $subTitle);




    if ($substmt->num_rows > 0) {

        while ($substmt->fetch()) {
            echo '<p>'.$subId.': '.$subTitle.'</p>';
        }

    }
    else {
        echo '<p>None</p>';
    }

    $substmt->close();
    $subdb->close();
<?php

/**
 * Learning Competency Subreport.php
 *
 * Return a list of learning competencies.
 *
 * @author twhiten
 * @since 20201/03/08
 */

    $subdb = new mysqli('localhost', 'root', '', 'twhiten_curriculum');

    $subquery = 'SELECT LearningCompetencyName FROM learningcompetency ORDER BY LearningCompetencyId';

    $substmt = $subdb->prepare($subquery);

    if (isset($courseSearch)) {
        $subquery .= ' AND ChildCourseId = ?';
    }
    if (isset($yearSearch)) {
        $subquery .= ' AND CatalogYearName = ?';
    }
    else {
        $subquery .= ' AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)';
    }

    $stmt->execute();
    $stmt->store_result();


    $stmt->bind_result($competency);


    ?>

    <div class="header">

    </div>

    <?php

    echo '<table>
              <tr class="tableHeader">
                <td>Learning Competency</td>
              </tr>';

    if ($stmt->num_rows > 0) {


        while ($stmt->fetch()) {
            echo '<tr><td>'.$competency.'</td></tr>
';
        }
    }
    else {
        $error = $db->errno . " " . $db->error;
        echo '<tr><td>'.$error.'</td></tr>';
    }

    echo '</table>';

    $substmt->close();
    $subdb->close();
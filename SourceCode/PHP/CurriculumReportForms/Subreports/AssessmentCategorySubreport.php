<?php

/**
 * AssessmentCategorySubreport.php
 *
 * Returns a list of assessment categories and category percents
 *
 * @author twhiten
 * @since 20201/03/03
 */


    $subdb = new mysqli('localhost', 'root', '', 'twhiten_curriculum');

    $subquery = 'SELECT AssessmentCategoryName, AssessmentCategoryPercent FROM AssessmentCategory INNER JOIN LookupAssessmentCategoryName ON LookupAssessmentCategoryName.AssessmentCategoryNameId = AssessmentCategory.AssessmentCategoryNameId INNER JOIN CourseCatalogYear ON AssessmentCategory.CourseCatalogYearId = CourseCatalogYear.CourseCatalogYearId INNER JOIN LookupCatalogYear ON CourseCatalogYear.CatalogYearId = LookupCatalogYear.CatalogYearId';



if (isset($courseSearch)) {
    $subquery .= ' AND CourseId = ?';
}
if (isset($yearSearch)) {
    $subquery .= ' AND CatalogYearName = ?';
}
else {
    $subquery .= ' AND CatalogYearName = (SELECT MAX(CatalogYearName) FROM LookupCatalogYear)';
}

    $subquery .= ' AND AssessmentCategoryName IS NOT NULL';

$substmt = $subdb->prepare($subquery);

if (!isset($yearSearch)) {
    $substmt->bind_param('s', $courseSearch);
}
else {
    $substmt->bind_param('ss', $courseSearch, $yearSearch);
}

$substmt->execute();
$substmt->store_result();


$substmt->bind_result($name, $percent);




    if ($substmt->num_rows > 0) {
        echo '<h3>Assessment Category/Percent: </h3>';

        while ($substmt->fetch()) {
            echo '<p>'.$name.' '.$percent.'</p>
    ';
        }

    }
    else {
        echo '<p>None</p>';
    }

$substmt->close();
$subdb->close();

<?php
include("../../Bootstrap/incPageHead.php");
?>
    <section class="page-section" id="contact">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class="mt-0">About us</h2>
                    <hr class="divider my-4" />
                    <p class="text-muted mb-5">The curriculum database is used to store
                        and organise information about Holland college's courses, programs and people involved with them. The database is managed by Colleen Myer.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                    <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                    <div>+1 902-566-9645</div>
                </div>
                <div class="col-lg-4 mr-auto text-center">
                    <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                    <!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                    <a class="d-block" href="mailto:cmmyer@hollandcollege.com">
                        cmmyer@hollandcollege.com
                    </a>
                </div>
            </div>
        </div>
    </section>

<?php
require("../../Bootstrap/incFootPage.php");
<?php
/*
 Date: March 10, 2021
 Author: Cameron MacDonald
 modified by: Marie Elodie Ineza
 Purpose: Page to display room and link icons
*/
session_start();

// Check if the user is logged in, if not then redirect him to login page
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}

$pageTitle = "Room Service - Home";
require("../../Bootstrap/incPageHead.php");

?>

    <?php

    //Connecting to DB
    @ $db = new mysqli('localhost', 'root', '', 'curriculum');



if (mysqli_connect_errno()) {
        echo "Error: Could not connect to database.  Please try again later.</body></html>";
        exit;
    }


    if(isset($_GET["codeTypeId"])){
        $codeTypeId = $_GET["codeTypeId"];
        $query = "SELECT * FROM codevalue WHERE codeTypeId = '$codeTypeId';";

    } else {
        $query = "SELECT * FROM codevalue;";
    }


    //Default query statement


    // use query() method
    $result = $db->query($query);

    // gather number of rows we got in the query result set
    $num_results = $result->num_rows;

    echo "<table class='table table-striped '>";
    echo "<thead>";
    if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the books retrieved with the query
        $codeTypes= $result->fetch_all(MYSQLI_ASSOC);
        echo "<table class='table table-bordered'><tr>";
//This dynamically retieves header names
        foreach ($codeTypes[0] as $k => $v) {

            echo "<th>" . $k . "</th>";

        }

        echo "<th>Action</th>";

        echo "</tr></thead>";
        echo "<tbody>";
//Create a new row for each book
        foreach ($codeTypes as $codetype) {
            echo "<tr>";
            $i = 0;

            foreach($codetype as $k => $v){
                if($k == 'codeValueSequence'){
                    $codeValueId = $v;
                }
            }

            foreach ($codetype as $k => $v) {

                if ($k == 'codeTypeId') {
                    echo "<td>" . $v . "</td>";
                    $codetypeId = $v;
                }  else {
                    echo "<td>" . $v . "</td>";
                }

                if (($i == count($codetype) - 1)) {
                    echo "<td>";
                    echo "<div class='btn-toolbar'>";
                    echo "<a href='editcodeValue.php?codeTypeId=" . $codetypeId .  "&codeValueId=" . $codeValueId . "' title='Edit Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Edit</a>";
                    echo "<a href='deleteCodeValue.php?codeTypeId=" . $codetypeId . "&codeValueId=" . $codeValueId . "' title='Delete Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Delete</a>";
                    echo "</div>";
                    echo "</td>";
                }
                $i++;
            }
            echo "</tr>";

        }

        echo "<tr><td colspan='6'>";
        echo "<a href='add_codeType.php' title='View Record' class='btn btn-info' data-toggle='tooltip'>Add a new codeType</a>";
        echo "</td></tr>";

        echo "</tbody>";
        echo "</table>";

    }
    //include ("incPageFoot.php");
    ?>

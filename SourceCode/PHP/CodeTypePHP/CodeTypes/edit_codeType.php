<?php
/*
 Date: March 10, 2021
 Author: Cameron MacDonald
 modified by: Marie Elodie Ineza
 Purpose: Refactoring to be edit room from the database
*/
session_start();

// Check if the user is logged in, if not then redirect him to login page
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}



$pageTitle = "Room Service - Edit";
require ("../../Bootstrap/incPageHead.php");


//This page will present the user with an edit form and is also used to process form input

@ $db = new mysqli('localhost', 'root', '', 'curriculum');

if (mysqli_connect_errno()) {
    echo "Error: Could not connect to database.  Please try again later.</body></html>";
    exit;
}



$codeTypeId = $_GET['codeTypeId'];
$englishDescription = "";
$frenchDescription = "";
$createdDateTime = "";
$createdUserId = "";
$updatedDateTime = "";
$updatedUserId = "";



//If submit button is pushed...
if (isset($_POST["submit"])) {

    // create short variable names
    $codeTypeId = $_POST['codeTypeId'];
    $englishDescription = $_POST['englishDescription'];
    $frenchDescription = $_POST['frenchDescription'];
    $createdDateTime = $_POST['createdDateTime'];
    $createdUserId = $_POST['createdUserId'];
    $updatedDateTime = $_POST['updatedDateTime'];
    $updatedUserId = $_POST['updatedUserId'];


    $codeTypeId = $db->real_escape_string($codeTypeId);

    // get the data for just the book we want to edit!
    $query = "SELECT * FROM codetype WHERE codeTypeId = '$codeTypeId'";
    $result = $db->query($query);

    $num_results = $result->num_rows;

    //If statement for if no rows are returns from query
    if ($num_results == 0) {
        $message = "Error";
        $update = false;
    } else {
        $row = $result->fetch_assoc();

        $englishDescription = $row['englishDescription'];
        $frenchDescription = $row['frenchDescription'];
        $createdDateTime = $row['createdDateTime'];
        $createdUserId = $row['createdUserTime'];
        $updatedDateTime = $row['updatedDateTime'];
        $updatedUserId = $row['updatedUserId'];
        $update = true;
    }

    $result->free();
    $db->close();




    //Connect to DB
    @ $db = new mysqli('localhost', 'root', '', 'canes3');

    if (mysqli_connect_errno()) {
        echo "Error: Could not connect to database.  Please try again later.</body></html>";
        exit;
    }



    //Validation that the fields are not empty
    if (empty($codeTypeId) || empty($englishDescription) || empty($frenchDescription)|| empty($createdDateTime) || empty($createdDateTime)
        || empty($createdUserId) || empty($updatedDateTime) || empty($updatedUserId)) {
        //Setting message
        $message = "<div class='alert alert-danger'>One or more fields was empty. <a href=\"javascript:history.back()\">Go Back</a></div>";
        echo $message;
        exit;
    }

    //Setting to real escape strings for sql injection prevention
//

    $codeTypeId = $db->real_escape_string($codeTypeId);
    $englishDescription = $db->real_escape_string($englishDescription);
    $frenchDescription = $db->real_escape_string($frenchDescription);
    $createdDateTime = $db->real_escape_string(doubleval($createdDateTime));
    $createdUserId = $db->real_escape_string($createdUserId);
    $updatedDateTime = $db -> real_escape_string($updatedDateTime);
    $updatedUserId  =  $db -> real_escape_string($updatedUserId);


    $query2 = "UPDATE codetype SET englishDescription='$englishDescription',frenchDescription='$frenchDescription', createdDateTime='$createdDateTime', createdUserId='$createdUserId', updatedDateTime=$updatedDateTime, updatedUserId=$updatedUserId  WHERE codeTypeId=$codeTypeId LIMIT 1";

    //Store the query in the result variable
    $result1 = $db->query($query2);

    if ($result1) {
        //Setting message
        $message = "<div class='alert alert-success'>Edit Success <a href='codeType.php'>View codetypes</a></div>";
        echo $message;
        exit;
    } else {
        //Setting message
        $message = "<div class='alert alert-danger'>There was a problem with your query. <a href=\"javascript:history.back()\">Go Back</a></div>";
        echo $message;
        exit;
    }

    $db->close();
}
?>
<body>
<h2>Edit codetype </h2>
<form action="edit_codeType.php" method="post">

        <fieldset  class="scheduler-border">
        <div class="form-group">
            <label for="codeTypeId">codeTypeId:</label>
            <input type="text" class="form-control" id="codeTypeId" value='<?php echo $codeTypeId; ?>' placeholder="Enter codeTypeId" name="codeTypeId">
        </div>
        <div class="form-group">
            <label for="englishDescription">englishDescription:</label>
            <input type="text" class="form-control" id="englishDescription" value='<?php echo $englishDescription; ?>' placeholder="Enter englishDescription" name="englishDescription">
        </div>
        <div class="form-group">
            <label for="frenchDescription">frenchDescription:</label>
        <input type="text" class="form-control" id="author" value='<?php echo $frenchDescription; ?>' placeholder="Enter frenchDescription" name="frenchDescription">
        </div>

        <div class="form-group">
            <label for="createdDateTime">createdDateTime:</label>
            <input type="text" class="form-control" id="frenchDescription" value='<?php echo $createdDateTime; ?>' placeholder="Enter createdDateTime" name="createdDateTime">
        </div>
        <div class="form-group">
            <label for="createdUserId">createdUserId:</label>
            <input type="text" class="form-control" id="createdUserId" value='<?php echo $createdUserId; ?>' placeholder="Enter createdUserId" name="createdUserId">
        </div>
        <div class="form-group">
            <label for="updatedDateTime">updatedDateTime:</label>
            <input type="text" class="form-control" id="updatedDateTime" value='<?php echo $updatedDateTime; ?>' placeholder="Enter updatedDateTime" name="updatedDateTime">
        </div>
        <div class="form-group">
            <label for="updatedUserId">updatedUserId:</label>
            <input type="text" class="form-control" id="updatedUserId" value='<?php echo $updatedUserId; ?>' placeholder="Enter updatedUserId" name="updatedUserId">
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Update</button>
        </div>
        </fieldset>
</form>
<?php
?>

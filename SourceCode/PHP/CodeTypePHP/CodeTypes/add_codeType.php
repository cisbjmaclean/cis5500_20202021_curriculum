<?php
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}
// Initialize the session
//session_start();
//
//// Check if the user is logged in, if not then redirect to login page
//if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
//    header("location: login.php");
//    exit;
//}
//
//// Include config file
////require_once "config.php";
//?>
<!--<!doctype html>-->
<!--<html>-->
<!--<head>-->
<!--    <title>CodeType</title>-->
<!--    <meta charset="utf-8">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">-->
<!--    <link rel="stylesheet" href="css/custom.css">-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
<!--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>-->
<!--</head>-->
<!--<body>-->
<!--<div id="container">-->
<!---->
<!--    <h1>Book-O-Rama Book - Add Book</h1>-->
    <?php
 //   require_once("lib/utilities.php");
    require ("../../Bootstrap/incPageHead.php");
    if (isset($_POST['submit'])) {

        // create short variable names
        $codeTypeId = $_POST['codeTypeId'];
        $englishDescription = $_POST['englishDescription'];
        $frenchDescription = $_POST['frenchDescription'];
        $createdDateTime = $_POST['createdDateTime'];
        $createdUserId = $_POST['createdUserId'];
        $updatedDateTime = $_POST['updatedDateTime'];
        $updatedUserId = $_POST['updatedUserId'];

        if (empty($codeTypeId) || empty($englishDescription) || empty($frenchDescription) || empty($createdDateTime) || empty($createdDateTime)
        || empty($createdUserId) || empty($updatedDateTime) || empty($updatedUserId)) {

            header("location:newCodeType.php?error=empty");
            exit();

        }
        //Create DB object
        require_once('lib/config.php');

        $codeTypeId = $mysqli->real_escape_string($codeTypeId);
        $englishDescription = $mysqli->real_escape_string($englishDescription);
        $frenchDescription = $mysqli->real_escape_string($frenchDescription);
        $createdDateTime = $mysqli->real_escape_string(doubleval($createdDateTime));
        $createdUserId = $mysqli->real_escape_string($createdUserId);
        $updatedDateTime = $mysqli -> real_escape_string($updatedDateTime);
        $updatedUserId  =  $mysqli -> real_escape_string($updatedUserId);



        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.  Please try again later.";
            exit;
        }

        $query = "INSERT INTO codetype VALUES (NULL,'" . $codeTypeId . "', '" . $englishDescription . "', '" . $frenchDescription . "', '" . $createdDateTime . "', '" .$createdUserId."','".
            $updatedDateTime."', '".$updatedUserId."')";
       // echo $query;
        $result = $mysqli->query($query);

        if ($result) {
            echo $mysqli->affected_rows . " book inserted into database. <a href='add_codeType.php'>Add another?</a>";

            //Display book inventory
            $query = "SELECT * FROM codetype";
// Here we use our $mysqli object created above and run the query() method. We pass it our query from above.
            $result = $mysqli->query($query);

            $num_results = $result->num_rows;

            echo "<p>Number of books found: " . $num_results . "</p>";

            echo "<h2>CIS Book Inventory</h2>";
            echo "<table class='table table-bordered table-striped'>";
            echo "<thead>";
            if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the books retrieved with the query
                $codetypes = $result->fetch_all(MYSQLI_ASSOC);
                echo "<table class='table table-bordered'><tr>";

//This dynamically retrieves header names
                foreach ($codetypes[0] as $k => $v) {
                    echo "<th>" . $k . "</th>";
                }
                echo "</thead>";
                echo "<tbody>";
//Create a new row for each book
                foreach ($codetypes as $codetype) {
                    echo "<tr>";

                    foreach ($codetype as $k => $v) {

                        echo "<td>" . $v . "</td>";

                    }
                    echo "</tr>";
                }

                echo "</tbody>";
                echo "</table>";
            }
            $result->free();
            $mysqli->close();

        } else {
            echo "An error has occurred.  The item was not added. <a href='add_codeType.php'>Try again?</a>";
        }

    } else {

        header("location:newCodeType.php?error=noform");
        exit();
    }
    ?>
</div>

</body>
</html>

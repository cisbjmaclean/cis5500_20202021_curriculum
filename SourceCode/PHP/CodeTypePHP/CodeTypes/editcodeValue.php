<?php
/*
 Date: March 10, 2021
 Author: Cameron MacDonald
 modified by: Marie Elodie Ineza
 Purpose: Refactoring to be edit room from the database
*/
session_start();

// Check if the user is logged in, if not then redirect him to login page
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}

require("../../Bootstrap/incPageHead.php");

$pageTitle = "Room Service - Edit";
//include("incPageHead.php");


//This page will present the user with an edit form and is also used to process form input

@ $db = new mysqli('localhost', 'root', '', 'curriculum');

if (mysqli_connect_errno()) {
    echo "Error: Could not connect to database.  Please try again later.</body></html>";
    exit;
}
$englishDescription = "";
$frenchDescription = "";
$createdDateTime = "";
$createdUserId = "";
$updatedDateTime = "";
$updatedUserId = "";
////Logged in validation
if (isset($_SESSION["valid"])) {


    $codeTypeId = $_GET['codeTypeId'];
    $codeValueId = $_GET['codeValueId'];

}

//If submit button is pushed...
    if (isset($_POST["submit"])) {

        // create short variable names
        $codeTypeId = $_POST['codeTypeId'];
        $codeValueId = $_POST['codeValueId'];
        $englishDescription = $_POST['englishDescription'];
        $frenchDescription = $_POST['frenchDescription'];


        $updatedDateTime = $_POST['updatedDateTime'];
        $updatedUserId = $_POST['updatedUserId'];


        $codeTypeId = $db->real_escape_string($codeTypeId);
        $codeValueId = $db->real_escape_string($codeValueId);

        // get the data for just the book we want to edit!
        $query = "SELECT * FROM codevalue WHERE codeTypeId = '$codeTypeId'";
        $result = $db->query($query);

        $num_results = $result->num_rows;

        //If statement for if no rows are returns from query
        if ($num_results == 0) {
            $message = "Error";
            $update = false;
        } else {
            $row = $result->fetch_assoc();

            $englishDescription = $row['englishDescription'];
            $frenchDescription = $row['frenchDescription'];
            $createdDateTime = $row['createdDateTime'];
            $createdUserId = $row['createdUserTime'];
            $updatedDateTime = $row['updatedDateTime'];
            $updatedUserId = $row['updatedUserId'];
            $update = true;
        }

        $result->free();
        $db->close();


        //Connect to DB
        @ $db = new mysqli('localhost', 'root', '', 'canes3');

        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.  Please try again later.</body></html>";
            exit;
        }


        //Validation that the fields are not empty
        if (empty($codeTypeId) || empty($englishDescription) || empty($frenchDescription)
             || empty($updatedDateTime) || empty($updatedUserId)) {
            //Setting message
            $message = "<div class='alert alert-danger'>One or more fields was empty. <a href=\"javascript:history.back()\">Go Back</a></div>";
            echo $message;
            exit;
        }

        //Setting to real escape strings for sql injection prevention
//

        $codeTypeId = $db->real_escape_string($codeTypeId);
        $codeValueId = $db->real_escape_string($codeValueId);
        $englishDescription = $db->real_escape_string($englishDescription);
        $frenchDescription = $db->real_escape_string($frenchDescription);
        $updatedDateTime = $db->real_escape_string($updatedDateTime);
        $updatedUserId = $db->real_escape_string($updatedUserId);


        $query2 = "UPDATE codevalue SET englishDescription='$englishDescription',frenchDescription='$frenchDescription', updatedDateTime='$updatedDateTime', updatedUserId='$updatedUserId'  WHERE codeTypeId='$codeTypeId' AND codeValueSequence ='$codeValueId' LIMIT 1";

        //Store the query in the result variable
        $result1 = $db->query($query2);

        if ($result1) {
            //Setting message
            $message = "<div class='alert alert-success'>Edit Success <a href='codeValue.php'>View code values</a></div>";
            echo $message;
            exit;
        } else {
            //Setting message
            $message = "<div class='alert alert-danger'>There was a problem with your query. <a href=\"javascript:history.back()\">Go Back</a></div>";
            echo $message;
            exit;
        }

        $db->close();
    }

?>
<body>
<h2>Edit codetype </h2>
<form action="editcodeValue.php" method="post">

    <fieldset  class="scheduler-border">
        <div class="form-group">
            <input type="hidden" class="form-control" id="codeValueId" value='<?php echo $codeValueId; ?>' placeholder="Enter Code Sequence Value" name="codeValueId">
        </div>
        <div class="form-group">
            <input type="hidden" class="form-control" id="codeValueId" value='<?php echo $codeTypeId; ?>' placeholder="Enter Code Sequence Value" name="codeTypeId">
        </div>
        <div class="form-group">
            <label for="English Description">English Description:</label>
            <input type="text" class="form-control" id="englishDescription" value='<?php echo $englishDescription; ?>' placeholder="Enter English Description" name="englishDescription">
        </div>
        <div class="form-group">
            <label for="French Description">French Description:</label>
            <input type="text" class="form-control" id="author" value='<?php echo $frenchDescription; ?>' placeholder="Enter French Description" name="frenchDescription">
        </div>
        <div class="form-group">
            <label for="Updated DateTime">Updated DateTime:</label>
            <input type="text" class="form-control" id="updatedDateTime" value='<?php echo $updatedDateTime; ?>' placeholder="Enter Updated DateTime" name="updatedDateTime">
        </div>
        <div class="form-group">
            <label for="Updated UserId">Updated UserId:</label>
            <input type="text" class="form-control" id="updatedUserId" value='<?php echo $updatedUserId; ?>' placeholder="Enter Updated UserId" name="updatedUserId">
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Update</button>
        </div>
    </fieldset>
</form>
<?php
?>

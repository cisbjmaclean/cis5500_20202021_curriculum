<?php
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}
// Updated by: Don Bowers,jdkitson
// Date: November 22, 2019
// Purpose: Demo DB and PHP inserting - updating with PHP

?>

<!doctype html>
<html>

    <?php
    require("../../Bootstrap/incPageHead.php");

    // set up connection
    require("config.php");

    //Sort type
    //$sort = " order by books.title asc";

    //Display book inventory
    $query = "SELECT codeTypeId, englishDescription,frenchDescription, createdDateTime, createdUserId, updatedDateTime,updatedUserId FROM codetype";

    // Here we use our $db object created above and run the query() method. We pass it our query from above.
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;
    if(isset($_GET['msg'])) {
        echo "<p>{$_GET['msg']}</p>";
    }

//    echo "<h2>CIS Book Inventory</h2>";
    echo "<table class='table table-striped'>";
    echo "<thead>";
    if ($num_results > 0) {
//  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the books retrieved with the query
        $codetypes = $result->fetch_all(MYSQLI_ASSOC);
        echo "<table class='table table-bordered'><tr>";
//This dynamically retieves header names
        foreach ($codetypes[0] as $k => $v) {

                echo "<th>" . $k . "</th>";

        }

            echo "<th>Action</th>";

        echo "</tr></thead>";
        echo "<tbody>";
        //Create a new row for each book
        foreach ($codetypes as $codetype) {
            echo "<tr>";
            $i = 0;

            foreach ($codetype as $k => $v) {

                if ($k == 'codeTypeId') {
                    $codeTypeId = $v;
                    echo "<td><a href='codeValue.php?codeTypeId=" . $codeTypeId . "'> " . $v . "</a></td>";

                } else {
                    echo "<td>" . $v . "</td>";
                }
                    if (($i == count($codetype) - 1)) {
                    echo "<td>";
                    echo "<div class='btn-toolbar'>";
                   echo "<a href='edit_codeType.php?codeTypeId=" . $codeTypeId . "' title='Edit Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Edit</a>";
                        echo "<a href='deletecodeType.php?codeTypeId=" . $codeTypeId . "' title='Delete Record' class='btn btn-info btn-xs' data-toggle='tooltip'>Delete</a>";
                       echo "</div>";
                        echo "</td>";
                }
                $i++;
            }
            echo "</tr>";

        }



        echo "</tbody>";
        echo "</table>";

        echo "<a href='newCodeType.php' title='View Record' class='btn btn-info' data-toggle='tooltip'>Add a new codeType</a>";

    }
    // free result and disconnect
    $result->free();
    $mysqli->close();
    include ("../../Bootstrap/incFootPage.php");
    ?>

</html>
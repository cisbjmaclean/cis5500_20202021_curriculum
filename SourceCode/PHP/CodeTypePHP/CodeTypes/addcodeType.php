<?php
/*
 Date: March 10, 2021
 Author: Cameron MacDonald
 modified by: Marie Elodie Ineza
 Purpose: Refactoring to be add room to the database
*/
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../../UserAccessPHP/UserAccess/login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: ../../UserAccessPHP/UserAccess/welcome.php");
    exit;
}
$pageTitle = "Room Service - Add";
//include("incPageHead.php");

//This page will present the user with an add form and is also used to process form input
@ $db = new mysqli('localhost', 'root', '', 'curriculum');

if (mysqli_connect_errno()) {
    echo "Error: Could not connect to database.  Please try again later.</body></html>";
    exit;
}



    //If submit button is pushed...
    if (isset($_POST["submit"])) {

        //Connect to DB
        @ $db = new mysqli('localhost', 'root', '', 'curriculum');

        if (mysqli_connect_errno()) {
            echo "Error: Could not connect to database.  Please try again later.</body></html>";
            exit;
        }
        require ("../../Bootstrap/incPageHead.php");

        // create short variable names
        $codeTypeId = $_POST['codeTypeId'];
        $englishDescription = $_POST['englishDescription'];
        $frenchDescription = $_POST['frenchDescription'];
        $createdDateTime = $_POST['createdDateTime'];
        $createdUserId = $_POST['createdUserId'];


        //Validation that the fields are not empty
        if (empty($codeTypeId) || empty($englishDescription) || empty($frenchDescription) || empty($createdDateTime) || empty($createdDateTime)
            || empty($createdUserId) ) {
            //Setting message
            $message = "<div class='alert alert-danger'>One or more fields was empty. <a href=\"javascript:history.back()\">Go Back</a></div>";
            echo $message;
            exit;
        }
        //Setting to real escape strings for sql injection prevention
        $codeTypeId = $db->real_escape_string($codeTypeId);
        $englishDescription = $db->real_escape_string($englishDescription);
        $frenchDescription = $db->real_escape_string($frenchDescription);
        $createdDateTime = $db->real_escape_string(doubleval($createdDateTime));
        $createdUserId = $db->real_escape_string($createdUserId);



        $query2 = "INSERT INTO codetype SET codeTypeId = '$codeTypeId ', englishDescription= '$englishDescription', frenchDescription='$frenchDescription ', createdDateTime = ' $createdDateTime', createdUserId ='$createdUserId'
            ";
        //Store the query in the result variable
        $result1 = $db->query($query2);

        if ($result1) {
            //Setting message
            $message = "<div class='alert alert-success'>Add Success <a href='codeType.php'>View All Types</a></div>";
            echo $message;
            exit;
        } else {
            //Setting message
            $message = "<div class='alert alert-danger'>There was a problem with your query. <a href=\"javascript:history.back()\">Go Back</a></div>";
            echo $message;
            exit;
        }

        $db->close();
    }



?>
<body>
<h2>Add codeType</h2>

<form action="addcodeType.php" method="post">

    <div class="form-group">
        <label for="codeTypeId">codeTypeId:</label>
        <input type="text" class="form-control" id="codeTypeId" placeholder="Enter codeTypeId" name="codeTypeId">
    </div>
    <div class="form-group">
        <label for="englishDescription">englishDescription:</label>
        <input type="text" class="form-control" id="author" placeholder="Enter englishDescription" name="englishDescription">
    </div>
    <div class="form-group">
        <label for="frenchDescription">frenchDescription:</label>
        <input type="text" class="form-control" id="author" placeholder="Enter frenchDescription" name="frenchDescription">
    </div>
    <div class="form-group">
        <label for="createdDateTime">createdDateTime</label>
        <input type="text" class="form-control" id="price" placeholder="Enter createdDateTime" name="createdDateTime">
    </div>
    <div class="form-group">
        <label for="createdUserId">createdUserId</label>
        <input type="text" class="form-control" id="price" placeholder="Enter createdUserId" name="createdUserId">
    </div>

    <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
    </div>
</form>

<?php
//include("incPageFoot.php");
?>

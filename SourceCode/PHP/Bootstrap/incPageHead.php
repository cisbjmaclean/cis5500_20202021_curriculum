<?php
/* Date: Dec 9, 2020
   Author: Cole Gallant and Donnie McKinnon
   Purpose: Final Practical 2

   Date: Feb 28, 2021
   Contributors: Cameron MacDonald
   Purpose: Top section of page for SEHMS Web App */

if(session_status() == PHP_SESSION_NONE){
    //start a session
    session_start();
}
$db = new mysqli('localhost', 'root', '', 'curriculum');

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Curriculum</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/hollandLogo.png" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="../../Bootstrap/css/styles.css" rel="stylesheet" />
    <link href="../../Bookstrap/css/custom.css" rel="stylesheet" />
<!--    <title>--><?php //echo $pageTitle; ?><!--</title>-->
</head>

<!--    <body >-->

    <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">Curriculum</a>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto my-2 my-lg-0">



                            <?php
                            if(isset($_SESSION["userType"]) && $_SESSION["userType"] == 2) {
                            echo'<li  class="nav-item"><div class="dropdown">';
                            echo'<a class="nav-link js-scroll-trigger" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                            Forms
                            <span class="caret"></span>
                        </a>';
                            echo'<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';

                                echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Forms/AddCourseForm.php">Add Course</a></li>';
                                echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Forms/AddPersonForm.php">Add Person</a></li>';
                                echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Forms/AddProgramForm.php">Add Program</a></li>';
                                echo'</ul>';
                                echo'</div></li>';
                            }?>


                    <li class="nav-item"><div class="dropdown">
<!--                        <a class="nav-link js-scroll-trigger" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">-->
<!--                            Reports-->
<!--                            <span class="caret"></span>-->
<!--                            </a>-->

                        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">

                            <?php
                             if (isset($_SESSION['userType'])) {
                                 echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/PersonList.php">Person List</a></li>';
                             }
                            ?>

                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/AssessmentCategoryName.php">Assess. Cat. Name</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/AssessmentCategoryQuery.php">Assess. Cat. Query</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/AssessmentCategorySearch.php">Assess. Cat. Search</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CommonCourseList.php">Common Course List</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseCreditValue.php">Course Credit Value</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseCreditValueNoReqs.php">CCV No Reqs</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseHourBreakdown.php">Course Hour Breakdown</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseProgramFind.php">Course Program Find</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/EntrepreneurshipCollege.php">Entr. College</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/EntrepreneurshipExtended.php">Entr. Extended</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/NCPR.php">NCPR</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/OJTPracticumCourseList.php">OJT Pract. Course List</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/OJTPracticumProgramList.php">OJT Pract. Program List</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/OpenStudies.php">Open Studies</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/PlarCourses.php">Plar Courses</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramConsultant.php">Program Consultant</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramCourseListDetail.php">Prog. Course Detail</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramList.php">Program List</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramListRequiredGrade.php">Prog. List Req. Grade</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/QF132CourseOutline.php">Quality Form 132</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ResearchCourses.php">Research Courses</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ReserveSubjectCode.php">Reserve Subject Code</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/SubjectCodes.php">Subject Codes</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/TitleSearch.php">Title Search</a></li>

                        </ul>
                        </div></li>

                    <li class="nav-item"><div class="dropdown">
                            <a class="nav-link js-scroll-trigger" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Program
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <?php
                                if (isset($_SESSION['userType'])) {
                                    echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/PersonList.php">Person List</a></li>';
                                }
                                ?>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramList.php">Program List</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramListRequiredGrade.php">Prog. List Req. Grade</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/OJTPracticumProgramList.php">OJT Pract. Program List</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramConsultant.php">Program Consultant</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/NCPR.php">NCPR</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramListRequiredGrade.php">Prog. List Req. Grade</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ProgramCourseListDetail.php">Prog. Course Detail</a></li>






                            </ul>
                        </div></li>


                    <li class="nav-item"><div class="dropdown">
                            <a class="nav-link js-scroll-trigger" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Course
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/QF132CourseOutline.php">Quality Form 132</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CommonCourseList.php">Common Course List</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseCreditValue.php">Course Credit Value</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseHourBreakdown.php">Course Hour Breakdown</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseProgramFind.php">Course Program Find</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/EntrepreneurshipCollege.php">Entr. College</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/EntrepreneurshipExtended.php">Entr. Extended</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/OJTPracticumCourseList.php">OJT Pract. Course List</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/PlarCourses.php">Plar Courses</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ResearchCourses.php">Research Courses</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/TitleSearch.php">Title Search</a></li>

                            </ul>
                        </div></li>

                    <li class="nav-item"><div class="dropdown">
                            <a class="nav-link js-scroll-trigger" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Simple Filter Queries
                                <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/PermissionStatement.php">Permission Statement</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/AssessmentCategoryName.php">Assess. Cat. Name</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/AssessmentCategoryQuery.php">Assess. Cat. Query</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/AssessmentCategorySearch.php">Assess. Cat. Search</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/OpenStudies.php">Open Studies</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ResearchCourses.php">Research Courses</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/CourseCreditValueNoReqs.php">CCV No Reqs</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/ReserveSubjectCode.php">Reserve Subject Code</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="../../CurriculumReportForms/Reports/SubjectCodes.php">Subject Codes</a></li>
                            </ul>
                        </div></li>
                </ul>




            </div>

            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto my-2 my-lg-0">

                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../../UserAccessPHP/UserAccess/login.php">Home</a></li>
                    <?php
                    if(isset($_SESSION["userType"]) && $_SESSION["userType"] == 2) {
//                        echo'<li class="nav-item"><a class="nav-link js-scroll-trigger" href="../../CodeTypePHP/CodeTypes/codeType.php">Codes</a></li>
//';
//                        echo"";
                    }
                    ?>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../../About/about/about.php">About</a></li>
                    <li class="nav-item"><div class="dropdown">
                            <a class="nav-link js-scroll-trigger" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Account
                                <span class="caret"></span>
                            </a>

                                <?php
                                if(isset($_SESSION["userType"]) && $_SESSION["userType"] == 2) {
                                    echo '<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">';
                                    echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../UserAccessPHP/UserAccess/reset-password.php">Reset password</a></li>';
                                    echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../UserAccessPHP/UserAccess/update-user.php">Reset name and account type</a></li>';
                                    echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../UserAccessPHP/UserAccess/delete-user.php">Delete account</a></li>';
                                    echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../UserAccessPHP/UserAccess/admin-view-users.php">View User Accounts</a></li>';
                                    echo '<li role="presentation"><a role="menuitem" tabindex="-1" href="../../UserAccessPHP/UserAccess/register.php">Add New Account</a></li>';
                                    echo '</ul>';
                                }


                                ?>


                        </div>
                    </li>

                    <?php
                    if (isset($_SESSION['userType'])) {
                        echo '<li class="nav-item"><a class="nav-link js-scroll-trigger" href="../../UserAccessPHP/UserAccess/logout.php">Logout</a></li>';
                    }
                    else {
                        echo '<li class="nav-item"><a class="nav-link js-scroll-trigger" href="../../UserAccessPHP/UserAccess/login.php">Login</a></li>';
                    }
                    ?>

                </ul>
            </div>
        </div>
    </nav>

<body>
<div class="container">


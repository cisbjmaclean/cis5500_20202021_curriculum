<?php
    // Initialize the session
    require("../../Bootstrap/incPageHead.php");
//    session_start();

    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login.php");
        exit;
    }
    if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
        header("location: welcome.php");
        exit;
    }
    require_once "config.php";
    // Define variables and initialize with empty values
    $new_password = $confirm_password = $username = "";
    $new_password_err = $confirm_password_err = $username_err = $new_name = $id = "";
    $id = $_GET['id'];



    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST") {

        $userType = $_POST['userType'];
        $username = trim($_POST["username"]);
        $new_name = trim($_POST['new_name']);
        $id = $_POST['id'];
        $newUserAccess = $_POST['userActive'];

        // Validate new password
        if (empty(trim($_POST["new_password"]))) {
            $new_password_err = "Please enter the new password.";
        } elseif (strlen(trim($_POST["new_password"])) < 6) {
            $new_password_err = "Password must have atleast 6 characters.";
        } else {
            $new_password = trim($_POST["new_password"]);
        }

        // Validate confirm password
        if (empty(trim($_POST["confirm_password"]))) {
            $confirm_password_err = "Please confirm the password.";
        } else {
            $confirm_password = trim($_POST["confirm_password"]);
            if (empty($new_password_err) && ($new_password != $confirm_password)) {
                $confirm_password_err = "Password did not match.";
            }
        }


// Check input errors before updating the database
        if (empty($new_password_err) && empty($confirm_password_err)) {
            // Prepare an update statement
            // Changed SQL parameters
            $sql = "UPDATE useraccess SET username = ?, password = ?, name = ?, userAccessStatusCode = ?, userTypeCode = ? WHERE userAccessId = ?";

            if ($stmt = $mysqli->prepare($sql)) {
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("sssiii", $param_username, $param_password, $param_name, $param_userAccess, $param_userType, $param_id);

                // Set parameters
                $param_username = $username;
                $param_name = $new_name;
                $param_password = password_hash($new_password, PASSWORD_DEFAULT);
                $param_id = $id;
                $param_userType = $userType;
                $param_userAccess = $newUserAccess;

                // Attempt to execute the prepared statement
                if ($stmt->execute()) {
                    // Password updated successfully. Destroy the session, and redirect to login page
                    session_destroy();
                    header("location: login.php");
                    exit();
                } else {
                    echo "Oops! Something went wrong. Please try again later.";
                }

                // Close statement
                $stmt->close();
            }
            // Close connection
            $mysqli->close();
        }
    }
?>
<!---->
<!--<!DOCTYPE html>-->
<!--<html lang="en">-->
<!--<head>-->
<!--    <meta charset="UTF-8">-->
<!--    <title>Reset Password</title>-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">-->
<!--    <style type="text/css">-->
<!--        body{ font: 14px sans-serif; }-->
<!--        .wrapper{ width: 350px; padding: 20px; }-->
<!--    </style>-->
<!--</head>-->
<!--<body>-->
<!--<div class="wrapper">-->
<fieldset>
    <h2>Update Information</h2>
    <p>Please fill out this form to change your information.</p>
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <div class="form-group">
            <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>">
            <span class="help-block"></span>
        </div>
        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
            <span class="help-block"><?php echo $username_err; ?></span>
        </div>
        <div class="form-group">
            <label>New Name</label>
            <input type="text" name="new_name" class="form-control" value="<?php echo $new_name; ?>">
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <?php
            //Connect to the database
            $db = new mysqli('localhost', 'root', '', 'canes3');

            //Get information from the table
            $selectQuery = 'SELECT codeValueSequence, englishDescription FROM codevalue WHERE CodeTypeId = 1';

            $stmt1 = $db->prepare($selectQuery);

            $stmt1->execute();
            $stmt1->store_result();

            //Bind query result
            $stmt1-> bind_result($codeValueSequence, $englishDescription);
               echo ' <label for="userType">New user type:</label>';
            echo '<select name="userType" >';
            //Return information
            if ($stmt1->num_rows > 0) {
                while ($stmt1->fetch()) {
                    echo '<option value= "'.$codeValueSequence.'">' .$englishDescription.'</option>';
                }
            }
            echo "</select>";

            ?>

        </div>
        <div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
            <label>New Password</label>
            <input type="password" name="new_password" class="form-control" value="<?php echo $new_password; ?>">
            <span class="help-block"><?php echo $new_password_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
            <label>Confirm Password</label>
            <input type="password" name="confirm_password" class="form-control">
            <span class="help-block"><?php echo $confirm_password_err; ?></span>
        </div>
        <div class="form-group">
        <?php
        //Connect to the database
        $db = new mysqli('localhost', 'root', '', 'canes3');

        //Get information from the table
        $selectQuery = 'SELECT codeValueSequence, englishDescription FROM codevalue WHERE CodeTypeId = 2';

        $stmt1 = $db->prepare($selectQuery);

        $stmt1->execute();
        $stmt1->store_result();

        //Bind query result
        $stmt1-> bind_result($codeValueSequence, $englishDescription);

        echo '<select name="userActive" >';
        //Return information
        if ($stmt1->num_rows > 0) {
            while ($stmt1->fetch()) {
                echo '<option value= "'.$codeValueSequence.'">' .$englishDescription.'</option>';
            }
        }
        echo "</select>";

        ?>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">
            <a class="btn btn-link" href="welcome.php">Cancel</a>
        </div>
    </form>
</fieldset>
<!--</div>-->
<!--</body>-->
<!--</html>-->
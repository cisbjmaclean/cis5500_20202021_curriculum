<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

?>

<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-lg-10 align-self-end">
                <h1 class="text-uppercase text-white font-weight-bold">Curriculum</h1>
                <hr class="divider my-4" />
            </div>
            <div class="col-lg-8 align-self-baseline">
                <?php
                if (isset($_SESSION['username'])) {
                    echo '<p class="text-white-75 font-weight-light mb-5"> Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b></p>';
                }
                ?>
                <p>Welcome to our site</p>
            </div>
        </div>
    </div>
</header>

<?php
require("../../Bootstrap/incPageHead.php");
?>

<!--</body>-->
<!--</html>-->
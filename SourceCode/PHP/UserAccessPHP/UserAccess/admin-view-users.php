<?php
// Initialize the session

session_start();

// Check if the user is logged in, if not then redirect to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
    header("location: welcome.php");
    exit;
}
require("../../Bootstrap/incPageHead.php");
?>
<!--<html>-->
<!--<head>-->
<!--    <title>View Users</title>-->
<!--    <link rel="stylesheet" href="custom.css">-->
<!--</head>-->
<!--<body>-->
<div>
    <table class="table-bordered">
        <tr>
            <td>ID</td>
            <td>Username</td>
            <td>Password</td>
            <td>Name</td>
            <td>User Access Status Code</td>
            <td>Account Type</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>


<?php


//Connect to the database

include('config.php');

// get the records from the database
if ($result = $mysqli->query("SELECT * FROM useraccess")) {
// display records if there are records to display
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_object()) {
// set up a row for each record
            echo "<tr>";
            echo "<td>" . $row->userAccessId . "</td>";
            echo "<td>" . $row->username . "</td>";
            echo "<td>" . $row->password . "</td>";
            echo "<td>" . $row->name . "</td>";
            echo "<td>" . $row->userAccessStatusCode . "</td>";
            echo "<td>" . $row->userTypeCode . "</td>";
            echo "<td><a href='admin-edit-user.php?id=" . $row->userAccessId . "'>Edit</a></td>";
            echo "<td><a href='admin-delete-user.php?id=" . $row->userAccessId . "'>Delete</a></td>";
            echo "</tr>";
        }

    }
}?>





</table>
<!--</div>-->
<!--</body>-->
<!--</html>-->

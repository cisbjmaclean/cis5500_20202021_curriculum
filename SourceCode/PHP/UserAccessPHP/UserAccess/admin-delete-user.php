<?php
            // Initialize the session
    require("../../Bootstrap/incPageHead.php");
//    session_start();

    // Check if the user is logged in, if not then redirect him to login page
    if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
        header("location: login.php");
        exit;
    }
    if(!isset($_SESSION["userType"]) || $_SESSION["userType"] !== 2){
        header("location: welcome.php");
        exit;
    }
    require_once "config.php";



    $id = "";
$id = $_GET['id'];

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){

    $id = $_POST["id"];

    // Check input errors before updating the database

        // Prepare an update statement
        // Changed SQL parameters
        $sql = "DELETE FROM useraccess WHERE userAccessId = ?";

        if($stmt = $mysqli->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bind_param("i", $param_id);



            $param_id = $id;

            // Attempt to execute the prepared statement
            if($stmt->execute()){
                // Password updated successfully. Destroy the session, and redirect to login page

                header("location: welcome.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            $stmt->close();
        }


    // Close connection
    $mysqli->close();
}
?>
<fieldset>
        <h2>Delete User</h2>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group">
                <input type="hidden" name="id" class="form-control" value="<?php echo $id; ?>">
                <span class="help-block"></span>
            </div>
            <p>Are you sure you want to delete this account?</p>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-link" href="welcome.php">Cancel</a>
            </div>
        </form>
</fieldset>


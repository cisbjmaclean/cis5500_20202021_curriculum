<?php
/**
 * dataLoading.php
 *
 * Basic page designed to load information from a database on cpanel
 *
 * @author twhiten
 * @since 2020/02/24
 */
?>

<html>
    <head>
        <title>HCCIS Database - Person</title>
        <link rel="stylesheet" href="custom.css">
    </head>
    <body>
        <div>
            <table>
                <tr>
                   <td>ID</td>
                   <td>Name</td>
                   <td>Is Manager</td>
                   <td>Is Consultant</td>
                   <td>Is Active</td>
                </tr>
                <?php
                    //Connect to the database
                $db = new mysqli('69.172.198.104', 'twhiten_practice', 'U$o,Yis(58ez', 'twhiten_curriculum');



                //Get information from the table
                $selectQuery = 'SELECT * FROM Person';

                $stmt = $db->prepare($selectQuery);

                $stmt->execute();
                $stmt->store_result();

                //Bind query result
                $stmt-> bind_result($id, $firstName, $lastName ,$manager, $consultant, $active);


                //Return information
                if ($stmt->num_rows > 0) {
                    while ($stmt->fetch()) {
                        echo '<tr>
                                <td>'.$id.'</td>
                                <td>'.$firstName.' '.$lastName.'</td>
                                <td>'.$manager.'</td>
                                <td>'.$consultant.'</td>
                                <td>'.$active.'</td>
                              </tr>';
                    }
                }


                ?>
            </table>
        </div>
    </body>
</html>

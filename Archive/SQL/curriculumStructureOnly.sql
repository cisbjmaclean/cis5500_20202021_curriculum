-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 11, 2021 at 11:32 AM
-- Server version: 5.7.33
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `curriculum`
--
CREATE DATABASE IF NOT EXISTS `curriculum` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `curriculum`;

-- --------------------------------------------------------

--
-- Table structure for table `AssessmentCategory`
--

CREATE TABLE `AssessmentCategory` (
  `AssessmentCategoryId` int(11) NOT NULL,
  `AssessmentCategoryNameId` int(11) DEFAULT NULL,
  `AssessmentCategoryPercent` int(11) DEFAULT NULL,
  `CourseCatalogYearId` int(11) DEFAULT NULL,
  `AssessmentTypeId` int(11) DEFAULT NULL,
  `Assessment Note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Course`
--

CREATE TABLE `Course` (
  `CourseId` varchar(255) NOT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CourseCatalogYear`
--

CREATE TABLE `CourseCatalogYear` (
  `CourseCatalogYearId` int(11) NOT NULL,
  `CourseId` varchar(255) DEFAULT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `CourseVersion` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `CourseTitle` varchar(255) DEFAULT NULL,
  `MinimumGrade` int(11) DEFAULT NULL,
  `Hours` double DEFAULT NULL,
  `Credits` double DEFAULT NULL,
  `CourseDescription` mediumtext,
  `GradeSchemeId` int(11) DEFAULT NULL,
  `OutcomeHours` int(11) DEFAULT NULL,
  `Research` tinyint(1) DEFAULT NULL,
  `NewCourse` tinyint(1) DEFAULT NULL,
  `ReplacingExisting` tinyint(1) DEFAULT NULL,
  `OriginalCatYear` varchar(255) DEFAULT NULL,
  `CurrentVersionCat` varchar(255) DEFAULT NULL,
  `RevisionLevel` varchar(1) DEFAULT NULL,
  `RevisionDate` datetime DEFAULT NULL,
  `SupportingDoc` mediumtext,
  `AdditionalInfo` mediumtext,
  `SubjectMatterExpert` varchar(255) DEFAULT NULL,
  `ApprovedDate` datetime DEFAULT NULL,
  `ApprovedByConsultantPersonId` int(11) DEFAULT NULL,
  `ApprovedDateCC` datetime DEFAULT NULL,
  `Plar` tinyint(1) DEFAULT NULL,
  `RetiredDate` datetime DEFAULT NULL,
  `ESReading` tinyint(1) DEFAULT NULL,
  `ESDocumentUse` tinyint(1) DEFAULT NULL,
  `ESNumeracy` tinyint(1) DEFAULT NULL,
  `ESWriting` tinyint(1) DEFAULT NULL,
  `ESThinking` tinyint(1) DEFAULT NULL,
  `ESOral` tinyint(1) DEFAULT NULL,
  `ESWorking` tinyint(1) DEFAULT NULL,
  `ESCompUse` tinyint(1) DEFAULT NULL,
  `ESContinuousLearning` tinyint(1) DEFAULT NULL,
  `ClassHours` double DEFAULT NULL,
  `LabHours` double DEFAULT NULL,
  `ClinicalHours` double DEFAULT NULL,
  `OJTPracticumHours` double DEFAULT NULL,
  `InternshipHours` double DEFAULT NULL,
  `OpenStudies` tinyint(1) DEFAULT NULL,
  `ModalityFace` tinyint(1) DEFAULT NULL,
  `ModalityDistance` tinyint(1) DEFAULT NULL,
  `ModalityHybrid` tinyint(1) DEFAULT NULL,
  `InstructionalMethodId` int(11) DEFAULT NULL,
  `ApprovedByPMPersonId` int(11) DEFAULT NULL,
  `AssessmentNote` mediumtext,
  `AuthorizedByPersonId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CourseReplacement`
--

CREATE TABLE `CourseReplacement` (
  `CourseReplacementId` int(11) NOT NULL,
  `OldCourseId` varchar(255) DEFAULT NULL,
  `OldCourseTitle` varchar(255) DEFAULT NULL,
  `NewCourseId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CourseRequisite`
--

CREATE TABLE `CourseRequisite` (
  `CourseRequisiteId` int(11) NOT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `ParentCourseId` varchar(255) DEFAULT NULL,
  `ChildCourseId` varchar(255) DEFAULT NULL,
  `ChildTitle` varchar(255) DEFAULT NULL,
  `RequisiteTypeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LearningCompetency`
--

CREATE TABLE `LearningCompetency` (
  `LearningCompetencyId` int(11) NOT NULL,
  `LearningCompetencyName` mediumtext,
  `LearningCompetencyType` varchar(255) DEFAULT NULL,
  `LearningOutcomeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LearningOutcome`
--

CREATE TABLE `LearningOutcome` (
  `LearningOutcomeId` int(11) NOT NULL,
  `LearningOutcomeName` mediumtext,
  `LearningOutcomeType` varchar(255) DEFAULT NULL,
  `CourseCatalogYearId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupAssessmentCategoryName`
--

CREATE TABLE `LookupAssessmentCategoryName` (
  `AssessmentCategoryNameId` int(11) NOT NULL,
  `AssessmentCategoryName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupAssessmentType`
--

CREATE TABLE `LookupAssessmentType` (
  `AssessmentTypeId` int(11) NOT NULL,
  `AssessmentTypeName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupCatalogYear`
--

CREATE TABLE `LookupCatalogYear` (
  `CatalogYearID` int(11) NOT NULL,
  `CatalogYearName` int(11) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupGradeScheme`
--

CREATE TABLE `LookupGradeScheme` (
  `GradeSchemeId` int(11) NOT NULL,
  `GradeSchemeName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupInstrMethod`
--

CREATE TABLE `LookupInstrMethod` (
  `InstructionalMethodId` int(11) NOT NULL,
  `InstructionalMethodName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupProgramTitle`
--

CREATE TABLE `LookupProgramTitle` (
  `ProgramTitleId` int(11) NOT NULL,
  `ProgramTitle` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupRequisiteType`
--

CREATE TABLE `LookupRequisiteType` (
  `RequisiteId` int(11) NOT NULL,
  `RequisiteName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Name AutoCorrect Save Failures`
--

CREATE TABLE `Name AutoCorrect Save Failures` (
  `Object Name` varchar(255) DEFAULT NULL,
  `Object Type` varchar(255) DEFAULT NULL,
  `Failure Reason` varchar(255) DEFAULT NULL,
  `Time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Paste Errors`
--

CREATE TABLE `Paste Errors` (
  `F1` varchar(255) DEFAULT NULL,
  `F2` varchar(255) DEFAULT NULL,
  `F3` double DEFAULT NULL,
  `F4` varchar(255) DEFAULT NULL,
  `F5` varchar(255) DEFAULT NULL,
  `F6` varchar(255) DEFAULT NULL,
  `F7` double DEFAULT NULL,
  `F8` double DEFAULT NULL,
  `F9` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Person`
--

CREATE TABLE `Person` (
  `PersonId` int(11) NOT NULL,
  `PersonFirstName` varchar(40) DEFAULT NULL,
  `PersonLastName` varchar(40) DEFAULT NULL,
  `IsProgramManager` tinyint(1) DEFAULT NULL,
  `IsConsultant` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Program`
--

CREATE TABLE `Program` (
  `ProgramId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProgramCatalogYear`
--

CREATE TABLE `ProgramCatalogYear` (
  `ProgramCatalogYearId` int(11) NOT NULL,
  `ProgramId` varchar(255) DEFAULT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `ProgramTitleId` int(11) DEFAULT NULL,
  `ProgramMinimum` varchar(255) DEFAULT NULL,
  `ProgramManagerPersonId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProgramCatalogYearConsultant`
--

CREATE TABLE `ProgramCatalogYearConsultant` (
  `ProgramCatalogYearConsultantId` int(11) NOT NULL,
  `ProgramCatalogYearId` int(11) DEFAULT NULL,
  `ConsultantPersonId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProgramCourse`
--

CREATE TABLE `ProgramCourse` (
  `ProgramCourseId` int(11) NOT NULL,
  `ProgramCatalogYearId` int(11) DEFAULT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `CourseCatalogYearId` int(11) DEFAULT NULL,
  `Elective` tinyint(1) DEFAULT NULL,
  `Optional` tinyint(1) DEFAULT NULL,
  `DateRetired` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ReserveSubjectCode`
--

CREATE TABLE `ReserveSubjectCode` (
  `ID` int(11) NOT NULL,
  `SubjectCode` varchar(4) DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ReservedBy` varchar(255) DEFAULT NULL,
  `Common` varchar(255) DEFAULT NULL,
  `Accepted` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SubjectCodes`
--

CREATE TABLE `SubjectCodes` (
  `SubjectCodeId` int(11) NOT NULL,
  `FourLetterCode` varchar(255) DEFAULT NULL,
  `SubjectDescription` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AssessmentCategory`
--
ALTER TABLE `AssessmentCategory`
  ADD PRIMARY KEY (`AssessmentCategoryId`),
  ADD KEY `CourseCatalogYearId` (`CourseCatalogYearId`),
  ADD KEY `AssessmentTypeId` (`AssessmentTypeId`),
  ADD KEY `AssessmentCategoryNameId` (`AssessmentCategoryNameId`);

--
-- Indexes for table `Course`
--
ALTER TABLE `Course`
  ADD PRIMARY KEY (`CourseId`);

--
-- Indexes for table `CourseCatalogYear`
--
ALTER TABLE `CourseCatalogYear`
  ADD PRIMARY KEY (`CourseCatalogYearId`),
  ADD KEY `CourseId` (`CourseId`),
  ADD KEY `AuthorizedByPersonId` (`AuthorizedByPersonId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`),
  ADD KEY `GradeSchemeId` (`GradeSchemeId`),
  ADD KEY `ApprovedByConsultantPersonId` (`ApprovedByConsultantPersonId`),
  ADD KEY `ApprovedByPMPersonId` (`ApprovedByPMPersonId`),
  ADD KEY `InstructionalMethodId` (`InstructionalMethodId`);

--
-- Indexes for table `CourseReplacement`
--
ALTER TABLE `CourseReplacement`
  ADD PRIMARY KEY (`CourseReplacementId`),
  ADD KEY `OldCourseId` (`OldCourseId`),
  ADD KEY `NewCourseId` (`NewCourseId`);

--
-- Indexes for table `CourseRequisite`
--
ALTER TABLE `CourseRequisite`
  ADD PRIMARY KEY (`CourseRequisiteId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`),
  ADD KEY `ParentCourseId` (`ParentCourseId`),
  ADD KEY `ChildCourseId` (`ChildCourseId`),
  ADD KEY `RequisiteTypeId` (`RequisiteTypeId`);

--
-- Indexes for table `LearningCompetency`
--
ALTER TABLE `LearningCompetency`
  ADD PRIMARY KEY (`LearningCompetencyId`),
  ADD KEY `LearningOutcomeId` (`LearningOutcomeId`);

--
-- Indexes for table `LearningOutcome`
--
ALTER TABLE `LearningOutcome`
  ADD PRIMARY KEY (`LearningOutcomeId`),
  ADD KEY `CourseCatalogYearId` (`CourseCatalogYearId`);

--
-- Indexes for table `LookupAssessmentCategoryName`
--
ALTER TABLE `LookupAssessmentCategoryName`
  ADD PRIMARY KEY (`AssessmentCategoryNameId`);

--
-- Indexes for table `LookupAssessmentType`
--
ALTER TABLE `LookupAssessmentType`
  ADD PRIMARY KEY (`AssessmentTypeId`);

--
-- Indexes for table `LookupCatalogYear`
--
ALTER TABLE `LookupCatalogYear`
  ADD PRIMARY KEY (`CatalogYearID`);

--
-- Indexes for table `LookupGradeScheme`
--
ALTER TABLE `LookupGradeScheme`
  ADD PRIMARY KEY (`GradeSchemeId`);

--
-- Indexes for table `LookupInstrMethod`
--
ALTER TABLE `LookupInstrMethod`
  ADD PRIMARY KEY (`InstructionalMethodId`);

--
-- Indexes for table `LookupProgramTitle`
--
ALTER TABLE `LookupProgramTitle`
  ADD PRIMARY KEY (`ProgramTitleId`);

--
-- Indexes for table `LookupRequisiteType`
--
ALTER TABLE `LookupRequisiteType`
  ADD PRIMARY KEY (`RequisiteId`);

--
-- Indexes for table `Person`
--
ALTER TABLE `Person`
  ADD PRIMARY KEY (`PersonId`);

--
-- Indexes for table `Program`
--
ALTER TABLE `Program`
  ADD PRIMARY KEY (`ProgramId`);

--
-- Indexes for table `ProgramCatalogYear`
--
ALTER TABLE `ProgramCatalogYear`
  ADD PRIMARY KEY (`ProgramCatalogYearId`),
  ADD KEY `ProgramId` (`ProgramId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`),
  ADD KEY `ProgramTitleId` (`ProgramTitleId`),
  ADD KEY `ProgramManagerPersonId` (`ProgramManagerPersonId`);

--
-- Indexes for table `ProgramCatalogYearConsultant`
--
ALTER TABLE `ProgramCatalogYearConsultant`
  ADD PRIMARY KEY (`ProgramCatalogYearConsultantId`),
  ADD KEY `ConsultantPersonId` (`ConsultantPersonId`),
  ADD KEY `ProgramCatalogYearId` (`ProgramCatalogYearId`);

--
-- Indexes for table `ProgramCourse`
--
ALTER TABLE `ProgramCourse`
  ADD PRIMARY KEY (`ProgramCourseId`),
  ADD KEY `ProgramCatalogYearId` (`ProgramCatalogYearId`),
  ADD KEY `CourseCatalogYearId` (`CourseCatalogYearId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`);

--
-- Indexes for table `ReserveSubjectCode`
--
ALTER TABLE `ReserveSubjectCode`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `SubjectCodes`
--
ALTER TABLE `SubjectCodes`
  ADD PRIMARY KEY (`SubjectCodeId`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AssessmentCategory`
--
ALTER TABLE `AssessmentCategory`
  ADD CONSTRAINT `AssessmentCategory_ibfk_1` FOREIGN KEY (`CourseCatalogYearId`) REFERENCES `CourseCatalogYear` (`CourseCatalogYearId`),
  ADD CONSTRAINT `AssessmentCategory_ibfk_2` FOREIGN KEY (`AssessmentTypeId`) REFERENCES `LookupAssessmentType` (`AssessmentTypeId`),
  ADD CONSTRAINT `AssessmentCategory_ibfk_3` FOREIGN KEY (`AssessmentCategoryNameId`) REFERENCES `LookupAssessmentCategoryName` (`AssessmentCategoryNameId`);

--
-- Constraints for table `CourseCatalogYear`
--
ALTER TABLE `CourseCatalogYear`
  ADD CONSTRAINT `CourseCatalogYear_ibfk_1` FOREIGN KEY (`CourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_2` FOREIGN KEY (`AuthorizedByPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_3` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_4` FOREIGN KEY (`GradeSchemeId`) REFERENCES `LookupGradeScheme` (`GradeSchemeId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_5` FOREIGN KEY (`ApprovedByConsultantPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_6` FOREIGN KEY (`ApprovedByPMPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_7` FOREIGN KEY (`InstructionalMethodId`) REFERENCES `LookupInstrMethod` (`InstructionalMethodId`);

--
-- Constraints for table `CourseReplacement`
--
ALTER TABLE `CourseReplacement`
  ADD CONSTRAINT `CourseReplacement_ibfk_1` FOREIGN KEY (`OldCourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseReplacement_ibfk_2` FOREIGN KEY (`NewCourseId`) REFERENCES `Course` (`CourseId`);

--
-- Constraints for table `CourseRequisite`
--
ALTER TABLE `CourseRequisite`
  ADD CONSTRAINT `CourseRequisite_ibfk_1` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`),
  ADD CONSTRAINT `CourseRequisite_ibfk_2` FOREIGN KEY (`ParentCourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseRequisite_ibfk_3` FOREIGN KEY (`ChildCourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseRequisite_ibfk_4` FOREIGN KEY (`RequisiteTypeId`) REFERENCES `LookupRequisiteType` (`RequisiteId`);

--
-- Constraints for table `LearningCompetency`
--
ALTER TABLE `LearningCompetency`
  ADD CONSTRAINT `LearningCompetency_ibfk_1` FOREIGN KEY (`LearningOutcomeId`) REFERENCES `LearningOutcome` (`LearningOutcomeId`);

--
-- Constraints for table `LearningOutcome`
--
ALTER TABLE `LearningOutcome`
  ADD CONSTRAINT `LearningOutcome_ibfk_1` FOREIGN KEY (`CourseCatalogYearId`) REFERENCES `CourseCatalogYear` (`CourseCatalogYearId`);

--
-- Constraints for table `ProgramCatalogYear`
--
ALTER TABLE `ProgramCatalogYear`
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_1` FOREIGN KEY (`ProgramId`) REFERENCES `Program` (`ProgramId`),
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_2` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`),
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_3` FOREIGN KEY (`ProgramTitleId`) REFERENCES `LookupProgramTitle` (`ProgramTitleId`),
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_4` FOREIGN KEY (`ProgramManagerPersonId`) REFERENCES `Person` (`PersonId`);

--
-- Constraints for table `ProgramCatalogYearConsultant`
--
ALTER TABLE `ProgramCatalogYearConsultant`
  ADD CONSTRAINT `ProgramCatalogYearConsultant_ibfk_1` FOREIGN KEY (`ConsultantPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `ProgramCatalogYearConsultant_ibfk_2` FOREIGN KEY (`ProgramCatalogYearId`) REFERENCES `ProgramCatalogYear` (`ProgramCatalogYearId`);

--
-- Constraints for table `ProgramCourse`
--
ALTER TABLE `ProgramCourse`
  ADD CONSTRAINT `ProgramCourse_ibfk_1` FOREIGN KEY (`ProgramCatalogYearId`) REFERENCES `ProgramCatalogYear` (`ProgramCatalogYearId`),
  ADD CONSTRAINT `ProgramCourse_ibfk_2` FOREIGN KEY (`CourseCatalogYearId`) REFERENCES `CourseCatalogYear` (`CourseCatalogYearId`),
  ADD CONSTRAINT `ProgramCourse_ibfk_3` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

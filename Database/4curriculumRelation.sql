USE `curriculum`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AssessmentCategory`
--
ALTER TABLE `AssessmentCategory`
  ADD KEY `CourseCatalogYearId` (`CourseCatalogYearId`),
  ADD KEY `AssessmentTypeId` (`AssessmentTypeId`),
  ADD KEY `AssessmentCategoryNameId` (`AssessmentCategoryNameId`);

--
-- Indexes for table `Course`
--
ALTER TABLE `Course`
  ADD PRIMARY KEY (`CourseId`);

--
-- Indexes for table `CourseCatalogYear`
--
ALTER TABLE `CourseCatalogYear`
  ADD KEY `AuthorizedByPersonId` (`AuthorizedByPersonId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`),
  ADD KEY `GradeSchemeId` (`GradeSchemeId`),
  ADD KEY `ApprovedByConsultantPersonId` (`ApprovedByConsultantPersonId`),
  ADD KEY `ApprovedByPMPersonId` (`ApprovedByPMPersonId`),
  ADD KEY `InstructionalMethodId` (`InstructionalMethodId`);

--
-- Indexes for table `CourseReplacement`
--
ALTER TABLE `CourseReplacement`
  ADD KEY `OldCourseId` (`OldCourseId`),
  ADD KEY `NewCourseId` (`NewCourseId`);

--
-- Indexes for table `CourseRequisite`
--
ALTER TABLE `CourseRequisite`
  ADD KEY `CatalogYearId` (`CatalogYearId`),
  ADD KEY `ParentCourseId` (`ParentCourseId`),
  ADD KEY `ChildCourseId` (`ChildCourseId`),
  ADD KEY `RequisiteTypeId` (`RequisiteTypeId`);

--
-- Indexes for table `LearningCompetency`
--
ALTER TABLE `LearningCompetency`
  ADD KEY `LearningOutcomeId` (`LearningOutcomeId`);

--
-- Indexes for table `LearningOutcome`
--
ALTER TABLE `LearningOutcome`
  ADD KEY `CourseCatalogYearId` (`CourseCatalogYearId`);

--
-- Indexes for table `Program`
--
ALTER TABLE `Program`
  ADD PRIMARY KEY (`ProgramId`);

--
-- Indexes for table `ProgramCatalogYear`
--
ALTER TABLE `ProgramCatalogYear`
  ADD KEY `ProgramId` (`ProgramId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`),
  ADD KEY `ProgramTitleId` (`ProgramTitleId`),
  ADD KEY `ProgramManagerPersonId` (`ProgramManagerPersonId`);

--
-- Indexes for table `ProgramCatalogYearConsultant`
--
ALTER TABLE `ProgramCatalogYearConsultant`
  ADD KEY `ConsultantPersonId` (`ConsultantPersonId`),
  ADD KEY `ProgramCatalogYearId` (`ProgramCatalogYearId`);

--
-- Indexes for table `ProgramCourse`
--
ALTER TABLE `ProgramCourse`
  ADD KEY `ProgramCatalogYearId` (`ProgramCatalogYearId`),
  ADD KEY `CourseCatalogYearId` (`CourseCatalogYearId`),
  ADD KEY `CatalogYearId` (`CatalogYearId`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `AssessmentCategory`
--
ALTER TABLE `AssessmentCategory`
  ADD CONSTRAINT `AssessmentCategory_ibfk_1` FOREIGN KEY (`CourseCatalogYearId`) REFERENCES `CourseCatalogYear` (`CourseCatalogYearId`),
  ADD CONSTRAINT `AssessmentCategory_ibfk_2` FOREIGN KEY (`AssessmentTypeId`) REFERENCES `LookupAssessmentType` (`AssessmentTypeId`),
  ADD CONSTRAINT `AssessmentCategory_ibfk_3` FOREIGN KEY (`AssessmentCategoryNameId`) REFERENCES `LookupAssessmentCategoryName` (`AssessmentCategoryNameId`);

--
-- Constraints for table `CourseCatalogYear`
--
ALTER TABLE `CourseCatalogYear`
  ADD CONSTRAINT `CourseCatalogYear_ibfk_1` FOREIGN KEY (`CourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_2` FOREIGN KEY (`AuthorizedByPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_3` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_4` FOREIGN KEY (`GradeSchemeId`) REFERENCES `LookupGradeScheme` (`GradeSchemeId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_5` FOREIGN KEY (`ApprovedByConsultantPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_6` FOREIGN KEY (`ApprovedByPMPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `CourseCatalogYear_ibfk_7` FOREIGN KEY (`InstructionalMethodId`) REFERENCES `LookupInstrMethod` (`InstructionalMethodId`);

--
-- Constraints for table `CourseReplacement`
--
ALTER TABLE `CourseReplacement`
  ADD CONSTRAINT `CourseReplacement_ibfk_1` FOREIGN KEY (`OldCourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseReplacement_ibfk_2` FOREIGN KEY (`NewCourseId`) REFERENCES `Course` (`CourseId`);

--
-- Constraints for table `CourseRequisite`
--
ALTER TABLE `CourseRequisite`
  ADD CONSTRAINT `CourseRequisite_ibfk_1` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`),
  ADD CONSTRAINT `CourseRequisite_ibfk_2` FOREIGN KEY (`ParentCourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseRequisite_ibfk_3` FOREIGN KEY (`ChildCourseId`) REFERENCES `Course` (`CourseId`),
  ADD CONSTRAINT `CourseRequisite_ibfk_4` FOREIGN KEY (`RequisiteTypeId`) REFERENCES `LookupRequisiteType` (`RequisiteId`);

--
-- Constraints for table `LearningCompetency`
--
ALTER TABLE `LearningCompetency`
  ADD CONSTRAINT `LearningCompetency_ibfk_1` FOREIGN KEY (`LearningOutcomeId`) REFERENCES `LearningOutcome` (`LearningOutcomeId`);

--
-- Constraints for table `LearningOutcome`
--
ALTER TABLE `LearningOutcome`
  ADD CONSTRAINT `LearningOutcome_ibfk_1` FOREIGN KEY (`CourseCatalogYearId`) REFERENCES `CourseCatalogYear` (`CourseCatalogYearId`);

--
-- Constraints for table `ProgramCatalogYear`
--
ALTER TABLE `ProgramCatalogYear`
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_1` FOREIGN KEY (`ProgramId`) REFERENCES `Program` (`ProgramId`),
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_2` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`),
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_3` FOREIGN KEY (`ProgramTitleId`) REFERENCES `LookupProgramTitle` (`ProgramTitleId`),
  ADD CONSTRAINT `ProgramCatalogYear_ibfk_4` FOREIGN KEY (`ProgramManagerPersonId`) REFERENCES `Person` (`PersonId`);

--
-- Constraints for table `ProgramCatalogYearConsultant`
--
ALTER TABLE `ProgramCatalogYearConsultant`
  ADD CONSTRAINT `ProgramCatalogYearConsultant_ibfk_1` FOREIGN KEY (`ConsultantPersonId`) REFERENCES `Person` (`PersonId`),
  ADD CONSTRAINT `ProgramCatalogYearConsultant_ibfk_2` FOREIGN KEY (`ProgramCatalogYearId`) REFERENCES `ProgramCatalogYear` (`ProgramCatalogYearId`);

--
-- Constraints for table `ProgramCourse`
--
ALTER TABLE `ProgramCourse`
  ADD CONSTRAINT `ProgramCourse_ibfk_1` FOREIGN KEY (`ProgramCatalogYearId`) REFERENCES `ProgramCatalogYear` (`ProgramCatalogYearId`),
  ADD CONSTRAINT `ProgramCourse_ibfk_2` FOREIGN KEY (`CourseCatalogYearId`) REFERENCES `CourseCatalogYear` (`CourseCatalogYearId`),
  ADD CONSTRAINT `ProgramCourse_ibfk_3` FOREIGN KEY (`CatalogYearId`) REFERENCES `LookupCatalogYear` (`CatalogYearID`);
COMMIT;

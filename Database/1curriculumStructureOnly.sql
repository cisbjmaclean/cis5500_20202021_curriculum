--
-- Database: `curriculum`
--
DROP DATABASE IF EXISTS `curriculum`;
CREATE DATABASE `curriculum` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `curriculum`;

-- --------------------------------------------------------

--
-- Table structure for table `AssessmentCategory`
--

CREATE TABLE `AssessmentCategory` (
  `AssessmentCategoryId` int(11) AUTO_INCREMENT NOT NULL,
  `AssessmentCategoryNameId` int(11) DEFAULT NULL,
  `AssessmentCategoryPercent` int(11) DEFAULT NULL,
  `CourseCatalogYearId` int(11) DEFAULT NULL,
  `AssessmentTypeId` int(11) DEFAULT NULL,
  `Assessment Note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`AssessmentCategoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Course`
--

CREATE TABLE `Course` (
  `CourseId` varchar(255)NOT NULL,
  `Active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CourseCatalogYear`
--

CREATE TABLE `CourseCatalogYear` (
  `CourseCatalogYearId` int(11) AUTO_INCREMENT NOT NULL,
  `CourseId` varchar(255) DEFAULT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `CourseVersion` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `CourseTitle` varchar(255) DEFAULT NULL,
  `MinimumGrade` int(11) DEFAULT NULL,
  `Hours` double DEFAULT NULL,
  `Credits` double DEFAULT NULL,
  `CourseDescription` mediumtext,
  `GradeSchemeId` int(11) DEFAULT NULL,
  `OutcomeHours` int(11) DEFAULT NULL,
  `Research` tinyint(1) DEFAULT NULL,
  `NewCourse` tinyint(1) DEFAULT NULL,
  `ReplacingExisting` tinyint(1) DEFAULT NULL,
  `OriginalCatYear` varchar(255) DEFAULT NULL,
  `CurrentVersionCat` varchar(255) DEFAULT NULL,
  `RevisionLevel` varchar(1) DEFAULT NULL,
  `RevisionDate` date DEFAULT NULL,
  `SupportingDoc` mediumtext,
  `AdditionalInfo` mediumtext,
  `SubjectMatterExpert` varchar(255) DEFAULT NULL,
  `ApprovedDate` date DEFAULT NULL,
  `ApprovedByConsultantPersonId` int(11) DEFAULT NULL,
  `ApprovedDateCC` date DEFAULT NULL,
  `Plar` tinyint(1) DEFAULT NULL,
  `RetiredDate` date DEFAULT NULL,
  `ESReading` tinyint(1) DEFAULT NULL,
  `ESDocumentUse` tinyint(1) DEFAULT NULL,
  `ESNumeracy` tinyint(1) DEFAULT NULL,
  `ESWriting` tinyint(1) DEFAULT NULL,
  `ESThinking` tinyint(1) DEFAULT NULL,
  `ESOral` tinyint(1) DEFAULT NULL,
  `ESWorking` tinyint(1) DEFAULT NULL,
  `ESCompUse` tinyint(1) DEFAULT NULL,
  `ESContinuousLearning` tinyint(1) DEFAULT NULL,
  `ClassHours` double DEFAULT NULL,
  `LabHours` double DEFAULT NULL,
  `ClinicalHours` double DEFAULT NULL,
  `OJTPracticumHours` double DEFAULT NULL,
  `InternshipHours` double DEFAULT NULL,
  `OpenStudies` tinyint(1) DEFAULT NULL,
  `ModalityFace` tinyint(1) DEFAULT NULL,
  `ModalityDistance` tinyint(1) DEFAULT NULL,
  `ModalityHybrid` tinyint(1) DEFAULT NULL,
  `InstructionalMethodId` int(11) DEFAULT NULL,
  `ApprovedByPMPersonId` int(11) DEFAULT NULL,
  `AssessmentNote` mediumtext,
  `AuthorizedByPersonId` int(11) DEFAULT NULL,
  PRIMARY KEY (`CourseCatalogYearId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CourseReplacement`
--

CREATE TABLE `CourseReplacement` (
  `CourseReplacementId` int(11) AUTO_INCREMENT NOT NULL,
  `OldCourseId` varchar(255) DEFAULT NULL,
  `OldCourseTitle` varchar(255) DEFAULT NULL,
  `NewCourseId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CourseReplacementId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `CourseRequisite`
--

CREATE TABLE `CourseRequisite` (
  `CourseRequisiteId` int(11) AUTO_INCREMENT NOT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `ParentCourseId` varchar(255) DEFAULT NULL,
  `ChildCourseId` varchar(255) DEFAULT NULL,
  `ChildTitle` varchar(255) DEFAULT NULL,
  `RequisiteTypeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`CourseRequisiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LearningCompetency`
--

CREATE TABLE `LearningCompetency` (
  `LearningCompetencyId` int(11) AUTO_INCREMENT NOT NULL,
  `LearningCompetencyName` mediumtext,
  `LearningCompetencyType` varchar(255) DEFAULT NULL,
  `LearningOutcomeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`LearningCompetencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LearningOutcome`
--

CREATE TABLE `LearningOutcome` (
  `LearningOutcomeId` int(11) AUTO_INCREMENT NOT NULL,
  `LearningOutcomeName` mediumtext,
  `LearningOutcomeType` varchar(255) DEFAULT NULL,
  `CourseCatalogYearId` int(11) DEFAULT NULL,
  PRIMARY KEY (`LearningOutcomeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupAssessmentCategoryName`
--

CREATE TABLE `LookupAssessmentCategoryName` (
  `AssessmentCategoryNameId` int(11) AUTO_INCREMENT NOT NULL,
  `AssessmentCategoryName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`AssessmentCategoryNameId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupAssessmentType`
--

CREATE TABLE `LookupAssessmentType` (
  `AssessmentTypeId` int(11) AUTO_INCREMENT NOT NULL,
  `AssessmentTypeName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`AssessmentTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupCatalogYear`
--

CREATE TABLE `LookupCatalogYear` (
  `CatalogYearID` int(11) AUTO_INCREMENT NOT NULL,
  `CatalogYearName` int(11) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`CatalogYearId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupGradeScheme`
--

CREATE TABLE `LookupGradeScheme` (
  `GradeSchemeId` int(11) AUTO_INCREMENT NOT NULL,
  `GradeSchemeName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`GradeSchemeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupInstrMethod`
--

CREATE TABLE `LookupInstrMethod` (
  `InstructionalMethodId`int(11) AUTO_INCREMENT NOT NULL,
  `InstructionalMethodName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`InstructionalMethodId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupProgramTitle`
--

CREATE TABLE `LookupProgramTitle` (
  `ProgramTitleId` int(11) AUTO_INCREMENT NOT NULL,
  `ProgramTitle` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ProgramTitleId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `LookupRequisiteType`
--

CREATE TABLE `LookupRequisiteType` (
  `RequisiteId` int(11) AUTO_INCREMENT NOT NULL,
  `RequisiteName` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `Active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`RequisiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Name AutoCorrect Save Failures`
--

CREATE TABLE `Name AutoCorrect Save Failures` (
  `Object Name` varchar(255) DEFAULT NULL,
  `Object Type` varchar(255) DEFAULT NULL,
  `Failure Reason` varchar(255) DEFAULT NULL,
  `Time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Paste Errors`
--

CREATE TABLE `Paste Errors` (
  `F1` varchar(255) DEFAULT NULL,
  `F2` varchar(255) DEFAULT NULL,
  `F3` double DEFAULT NULL,
  `F4` varchar(255) DEFAULT NULL,
  `F5` varchar(255) DEFAULT NULL,
  `F6` varchar(255) DEFAULT NULL,
  `F7` double DEFAULT NULL,
  `F8` double DEFAULT NULL,
  `F9` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Person`
--

CREATE TABLE `Person` (
  `PersonId` int(11) AUTO_INCREMENT NOT NULL,
  `PersonFirstName` varchar(40) DEFAULT NULL,
  `PersonLastName` varchar(40) DEFAULT NULL,
  `IsProgramManager` tinyint(1) DEFAULT NULL,
  `IsConsultant` tinyint(1) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PersonId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Program`
--

CREATE TABLE `Program` (
  `ProgramId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProgramCatalogYear`
--

CREATE TABLE `ProgramCatalogYear` (
  `ProgramCatalogYearId` int(11) AUTO_INCREMENT NOT NULL,
  `ProgramId` varchar(255) DEFAULT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `ProgramTitleId` int(11) DEFAULT NULL,
  `ProgramMinimum` varchar(255) DEFAULT NULL,
  `ProgramManagerPersonId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProgramCatalogYearId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProgramCatalogYearConsultant`
--

CREATE TABLE `ProgramCatalogYearConsultant` (
  `ProgramCatalogYearConsultantId` int(11) AUTO_INCREMENT NOT NULL,
  `ProgramCatalogYearId` int(11) DEFAULT NULL,
  `ConsultantPersonId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProgramCatalogYearConsultantId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ProgramCourse`
--

CREATE TABLE `ProgramCourse` (
  `ProgramCourseId` int(11) AUTO_INCREMENT NOT NULL,
  `ProgramCatalogYearId` int(11) DEFAULT NULL,
  `CatalogYearId` int(11) DEFAULT NULL,
  `CourseCatalogYearId` int(11) DEFAULT NULL,
  `Elective` tinyint(1) DEFAULT NULL,
  `Optional` tinyint(1) DEFAULT NULL,
  `DateRetired` date DEFAULT NULL,
  PRIMARY KEY (`ProgramCourseId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ReserveSubjectCode`
--

CREATE TABLE `ReserveSubjectCode` (
  `ID` int(11) AUTO_INCREMENT NOT NULL,
  `SubjectCode` varchar(4) DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `ReservedBy` varchar(255) DEFAULT NULL,
  `Common` varchar(255) DEFAULT NULL,
  `Accepted` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `SubjectCodes`
--

CREATE TABLE `SubjectCodes` (
  `SubjectCodeId` int(11) AUTO_INCREMENT NOT NULL,
  `FourLetterCode` varchar(255) DEFAULT NULL,
  `SubjectDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SubjectCodeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

